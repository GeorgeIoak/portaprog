/* ***************************************************************************
* File:    avrSPIIO.cpp
* Date:    2019.09.16
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the MIT License
*
* ******************************************************************************/

/* ---
--------------------------------------------------------------------------
### AVR SPI Interface

Provides AVR flashing using standard HEX files stored on SPIFFS file system
--- */

/*
Pedigree:
    ESP32 as SPI Programmer
    Copyright (c) 2019 Bradan Lane Studio

	Standalone AVR ISP programmer
	August 2011 by Limor Fried / Ladyada / Adafruit
	Jan 2011 by Bill Westfield ("WestfW")

	based on AVRISP (attribution not specified)
*/

#include <SPI.h>
SPIClass SPI1(HSPI);
#define SPIIO SPI1

// local includes
#include "allincludes.h"

// -------------------------------------------------------------------------
// --------------- Data Structures, Constants, and Globals -----------------
// -------------------------------------------------------------------------

#define FUSE_RW_SUPPORT // WARNING!!! uncommententing this line should only be done by really smart ... or crazy people

// keep one of each of the following pairs of print macros

#define MAX_NAME_LEN  16

#define MAX_PAGE_SIZE 256

// fuses array used in the chip definition structure
#define FUSE_LOW	  0							/* Low fuse */
#define FUSE_HIGH	  1							/* High fuse */
#define FUSE_EXT	  2							/* Extended fuse */
#define FUSE_PROT	  3 /* memory protection */ // we will prevent this from beeing changed
#define SPI_MAX_FUSES 4

const char *_spi_fuse_names[] = {"LOW", "HIGH", "EXT", "PROT"};
const uint8_t _spi_fuse_masks[] = {0xFF, 0xFF, 0x07, 0x3F};
const uint8_t _spi_fuse_addr[] = {0xA0, 0xA8, 0xA4, 0xE0};

typedef struct chip_definition {
	char family[MAX_NAME_LEN];
	char shortname[MAX_NAME_LEN]; // use for logging; not required for programming - eg "atmega168p"
	char longname[MAX_NAME_LEN];  // use for logging; not required for programming - eg "atmega168p"
	uint16_t signature;			  // Low two bytes of signature
	uint32_t flash_size;		  // size of chip flash storage
	uint16_t flash_pagesize;	  // size of chip page
	uint16_t eeprom_size;		  // size of chip flash storage
	uint16_t eeprom_pagesize;	  // size of chip page
	bool has_uid;
} chip_definition_t;

const chip_definition_t _spi_chips[] = {
	{"ATmega", "m168p",		"ATmega168P", 		0x940B,  16 * 1024, 128,  512, 4, true},
	{"ATmega", "m168pb", 	"ATmega168PB", 		0x9415,  16 * 1024, 128,  512, 4, true},
	{"ATmega", "m328p", 	"ATmega328P", 		0x950F,  32 * 1024, 128, 1024, 4, true},
	{"ATmega", "m328pb", 	"ATmega328PB", 		0x9516,  32 * 1024, 128, 1024, 4, true},
	{"ATmega", "m128rf", 	"ATmega128RFA1", 	0xA701, 128 * 1024, 256, 4096, 8, true},
	{"ATtiny", "t85", 		"ATtiny85", 		0x930B,   8 * 1024,  64,  512, 4, false},
	{"ATtiny", "t13", 		"ATtiny13", 		0x9007,   1 * 1024,  32,   64, 4, false},
};
uint8_t SPI_NUMIMAGES = sizeof(_spi_chips) / sizeof(chip_definition_t);


static const chip_definition_t *g_spi;	// global device; shared with the various functions
#define SPI_UID_SIZE 10
static uint8_t _spi_chip_uid[SPI_UID_SIZE];

// Global Variables
static int _spi_pmode = 0;

static bool _spi_verified;

#define SPI_FREQ 125e3

static SPISettings g_spi_flash_settings = SPISettings(SPI_FREQ, MSBFIRST, SPI_MODE0);
//static SPISettings g_spi_fuses_settings = SPISettings(SPI_FREQ, MSBFIRST, SPI_MODE0);
#define g_spi_fuses_settings g_spi_flash_settings // we may use the same settings for both

// SPI programming of an AVR
static uint16_t _spi_transaction(uint8_t a, uint8_t b, uint8_t c, uint8_t d) {
	uint8_t n, m, r;
	SPIIO.transfer(a);
	n = SPIIO.transfer(b);
	//if (n != a) error = -1;
	m = SPIIO.transfer(c);
	r = SPIIO.transfer(d);

	//VERBOSE("SPI write: 0x%X 0x%X 0x%X 0x%X  SPI receive: 0x%X 0x%X 0x%X\n", a, b, c, d, n, m, r);

	return 0xFFFFFF & (((uint32_t)n << 16) + (m << 8) + r);
}

// Simply polls the chip until it is not busy any more - for erasing and programming
static void _spi_busy_wait(void) {
	SPIIO.beginTransaction(g_spi_fuses_settings);
	byte busybit;
	do {
		busybit = _spi_transaction(0xF0, 0x0, 0x0, 0x0);
	} while (busybit & 0x01);

	SPIIO.endTransaction();
}

static void _spi_start() {
	if (!_spi_pmode)
	{
		SPIIO.begin(PIN_SPI_CLK, PIN_SPI_MISO, PIN_SPI_MOSI);
		SPIIO.setBitOrder(MSBFIRST); // LSBFIRST
		SPIIO.setFrequency(SPI_FREQ);
		SPIIO.setHwCs(false);

		// try to sync the bus
		SPIIO.transfer(0x00);
		digitalWrite(PIN_SPI_RESET, HIGH);
		delayMicroseconds(50);
		digitalWrite(PIN_SPI_RESET, LOW);
		delay(30);
		uint8_t b = _spi_transaction(0xAC, 0x53, 0x00, 0x00);
		VERBOSE("ISP pmode start = %02X\n", b);
		_spi_pmode = 1;
	}
}

static void _spi_end() {
	if (_spi_pmode)
	{
		SPIIO.end();
		digitalWrite(PIN_SPI_RESET, HIGH);
		VERBOSE("ISP pmode end\n");
		_spi_pmode = 0;
	}
}


// -------------------------------------------------------------------------
// ------------------- AVR Fuses and Lock Bits Functions -------------------
// -------------------------------------------------------------------------

// get the current chip signature and lookup its settings
static const chip_definition_t *_spi_find_chip() {
	uint16_t signature = 0;
	uint8_t sig_bytes[3];
	// read the bottom two signature bytes; the highest signature byte is the same over all AVRs so we skip it

	//SPIIO.beginTransaction(g_spi_fuses_settings);
	sig_bytes[0] = _spi_transaction(0x30, 0x00, 0x00, 0x00);
	sig_bytes[1] = _spi_transaction(0x30, 0x00, 0x01, 0x00);
	sig_bytes[2] = _spi_transaction(0x30, 0x00, 0x02, 0x00);
	//SPIIO.endTransaction();
	signature = (sig_bytes[1] << 8) | sig_bytes[2];

	VERBOSE("Chip signature: %02X%02X%02X == %04X\n", sig_bytes[0], sig_bytes[1], sig_bytes[2], signature);

	if (signature == 0) {
		VERBOSE("No SPI chip found\n");
		return 0;
	}
	if (signature == 0xFFFF) {
		VERBOSE("Error reading chip\n");
		return 0;
	}

	// MESSAGE("Searching for chip:");

	memset(_spi_chip_uid, 0, SPI_UID_SIZE);

	for (uint8_t i = 0; i < SPI_NUMIMAGES; i++) {
		g_spi = &(_spi_chips[i]);
		if (g_spi && (g_spi->signature == signature)) {
			VERBOSE("Chip: %s\n", g_spi->longname);
			if (_spi_chips[i].has_uid) {
				// this assumes the UID is an even number of bytes ... which it is
				for (uint8_t j = 0; j < (SPI_UID_SIZE / 2); j++) {
					uint8_t b;
					b = _spi_transaction(0x30, 0x00, (0x0E) + j, 0x00);
					_spi_chip_uid[j * 2] = b;
					b = _spi_transaction(0x38, 0x00, (0x0E) + j, 0x00);
					_spi_chip_uid[(j * 2) + 1] = b;
				}
			}
			return g_spi;
		}
	}

	VERBOSE("Chip Not Recognized\n");
	return 0;
}

#ifdef FUSE_RW_SUPPORT // we don't want to allow changing of fuses and lock bits until we can trust ourselves
// Program one fuse
static void _spi_program_fuse_byte(uint8_t fuse_byte, uint8_t val) {
	_spi_busy_wait();
	if (val) {
		uint8_t rtn = 0;
		SPIIO.beginTransaction(g_spi_fuses_settings);
		rtn = _spi_transaction(0xAc, _spi_fuse_addr[fuse_byte], 0x00, val);
		SPIIO.endTransaction();
#if 0
		DEBUGSERIAL.print("  ");
		DEBUGSERIAL.print(_spi_fuse_names[fuse_byte]);
		DEBUGSERIAL.print(" Fuse: ");
		DEBUGSERIAL.print(fuse_byte, HEX);
		DEBUGSERIAL.print(":");
		DEBUGSERIAL.print(val, HEX);
		DEBUGSERIAL.print(" -> ");
		DEBUGSERIAL.print(rtn, HEX);
#endif
	}
}

#endif

static uint8_t _spi_read_fuse_byte(uint8_t byte1, uint8_t byte2) {
	uint8_t rtn;

	SPIIO.beginTransaction(g_spi_fuses_settings);
	rtn = _spi_transaction(byte1, byte2, 0x00, 0x00); // lock fuse
	SPIIO.endTransaction();
	return rtn;
}

// -------------------------------------------------------------------------
// -------------------------- AVR Flash Functions --------------------------
// -------------------------------------------------------------------------

// Send the erase command, then busy wait until the chip is erased

static void _spi_erase_chip(void) {
	//MESSAGE("Erasing chip:");

	SPIIO.beginTransaction(g_spi_fuses_settings);
	_spi_transaction(0xAC, 0x80, 0, 0); // chip erase
	SPIIO.endTransaction();
	_spi_busy_wait();
	MESSAGE("Chip erased\n");
}

// verifyImage does a byte-by-byte verify of the flash hex against the chip
// Thankfully this does not have to be done by pages!
// returns true if the image is the same as the file, returns false on any error
static bool _spi_read_flash(ihexStream* data, bool save) {
	_spi_verified = false; // global

	int32_t count;
	uint16_t flashaddr;
	int32_t size;

	flashaddr = 0;
	count = 0;
	uint8_t error_count = 0;

	if (save) {
		data->clear();
		size = g_spi->flash_size;
		MESSAGE("Reading %d bytes ", size); // deliberately no LF; the start + progress + finish messages are all combined
	}
	else {
		data->rewind();
		size = data->available();
		MESSAGE("Verifying %d bytes ", size); // deliberately no LF; the start + progress + finish messages are all combined
	}

	do {
		_spi_verified = false;

		uint8_t low, high; // we read flash in WORDs

		SPIIO.beginTransaction(g_spi_flash_settings);
		high = _spi_transaction(0x28, (flashaddr >> 8) & 0xFF, flashaddr & 0xFF, 0);
		low = _spi_transaction(0x20, (flashaddr >> 8) & 0xFF, flashaddr & 0xFF, 0);
		SPIIO.endTransaction();

		if (save) {
			// we save SPI data to buffer
			count += data->write(low);
			count += data->write(high);
		} else {
			// we compare SPI data to buffer
			int c;
			c = data->read();
			if (low != c) {
				VERBOSE("verror at %04X: %02X != %02X\n", flashaddr, low, c);
				error_count++;
				if (error_count > 8)
					break; // return false;
			}
			count++;
			c = data->read();
			if (high !=c) {
				VERBOSE("verror at %04X: %02X != %02X\n", flashaddr, high, c);
				error_count++;
				if (error_count > 8)
					break; // return false;
			}
			count++;
		}
		if ((count % (g_spi->flash_pagesize)) == 0)
			MESSAGE(".");

		flashaddr++;
		_spi_verified = true;
	} while (count < size);

	if (_spi_verified) {
		if (save)
			MESSAGE(" Success %d bytes (%d)\n", count, error_count);
		else
			MESSAGE(" Verified %d bytes (%d)\n", count, error_count);
	}
	else
		MESSAGE(" Failed at %d bytes\n", count);

	return _spi_verified;
}


static bool _spi_read_eeprom(ihexStream* data, bool save) {
	_spi_verified = false; // global

	uint16_t count;
	uint16_t eepromaddr;
	uint32_t size;

	eepromaddr = 0;
	count = 0;
	uint8_t error_count = 0;

	if (save) {
		data->clear();
		size = g_spi->eeprom_size;
		MESSAGE("Reading EEPROM %d bytes ", size); // deliberately no LF; the start + progress + finish messages are all combined
	}
	else {
		data->rewind();
		size = data->available();
		MESSAGE("Verifying EEPROM %d bytes ", size); // deliberately no LF; the start + progress + finish messages are all combined
	}

	do {
		_spi_verified = false;

		uint8_t low; // we read eeprom in BYTEs

		SPIIO.beginTransaction(g_spi_flash_settings);
		low = _spi_transaction(0xA0, (eepromaddr >> 8) & 0xFF, eepromaddr & 0xFF, 0xFF);
		SPIIO.endTransaction();

		if (save) {
			// we save SPI data to buffer
			// BUG need to test the return from write to make sure it is indeed 1 byte each time (0 would mean it failed)
			data->write(low);
			count += 1;
		} else {
			// we compare SPI data to buffer
			int c;
			c = data->read();
			if (low != c) {
				VERBOSE("verror at %04X: %02X != %02X\n", eepromaddr, low, c);
				error_count++;
				if (error_count > 8)
					break; // return false;
			}
			count++;
		}
		if ((count % (g_spi->eeprom_pagesize)) == 0)
			MESSAGE(".");

		eepromaddr++;
		_spi_verified = true;
	} while (count < size);

	if (_spi_verified)
		MESSAGE(" Success\n");
	else
		MESSAGE(" Failed at %d bytes\n", count);

	return _spi_verified;
}


static void _spi_write_byte_to_chip(uint8_t hilo, uint16_t addr, uint8_t data) {
	uint8_t val;
	val = _spi_transaction(0x40 + (8 * hilo), addr >> 8 & 0xFF, addr & 0xFF, data);
	VERBOSE("%02X ", data);
	//VERBOSE(":%02X ", val);
}


// write the pagebuff (with pagesize bytes in it) into page $pageaddr
static bool _spi_write_page_buffer_to_chip(uint32_t pageaddr, uint16_t pagesize, uint8_t *pagebuff) {
	uint16_t commitreply;

	uint32_t addr = (pageaddr / 2); // page addr is in bytes, but we need to convert to words (/2)

	VERBOSE("Flashing page 0x%04X\n", pageaddr);

	SPIIO.beginTransaction(g_spi_flash_settings);
	for (uint16_t i = 0; i < pagesize; i += 2) {
		_spi_write_byte_to_chip(LOW, addr+(i/2), pagebuff[i]);
		_spi_write_byte_to_chip(HIGH, addr+(i/2), pagebuff[i+1]);
	}
	commitreply = _spi_transaction(0x4C, (addr >> 8) & 0xFF, addr & 0xFF, 0);
	SPIIO.endTransaction();

	VERBOSE("\n");

	VERBOSE("  Commit Page: 0x%x -> 0x%X\n", pageaddr, commitreply*2);
	if (commitreply != addr) {
		MESSAGE (" cerror: %04X:%02X ", pageaddr, commitreply*2);
		return false;
	}

	_spi_busy_wait();
	return true;
}

static bool _spi_write_flash(ihexStream* src) {
	bool success;
	uint8_t buffer[MAX_PAGE_SIZE]; /* One page of flash */

	success = true; // assume we are successful; we change this at any point that we know we failed

	_spi_start();

	do { // a do {...} while(0); loop allows use to break to the end at any point
		const chip_definition_t *target_chip;
		if (!(target_chip = _spi_find_chip())) {
			success = false;
			break;
		}

		int32_t chipsize = target_chip->flash_size;
		uint16_t pagesize = target_chip->flash_pagesize;
		uint32_t pageaddr = 0;
		int32_t datasize = src->available();

		// this should not happen
		if (pagesize > MAX_PAGE_SIZE) {
			MESSAGE("ERROR: pagesize %d > %d\n", pagesize, MAX_PAGE_SIZE);
			success = false;
			break;
		}

		VERBOSE("Chip Flash: %d Page: %d\n", chipsize, pagesize);

		MESSAGE("Flashing %d bytes ", datasize); // deliberately no LF; the start + progress + finish messages are all combined

		_spi_erase_chip();

		//MESSAGE("Starting Flash: ");
		pageaddr = 0;
		while (pageaddr < datasize) {
			VERBOSE("Writing address %4X\n", pageaddr);

			// test if entire buffer is all filler bytes aka 0xFF
			bool blankpage = true;
			for (uint16_t i = 0; i < pagesize; i++) {
				if (src->available()) {
					buffer[i] = src->read();
					blankpage = false;
				}
				else
					buffer[i] = 0xFF;	// fill unused end of buffer with the FILLER_BYTE
			}

			if (!blankpage) {
				if (!_spi_write_page_buffer_to_chip(pageaddr, pagesize, buffer)) {
					MESSAGE(" Failed\n");
					success = false;
					break;
				}
			} else {
				// skip blank pages; typically this occurs at end of flash file since its rare to use 100% of flash storage
			}
			pageaddr += pagesize;
			MESSAGE(".");		   		// give progress indication
		}
		MESSAGE("\n");

		if (success)
			MESSAGE("Finished\n");

		delay(100);

		if (success) {
			src->rewind();
			success = _spi_read_flash(src, false);
		}
	} while (0); // this do-while non-loop allows us to break out at any point we have an error

	_spi_end();
	return success;
}

static bool _spi_write_eeprom(ihexStream* src) {
	bool success;
	success = true; // assume we are successful; we change this at any point that we know we failed

	_spi_start();

	do { // a do {...} while(0); loop allows use to break to the end at any point
		const chip_definition_t *target_chip;
		if (!(target_chip = _spi_find_chip())) {
			success = false;
			break;
		}

		uint16_t chipsize = target_chip->eeprom_size;
		uint16_t pagesize = target_chip->eeprom_pagesize;
		uint32_t datasize = src->available();

		// this should not happen
		if (datasize > chipsize) {
			MESSAGE("ERROR: data %d > %d\n", datasize, chipsize);
			success = false;
			break;
		}

		VERBOSE("Chip eeprom: %d Page: %d\n", chipsize, pagesize);
		MESSAGE("EEPROM WRITE %d bytes ", datasize); // deliberately no LF; the start + progress + finish messages are all combined

		//MESSAGE("Starting Flash: ");
		uint32_t addr = 0;

		for (int x = 0; x < datasize; x++) {
			int data = src->read();
			_spi_transaction(0xC0, (addr >> 8) & 0xFF, addr & 0xFF, data);
			delay(45);

			addr++;
			if (!(addr & pagesize))
				MESSAGE("."); // give progress indication
		}
		MESSAGE("\n");

		if (success)
			MESSAGE("Finished\n");

		delay(100);

		if (success) {
			src->rewind();
			success = _spi_read_flash(src, false);
		}
	} while (0); // this do-while non-loop allows us to break out at any point we have an error

	_spi_end();
	return success;
}

// -------------------------------------------------------------------------
// ----------------------------- Public-like Functions --------------------------
// -------------------------------------------------------------------------

// this is for test purposes only and will go away
bool spiInit() {
	pinMode(PIN_SPI_RESET, OUTPUT);
	digitalWrite(PIN_SPI_RESET, HIGH);
	g_spi = NULL;
	_spi_pmode = 0;
	return true;
}

void spiLoop() {
	// noting to do as it is all handled thru TCP methods
}

bool spiIsConnected(Stream* client, bool silent) {
	_spi_start();
	const chip_definition_t *cp = _spi_find_chip();
	_spi_end();
	if (!cp) {
		if (!silent)
			CLIENTMSG("No SPI chip detected\n");
		return false;
	}
	if (!silent)
		CLIENTMSG("Detected %s\n", cp->longname);
	return true;
}

bool spiGetInfo(Stream *client) {
	VERBOSE("Begin spiGetInfo()\n");
	_spi_start();
	const chip_definition_t *cp = _spi_find_chip();
	_spi_end();
	if (!cp) {
		CLIENTMSG("Error: No SPI chip detected\n");
		return false;
	}

	CLIENTMSG("Device info:\n");
	CLIENTMSG("Family: %s\n", cp->family);
	CLIENTMSG("Shortname: %s\n", cp->shortname);
	CLIENTMSG("Longname: %s\n", cp->longname);
	CLIENTMSG("Signature: 1E%04X\n", cp->signature);
	CLIENTMSG("FLASH Size: %d bytes\n", cp->flash_size);
	CLIENTMSG("EEPROM Size: %d bytes\n", cp->eeprom_size);
	if (cp->has_uid) {
#if 1
		char hex_string[((SPI_UID_SIZE + 1) * 2) + 1];
		for (uint i = 0; i < SPI_UID_SIZE; i++)
			sprintf(&(hex_string[i*2]), "%02X", (_spi_chip_uid[i])&0xFF);
		CLIENTMSG("UID: %s\n", hex_string);
#else
		CLIENTMSG("UID: ");
		for (uint i = 0; i < SPI_UID_SIZE; i++)
			CLIENTMSG("%02X", _spi_chip_uid[i]);
		CLIENTMSG("\n");
#endif
	}

	return true;
}

bool spiReadFuses(Stream *client) {
	VERBOSE("Begin spiReadFuses()\n");
	_spi_start();
	const chip_definition_t *cp = _spi_find_chip();
	if (!cp) {
		client->printf("Error: No chip detected\n");
	_spi_end();
		return false;
	}

	uint8_t fuses[SPI_MAX_FUSES];
	//MESSAGE("Reading fuses:");

	fuses[0] = _spi_read_fuse_byte(0x50, 0x00);	// low
	fuses[1] = _spi_read_fuse_byte(0x58, 0x08);	// hight
	fuses[2] = _spi_read_fuse_byte(0x50, 0x08);	// extended
	fuses[3] = _spi_read_fuse_byte(0x58, 0x00);	// protected = lock

	client->printf("Fuses:  ");
	for (int i = 0; i < SPI_MAX_FUSES; i++) {
		client->printf("%s:%d:%02X ", _spi_fuse_names[i], i, fuses[i]);
	}
	client->println("");
	_spi_end();
	return true;
}

bool spiWriteFuse(Stream *client, uint8_t fuse_number, uint8_t fuse_value) {
	VERBOSE("Begin spiWriteFuse()\n");
	_spi_start();
	const chip_definition_t *cp = _spi_find_chip();
	_spi_end();

	if (!cp) {
		client->printf("Error: No chip detected\n");
		return false;
	}

	if (fuse_number >= SPI_MAX_FUSES) {
		client->printf("Error: fuse #%d is out of range of 0..%d\n", fuse_number, SPI_MAX_FUSES);
		return false;
	}
	if (fuse_number == FUSE_PROT) {
		client->println("Nope: we are not going to let you change the lock bits");
		return false;
	}

	client->printf("Current ");
	spiReadFuses(client);
	_spi_start();
	_spi_program_fuse_byte(fuse_number, fuse_value);
	_spi_end();
	client->printf("    New ");
	spiReadFuses(client);
	return true;
}

bool spiFlashData(Stream *client, ihexStream* src) {
	VERBOSE("Begin spiFlashData()\n");
	_spi_start();
	const chip_definition_t *cp = _spi_find_chip();
	if (!cp) {
		client->printf("Error: No chip detected\n");
		_spi_end();
		return false;
	}
	_spi_verified = false;
	bool success = _spi_write_flash(src);
	_spi_end();

	client->println("");
	if (success) {
		uint32_t size;
		src->rewind();
		size = src->available();

		if (size) {
			if (_spi_verified) {
				client->printf("FLASH %d bytes to chip\n", size);
				return true;
			}
		}
		client->printf("Error: FLASH failed, Chip in unknown state\n");
	}
	return false;
}

bool spiDumpFlash(Stream *client, ihexStream* dest) {
	VERBOSE("Begin spiDumpFlash()\n");
	_spi_start();
	const chip_definition_t *cp = _spi_find_chip();
	if (!cp) {
		client->printf("Error: No chip detected\n");
		_spi_end();
		return false;
	}

	dest->clear();
	bool success = _spi_read_flash(dest, true);
	_spi_end();

	client->println("");
	if (success) {
		int32_t size;
		dest->resize(cp->flash_size, cp->flash_pagesize);
		dest->rewind();
		size = dest->available();

		if (size) {
			client->printf("FLASH dumped %d bytes to memory buffer\n", size);
			return true;
		}
		client->printf("Error: FLASH dump: no data available\n");
	}
	return false;
}

bool spiErase(Stream *client) {
	VERBOSE("Begin spiErase()\n");
	_spi_start();
	const chip_definition_t *cp = _spi_find_chip();
	if (!cp) {
		client->printf("Error: No chip detected\n");
		_spi_end();
		return false;
	}
	_spi_erase_chip();
		_spi_end();
	client->printf("Erase finished.\n");
	return true;
}

bool spiWriteEeprom(Stream *client, uint32_t offset, ihexStream* src) {
	VERBOSE("Begin spiWriteEeprom()\n");
	_spi_start();
	const chip_definition_t *cp = _spi_find_chip();
	if (!cp) {
		client->printf("Error: No chip detected\n");
		_spi_end();
		return false;
	}
	_spi_verified = false;

	client->printf("spiWriteEeprom needs new code\n"); // TODO need to limit the write to the available bytes starting at 'offset'
	bool success = _spi_write_eeprom(src);
	_spi_end();

	client->println("");
	if (success) {
		uint32_t size;
		src->rewind();
		size = src->available();

		if (size) {
			if (_spi_verified) {
				client->printf("EEPROM WRITE %d bytes to chip\n", size);
				return true;
			}
		}
		client->printf("Error: EEPROM WRITE failed, Chip in unknown state\n");
	}
	return false;
}

bool spiReadEeprom(Stream * client, uint32_t offset, uint32_t size, ihexStream * dest) {
	VERBOSE("Begin spiReadEeprom()\n");
	_spi_start();

	const chip_definition_t *cp = _spi_find_chip();
	if (!cp) {
		client->printf("Error: No chip detected\n");
		_spi_end();
		return false;
	}

	dest->clear();

	client->printf("spiReadEeprom needs new code\n"); // TODO need to limit the read to the 'size' bytes starting at 'offset'
	bool success = _spi_read_eeprom(dest, true);

	_spi_end();

	client->println("");
	if (success) {
		int32_t size;
		dest->resize(cp->flash_size, cp->flash_pagesize);
		dest->rewind();
		size = dest->available();

		if (size) {
			client->printf("EEPROM READ %d bytes to memory buffer\n", size);
			return true;
		}
		client->printf("Error: EEPROM READ no data available\n");
	}
	return false;
}
