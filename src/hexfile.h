
/*
	Until the most recent ESP32-S4, the RAM was not contiguous.
	If necessary, the internal system will break the requested buffer allocation
	into smaller buffers in an attempt to get the full requested size
	from the fragmented RAM of the ESP32.
	It is possible that as much as 192KB is available.
	Since microchips have FLASH size in powers of 2, 128KB is the realistic upper limit.
*/
#define IHEX_MAX_BUFFERS 16	// needs to be a power of 2

// by tracking 'stored_size' and 'unread_size' we are able to rewind the buffer repeat using it
class ihexStream : public Stream {
  private:
	uint8_t *_blocks[IHEX_MAX_BUFFERS];
	uint8_t _block_count;
	int32_t _block_size;
	int32_t _total_size, _stored_size, _unread_size;
	int32_t _position;
	int32_t _non_filler;
	uint16_t _buffer_errors;
	int32_t _i8hex_lineaddr;
	bool _i8hex_EOD;

	uint8_t _hex_to_char_high(uint8_t c);
	uint8_t _hex_to_char_low(uint8_t c);
	bool _is_EOL(char);
	int _hex_to_num(uint8_t);
	int _read_hex_from_line(char *);
	int _read_hex_from_stream(Stream *);
	int32_t _read_hex(uint8_t);

  public:
	// represents the the maximin possible string when formatted using I8HEX; realistically, the data portion will likely be 32 2B data
	// 1B start code; 1 2B data length; 1 4B address; 1 2B data type; 255 2B max possible data; 1 1B checksum; 1B null terminator
	static const uint16_t I8HEX_LINE_SIZE = ((1 + 2 + 4 + 2 + (255 * 2) + 1) + 1);
	static const uint32_t DEFAULT_SIZE = 256;
	static const uint8_t FILLER_BYTE = 0xFF;

	char LINE_BUFFER[ihexStream::I8HEX_LINE_SIZE]; // we expose this to the user to save duplicating this buffer
	// WARNING: we do not take into account the possibility for leading whitespace in the line buffer

	ihexStream(int32_t _block_size = ihexStream::DEFAULT_SIZE);
	~ihexStream();

	void clear();
	void rewind();

	virtual int32_t available();
	virtual void flush();

	virtual int seek(uint32_t offset);	// set current position
	virtual int peek();					// get current character
	virtual int fetch();				// get current character and increment position
	virtual int read();					// get current character and increment position and decrement available data

	virtual size_t write(uint8_t);	// add data to end of buffer and increment available data
	virtual size_t update(uint8_t); // update data at current position and increment position

	virtual int32_t availableForWrite(void);

	// the following are specialized Intel HEX format methods
	int16_t writeIHexLine(char *line); // writes character array and will add or remove excess trailing whitespace to insure a single new line
	char *readIHexLine();			   // reads into character array and will remove excess trailing whitespace to insure null terminated with no new line

	int32_t resize(int32_t max_size, uint16_t pagesize); // attempt to find actual end of data in buffer
};
