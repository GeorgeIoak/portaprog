/* ***************************************************************************
* File:    config.cpp
* Date:    2020.08.16
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the MIT License
*
* ***************************************************************************** */

/* ---
--------------------------------------------------------------------------

## CONFIG

A JSON syntax config file with each setting on a line and the order is assumed. There is no error checking.

The Order is:
|     Parameter     | Type    | Description                                      |  Required  |
|:-----------------:|---------|--------------------------------------------------|:----------:|
| **ssid**          | string  | SSID of the access point to connect              |  required  |
| **password**      | string  | Password of the access point to connect          |  required  |
| **port**          | integer | TCP Port used in conjunction with the IP address | *optional* |
| **baud**          | integer | Serial baud rate for the connected device        | *optional* |
| **timeout**       | integer | Timeout waiting for serial data from the device  | *optional* |
| **power**         | boolean | Inital state of power for AVR programming        | *optional* |
| **startfile**     | string  | SPIFFS filename to load and run on startup       | *optional* |
| **b1script**      | string  | Command string to run on Actions:1 press         | *optional* |
| **b2script**      | string  | Command string to run on Actions:2 press         | *optional* |
| **b3script**      | string  | Command string to run on Actions:3 press         | *optional* |


The config file name must be `.config` and loaded to the SPIFFS.
The file may be loaded using TCP command set.

The command (from a Linux computer) is:
  - `(echo "upload .config"; cat <your config file>;) | nc <PortaProg IP> <Port>`

The command (from a Windows computer with netcat installed) is:
  - `(echo upload .config & type config_file) | netcat <PortaProg IP> <Port>`

The PortaProg receiver board has two 3-way button controls. One is for navigation and the other is for actions.
The config file allows assignment of scripted commands to the actions buttons.
If no Action button assignments have been provided, then the Action:Select will default to displaying the system Help.

*When the PortaProg is first started and no WiFi has been configured, it will create a hotspot for itself. The default IP address of the hotspot is `192.168.4.1` and the default TCP port is `8888`.*

--- */

#include "allincludes.h"
#include "ArduinoJson.h"

#define CONFIG_FILE ".config"

DynamicJsonDocument configData(1024);

const char configDummy[] = "";
const char configDefaultButtonAction[] = "help";

const char *configWifiSSID;
const char *configWifiPassword;
int configWifiPort;
long configSerialBaud;
int configSerialTimeout;
bool configPower;
const char *configScript;
char configButton2[MAX_BUTTON_STRING+1];
char configButton1[MAX_BUTTON_STRING+1];
char configButton3[MAX_BUTTON_STRING+1];

bool configRead() {

	configWifiSSID = configDummy;
	configWifiPassword = configDummy;
	configScript = configDummy;
	configButton2[0] = 0;
	configButton1[0] = 0;
	configButton3[0] = 0;
	configWifiPort = 0;
	configSerialBaud = 0;
	configSerialTimeout = 0;
	configPower = true;

	if (!filesysExists(CONFIG_FILE))
		return false;

	File cfg = filesysOpen(CONFIG_FILE, "r");
	if (!(cfg)) {
		VERBOSE("Failed to open config file\n");
		return false;
	}

	DeserializationError error;
	error = deserializeJson(configData, cfg);
	if (error != DeserializationError::Ok) {
		MESSAGE("Error %d: problem parsing config file\n", error);
	}

	configWifiSSID = 		configData["ssid"];
	configWifiPassword = 	configData["password"];

	configWifiPort = 		configData["port"];

	configSerialBaud = 		configData["baud"];
	configSerialTimeout = 	configData["timeout"];

	if (!configData["b1script"])
		configPower = true;
	else
		configPower = configData["power"] ? true : false;

	configScript = 			configData["startfile"];

	if (!configData["b1script"])	configButton1[0] = 0;
	else							strncpy(configButton1, configData["b1script"], MAX_BUTTON_STRING);
	if (!configData["b2script"])	configButton2[0] = 0;
	else							strncpy(configButton2, configData["b2script"], MAX_BUTTON_STRING);
	if (!configData["b3script"])	configButton3[0] = 0;
	else							strncpy(configButton3, configData["b3script"], MAX_BUTTON_STRING);

	// for any missing keys, we use our dummy empty string
	if (!configWifiSSID) 		configWifiSSID = configDummy;
	if (!configWifiPassword)	configWifiPassword = configDummy;
	if (!configScript)			configScript = configDummy;

	// if no buttons have been assigned, then we assign 'help' to the select button as a convenience for the user
	if (!configButton2[0] && !configButton1[0] && !configButton3[0])
		strcpy (configButton2, configDefaultButtonAction);

	filesysClose(cfg);
	return true;
}

bool configWrite() {
	char buffer[128];
	const uint8_t *ubuffer = (const uint8_t *)buffer;
	File cfg = filesysOpen(CONFIG_FILE, "w");
	if (!cfg) {
		VERBOSE("Failed to open config file\n");
		return false;
	}

	sprintf(buffer, "{\n");		cfg.write(ubuffer, strlen(buffer));
	if (configWifiSSID[0]) 		{	sprintf(buffer, "'%s': '%s',\n", "ssid", configWifiSSID);			cfg.write(ubuffer, strlen(buffer));	}
	if (configWifiPassword[0])	{	sprintf(buffer, "'%s': '%s',\n", "password", configWifiPassword);	cfg.write(ubuffer, strlen(buffer));	}
	if (configWifiPort) 		{	sprintf(buffer, "'%s': %d,\n", "port", configWifiPort);				cfg.write(ubuffer, strlen(buffer));	}
	if (configSerialBaud) 		{	sprintf(buffer, "'%s': %ld,\n", "baud", configSerialBaud);			cfg.write(ubuffer, strlen(buffer));	}
	if (configSerialTimeout) 	{	sprintf(buffer, "'%s': %d,\n", "timeout", configSerialTimeout);		cfg.write(ubuffer, strlen(buffer));	}
	if (true) 					{	sprintf(buffer, "'%s': %s,\n", "power", (configPower ? "true" : "false"));	cfg.write(ubuffer, strlen(buffer));	}
	if (configScript[0]) 		{	sprintf(buffer, "'%s': '%s',\n", "startfile", configScript);		cfg.write(ubuffer, strlen(buffer));	}
	if (configButton1[0]) 		{	sprintf(buffer, "'%s': '%s',\n", "b1script", configButton1);		cfg.write(ubuffer, strlen(buffer));	}
	if (configButton2[0]) 		{	sprintf(buffer, "'%s': '%s',\n", "b2script", configButton2);		cfg.write(ubuffer, strlen(buffer));	}
	if (configButton3[0]) 		{	sprintf(buffer, "'%s': '%s',\n", "b3script", configButton3);		cfg.write(ubuffer, strlen(buffer));	}
	sprintf(buffer, "}\n"); 	cfg.write(ubuffer, strlen(buffer));

	filesysClose(cfg);

	return true;
}
