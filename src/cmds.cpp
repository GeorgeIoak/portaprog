/* ***************************************************************************
* File:    avr.cpp
* Date:    2020.08.16
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the MIT License
*
* ***************************************************************************** */

/* ---
--------------------------------------------------------------------------

## COMMAND SET

The PortaProg has an extensive command set for interacting with attached AVR chips.
The commands abstract the difference between chips using SPI and chips using UPDI for programming.

- SPI is used for ATTiny and ATMega chips
- UPDI is used for tinyAVR and megaAVR chips

The interface processes an incoming stream and dispatches to the SPI or UPDI specific commands.
The chip type may be specified by command or dynamically determined.
_Specifying the chip type is faster as it saves the time required to interrogate the protocols._

The command interface Stream may be from any source and has been developed using a TCP Stream, SPIFFS File Stream, Action buttons, and an internal Buffer/Screen Stream.
- The file Stream is used to process CMD files from SPIFFS.
- The Buffer/Screen Stream is tied to the device UI and is used to perform the self-contained FLASH function from the device UI.
- This Stream is also used when running command sets stored on the SPIFFS.
- There are 3 actions buttons which may be assigned scripts

*(The examples in the documentation use the TCP Stream.)*

The TCP Stream commands are sent to the programmer using the IP and PORT of the PortaProg.
*(The current `IP address` and `port` are displayed on the display during the startup process.)*

The programming and flash processes display progress messages on the PortaProg display and - in teh case of TCP - completion message(s) back to the caller.

The command set interacts with the attached device, a memory buffer, SPIFFS storage, and a connected stream.

The supported operations work between any two endpoints at a time.

*Some common operations require multiple commands. This was chosen to maximize flexibility while keeping the command set to a reasonable size. An example is to program an attached device using a firmware file on the local computer. This operation uses a TCP connection to first `SEND` the firmware HEX file from the local computer to the PortaProg memory buffer; followed by the `FLASH` command to program the attached device with the firmware in the PortaProg memory buffer. Because the `SEND` and `FLASH` are separate, it is easy to change devices and repeat the `FLASH` quickly without reloaded the firmware.*

The Commands may be provided as independent lines or chained together as a single line operation. The exception to this is a _Stream_ parameter which assumes carrage returns and line feeds are part of the data.

**NOTE:** The POWER commands affect the SPI and UPDI power pins.
The implementation controls a MOSFET on the sGND side because an attached device may receive enough power through UART or other pins to keep the chip active.
If you want an always-on power circuit, use the VCC pin in conjunction with the GND located as part of the UART pin set.

--------------------------------------------------------------------------

--- */

#include "allincludes.h"
#include "avrspi.h"
#include "avrupdi.h"

static avrDevice _avr_device_funcs;

#define MAX_FLASH_SIZE (256 * 1024) // this covers all chips *BUT* is only possible because of the PSRAM on the T4; other devices will attempt half of this

static ihexStream* g_avr_i8hex_stream;

/*
	commands may any of the following:
		discrete commands
		commands with one or more parameter values
		commands with stream content (eg receiveing a file)

	a single line may have multiple commands
	there may be multiple lines
	a stream must be the last line as there is no way of knowing when the file ends
*/

#define AVR_COMMAND_NONE		-1
#define AVR_COMMAND_HELP		0 // this needs to be the first command ID
// setup / config commands
#define AVR_COMMAND_DETECT		1
#define AVR_COMMAND_INFO		2
#define AVR_COMMAND_UPDI		3
#define AVR_COMMAND_SPI			4
#define AVR_COMMAND_UART		5
#define AVR_COMMAND_POWERON		6
#define AVR_COMMAND_POWEROFF	7
#define AVR_COMMAND_POWERTOGGLE	8

#define AVR_COMMAND_B1SCRIPT	41
#define AVR_COMMAND_B2SCRIPT	42
#define AVR_COMMAND_B3SCRIPT	43

// file commands
#define AVR_COMMAND_DIR			11
#define AVR_COMMAND_DEL			12
#define AVR_COMMAND_CAT			13
#define AVR_COMMAND_UPLOAD		14
#define AVR_COMMAND_SEND		15
#define AVR_COMMAND_RECEIVE 	16
#define AVR_COMMAND_LOAD		17
#define AVR_COMMAND_STORE		18

// chip commands
#define AVR_COMMAND_READFUSES	21
#define AVR_COMMAND_SETFUSE		22
#define AVR_COMMAND_ERASE		23
#define AVR_COMMAND_WRITE		24
#define AVR_COMMAND_READ		25
#define AVR_COMMAND_EWRITE		26
#define AVR_COMMAND_EREAD		27

// smoketest commands
#define AVR_COMMAND_CLEAR		31
#define AVR_COMMAND_TEXT		32
#define AVR_COMMAND_UCLEAR		33
#define AVR_COMMAND_UTEXT		34
#define AVR_COMMAND_WAIT		35
#define AVR_COMMAND_BUTTON		36
#define AVR_COMMAND_TEST		37
#define AVR_COMMAND_MUTEON		38
#define AVR_COMMAND_MUTEOFF		39


typedef struct {
	const uint8_t id;
	const char *name;
	const char *parms;
	const char *desc;
	uint8_t args;
	bool has_stream;	// need to know this in case we attempt to skip the command
	bool force_power;	// command required connected device to have power
	bool abortable;		// command is skipped when abort is active
} avrCommand;

// FYI: If you are wondering why the above #defines are added to the data structure it's because I kept reordering the help text and forgetting to renumber the defines


/* ---
### HELP Command

**This command is here because everyone needs a little help now and then**
--- */

static avrCommand _avr_commands[] = {
	// id, command name, parameters, help text, parm count

/* ---
  - `HELP`: returns the full help text of the PortaProg
--- */
	{AVR_COMMAND_HELP, "HELP", "", "return this help text", 0, false, false, false},


/* ---
### SETUP Command

**These commands are used to setup communications with the attached device**
--- */

/* ---
  - `DETECT`: attempts to interrogate both interfaces for an attached device and return what is detected. SPI, UPDI, Both, or Neither. This command is useful for determine proper connections to a chip.
--- */
	{AVR_COMMAND_DETECT, "DETECT", "", "determine connected chip types", 0, false, true, true},
/* ---
  - `INFO`: attempts to interrogate the attached device(s) and return basic details of chip type, available memories, etc.
--- */
	{AVR_COMMAND_INFO, "INFO", "", "return chip information", 0, false, true, true},
/* ---
  - `UPDI`: configure the PortaProg to assume UPDI for subsequent commands.
--- */
	{AVR_COMMAND_UPDI, "UPDI", "", "force UPDI mode", 0, false, false, true},
/* ---
  - `SPI`: configure the PortaProg to assume SPI for subsequent commands.
--- */
	{AVR_COMMAND_SPI, "SPI", "", "force SPI mode", 0, false, false, true},
/* ---
  - `UART`: Set the baud rate of the _UART to attached device_ interface. This interface allows the attached device to send text data to the PortaProg which is displayed on its screen.
--- */
	{AVR_COMMAND_UART, "UART", "<baud> <timeout>", "setup UART (assumes 8N1) with responce timeout", 2, false, false, true},


/* ---
### FILE and BUFFER Command

**These commands are used for read/write operations to the file system and the memory buffer**
--- */

/* ---
  - `DIR`: List all files currently stored on the SPIFFS.
--- */
	{AVR_COMMAND_DIR, "DIR", "", "list files on SPIFFS", 0, false, false, true},
/* ---
  - `DEL` <filename>: Delete the named file from the SPIFFS.
--- */
	{AVR_COMMAND_DEL, "DEL", "<name>", "delete file from SPIFFS", 1, false, false, true},
/* ---
  - `CAT` <filename>: stream contents of file from the SPIFFS back to the local computer standard output.
--- */
	{AVR_COMMAND_CAT, "CAT", "<name>", "stream contents of file", 1, false, false, true},
/* ---
  - `UPLOAD` <filename> (stream): create a new file in the SPIFFS and store the contents of the stream to the file.
	The `stream` is whatever content is send over TCP to the PortaProg. A common method is to pipe the standard output of a command to TCP.
	For example, to send the contents of a file to standard output, use the Linux `cat` command or the windows `type` command.
  	This is a general purpose file operation and no analysis is performed to determine the contents.
	The primary use of the `UPLOAD` command is to store `CMD` files on the SPIFFS but it is available to store any file.
	_NOTE: The command will consume all data available from the stream.	It is not possible to include a follow-up command on the same TCP command-line. Subsequent commands may be send to the PortaProg using a new command-line._
--- */
	{AVR_COMMAND_UPLOAD, "UPLOAD", "<name> (stream)", "save any type of data stream to SPIFFS", 1, true, false, true},
/* ---
  - `SEND`: Send a stream of Intel HEX (I8HEX) format text from the local computer to the PortaProg which converts it to binary data and stores it in the memory buffer.
  	This is typically used to upload firmware which may be flashed to a chip with a subsequent command.
	The I8HEX data format has an explicit _end of data_ marker so it is possible to follow the `SEND` command with another command on the same line. _(see the examples below)_
--- */
	{AVR_COMMAND_SEND, "SEND", "(stream)", "send I8HEX stream to memory buffer", 0, true, false, true},
/* ---
  - `RECEIVE`: Convert the contents of the memory buffer to Intel HEX (I8HEX) format and stream back the resulting text to the local computer standard output.
--- */
	{AVR_COMMAND_RECEIVE, "RECEIVE", "", "send memory buffer as I8HEX to stream", 0, false, false, true},
/* ---
  - `LOAD` <filename>: Load the specified file from the SPIFFS into memory.
  	If the file extension is `HEX` then the file contents are assumed to be in Intel HEX (I8HEX) format
	and are converted to binary and stored in the memory buffer and available to subsequent commands.
	If the file extension is `CMD` then the file contents are processed as if they were TCP commands.
	_Note: This capability would rarely be used from the TCP command line and is intended to allow a series of commands to be initiated from the PortaProg interface._
--- */
	{AVR_COMMAND_LOAD, "LOAD", "<name>", "load I8HEX file from SPIFFS to memory buffer", 1, false, false, true},
/* ---
  - `STORE` <filename>: Convert the contents of the memory buffer into Intel HEX (I8HEX) format and store as a new file to the SPIFFS.
--- */
	{AVR_COMMAND_STORE, "STORE", "<name>", "store memory buffer as I8HEX file to SPIFFS", 1, false, false, true},


/* ---
### AVR Commands

**These commands are used for read/write operations to the attached device**
--- */

/* ---
  - `FUSES`: report back the values of the fuses from the chip.
--- */
	{AVR_COMMAND_READFUSES, "FUSES", "", "read fuses", 0, false, true, true},
/* ---
  - `FUSESET` nn:XX: Set fuse number `nn` to hex value `XX`.
  	_WARNING: No attempt is made to verify the new value. Sending a bad value may render the chip unusable. For this reason, the `lock` fuse may not be changed using the PortaProg._
--- */
	{AVR_COMMAND_SETFUSE, "FUSESET", "nn XX", "write fuse number n with HEX value XX", 2, false, true, true},
/* ---
  - `ERASE`: Initialize the attached device program memory space. This will obviously wipe out any existing program on the chip. _(with great power, comes great responsibility)_
--- */
	{AVR_COMMAND_ERASE, "ERASE", "", "erase chip", 0, false, true, true},
/* ---
  - `WRITE`: flash the attached device using the contents of the memory buffer.
  	The chip type should be specified prior to using the `WRITE` command - either using the `UPDI` or `SPI` commands
	or using `DETECT` to automatically set the connected chip type.
--- */
	{AVR_COMMAND_WRITE, "WRITE", "", "write memory buffer to flash", 0, false, true, true},
/* ---
  - `READ`: read aka dump the contents of the attached device flash memory to the memory buffer.
  	The operation will dump the entire contents of the chip's program memory and
	then attempt to determine the actual size. The result will be rounded up to a _page boundary_ based on the chip's flash memory page size.
	Because the operation must dump the entire program memory, this operation will take longer on larger chips, regardless of the actual size
	of the program.
--- */
	{AVR_COMMAND_READ, "READ", "", "read flash to memory buffer", 0, false, true, true},
/* ---
  - `EWRITE`: write the contents of the memory buffer to the the EEPROM of the attached device.
  	The chip type should be specified prior to using the `EWRITE` command - either using the `UPDI` or `SPI` commands
	or using `DETECT` to automatically set the connected chip type.
--- */
	{AVR_COMMAND_EWRITE, "EWRITE", "offset (stream)", "write HEX stream to eeprom at offset", 1, false, true, true},
/* ---
  - `EREAD`: Read the contents of the attached device eeprom to the memory buffer.
  	The operation will copy the entire attempt contents of the chip's program memory
--- */
	{AVR_COMMAND_EREAD, "EREAD", "offset count", "dump count HEX bytes from eeprom at offset to memory buffer", 2, false, true, true},
/* ---
  - `POWERON`: enable 3.3V programmer VCC+sGND circuit.
--- */
	{AVR_COMMAND_POWERON, "POWERON", "", "3.3V with sGND enabled (default)", 0, false, false, false},
/* ---
  - `POWEROFF`: disable 3.3V programmer VCC+sGND circuit. It may be helpful to disable power after programming when programming raw chips.
--- */
	{AVR_COMMAND_POWEROFF, "POWEROFF", "", "3.3V with sGND disabled", 0, false, false, false},
/* ---
  - `POWERTOGGLE`: toggle state of 3.3V programmer VCC+sGND circuit. This is a UI convenience so only one button is required.
--- */
	{AVR_COMMAND_POWERTOGGLE, "POWERTOGGLE", "", "3.3V with sGND toggled", 0, false, false, false},

/* ---
  - `B1SCRIPT`: update Button 1 command string.
--- */
	{AVR_COMMAND_B1SCRIPT, "B1SCRIPT", "(stream)", "update Button 1 command string (takes effect immediately)", 0, true, false, true},
/* ---
  - `B2SCRIPT`: update Button 2 command string.
--- */
	{AVR_COMMAND_B2SCRIPT, "B2SCRIPT", "(stream)", "update Button 2 command string (takes effect immediately)", 0, true, false, true},
/* ---
  - `B3SCRIPT`: update Button 3 command string.
--- */
	{AVR_COMMAND_B3SCRIPT, "B3SCRIPT", "(stream)", "update Button 3 command string (takes effect immediately)", 0, true, false, true},


/* ---
### SCRIPT Commands

**This commands are used for developing automation scripts.**
--- */
/* ---
  - `CLEAR`: Clear the PortaProg screen.
	This can be used in conjunction with TEXT and other commands to develop PortaProg smoke test scripts.
--- */
	{AVR_COMMAND_CLEAR, "CLEAR", "", "clear display text on screen", 0, false, false, false},
/* ---
  - `TEXT` (stream): Display text on PortaProg screen. The command supports embedding newlines in the message.
	This can be used in conjunction with WAIT, BUTTON, or TEST to develop PortaProg smoke test scripts.
--- */
	{AVR_COMMAND_TEXT, "TEXT", "(stream)", "display text on screen", 0, true, true, false},
/* ---
  - `UTEXT` (stream): Send a text message through the UART connection to the attached device. The command supports embedding newlines in the message.
  	The command will pause for the 'UART timeout' at any newline to give the attached device an opportunity to respond with text output.
	All text received back from the chip is streamed back. The contents of the `UTEXT` send and receive are also displayed on the PortaProg screen.
--- */
	{AVR_COMMAND_UTEXT, "UTEXT", "(stream)", "send text to UART connected device", 0, true, true, true},
/* ---
  - `UCLEAR`: Flush all UART data from the attached device and clear buffer used by `TEST`.
	This can be used in conjunction with and uploaded `CMD` file to develop PortaProg smoke test scripts.
--- */
	{AVR_COMMAND_UCLEAR, "UCLEAR", "", "send text to UART connected device", 0, false, true, true},
/* ---
  - `WAIT`: Cause execution to pause for the specified length of milliseconds.
	This is intended for use in an uploaded `CMD` file to develop PortaProg smoke test scripts.
--- */
	{AVR_COMMAND_WAIT, "WAIT", "<number>", "delay further execution for the number of milliseconds", 1, false, false, true},
/* ---
  - `BUTTON`: Cause execution to pause until the NAV Select button has been pressed.
    A short press will continue with any subsequent commands. A long press will abort and ignore any subsequent commands.
	This is intended for use in an uploaded `CMD` file to develop PortaProg smoke test scripts.
--- */
	{AVR_COMMAND_BUTTON, "BUTTON", "", "wait for NAV Select button press to continue or abort", 0, false, false, true},
/* ---
  - `TEST` <timeout ms> <abort 0:1> (stream): Compare text received from the attached device for the existence of the _stream_ text - wait for up to _timeout_ milliseconds.
  	If the _stream_ text is not detected **and** if the _abort_ is 1 , then all subsequent non-system commands are ignored.
	This is intended for use in an uploaded `CMD` file to develop PortaProg smoke test scripts.
--- */
	{AVR_COMMAND_TEST, "TEST", "<timeout> <abort 0:1> (stream)", "compare device text", 2, true, false, true},
/* ---
  - `MUTEON`: Reduce PortaProg status messaging.
    Some PortaProg commands and operations provide status messages on the device display. These messages may not be helpful while executing scripts.
	This command will suppress those messages.
--- */
	{AVR_COMMAND_MUTEON, "MUTEON", "", "suppress CMD status messages", 0, false, false, false},
/* ---
  - `MUTEOFF`: Restore PortaProg status messaging.
    Some PortaProg commands and operations provide status messages on the device display. These messages may not be helpful white executing scripts.
	This command will restore those messages if `MUTEON` has been performed.
--- */
	{AVR_COMMAND_MUTEOFF, "MUTEOFF", "", "restore CMD status messages", 0, false, false, false}

};

const char *_avr_help_text_usage = "Linux usage: (echo cmd; echo cmd; ...) | nc";	// the IP and port will be appended
const char *_avr_help_text_legend[] = {
	"",
	"(1) chip commands require a chip be connected",
	"(2) write memory to flash must be preceded by a LOAD or FETCH command",
	"(3) write to flash is always followed by a verify operation",
	"(4) I8HEX refers to ASCII data is Intel HEX format for 16bit addressed data",
	"",
	"The system has four data centers:",
	"    the TCP stream",
	"    SPIFFS file system",
	"    a memory buffer",
	"    the chip flash",
	"",
	"Commands provide moving content between these locations.",
	"",
	"Setting the mode to UPDI or SPI is optional but will avoid the detection time.",
	"",
	"The UART command allows sending any text to the device connected to the UART",
	"and receiving back the response. The command uses *responce timeout*.",
	"Possible uses are scripting smoke tests, demonstrations, etc.",
	NULL
};

/* ---

--------------------------------------------------------------------------

#### Examples

**NOTE 1**: the following commands assume a Linux command line. Similar command sequences are possible on Windows.
**NOTE 2**: the `IP` and `PORT` values are shown on the PortaProg screen when it is powered on.

- Program a chip using a firmware file from the local computer
	- `(echo 'receive'; cat file.hex; echo 'write') | nc IP PORT`
- Dump the program memory of a chip to a HEX file on the local computer
	- `(echo 'read'; echo 'send') | nc IP PORT > file.hex`
- Upload firmware file `file.hex` to SPIFFS
	- `(echo 'send'; cat file.hex; echo 'store file.hex') | nc IP PORT",
- Send text commands over a UART connection to the program running on attached device
  	_(assumes the running program has an active `Serial` connection open and understand the messages -
	in this example, the program on the chip understands the commands `LED`, `ON`, and `OFF` followed by a number and responds with the state of the LED)_
	- `echo -e 'UTEXT LED 1\nON 2\nOFF 1' | nc IP PORT`
- Update config *(the local file name does not need to be the same as the file stored to SPIFFS)*
	- (echo 'upload .config'; cat config_file) | nc IP PORT'

 #### Button Examples

- Assign a button to load firware from SPIFFS to memory and program a chip from memory
	- B1SCRIPT LOAD firmware.hex WRITE
- Assign a button to program a chip using the firware already in memory _(this is handy for repeat programming tasks)_
	- B2SCRIPT WRITE

#### Script Examples

The following examples are from the ECCCore smoketest and includes comments for each line. _(Note: script files do not support comments)_

In this example, the `.config` file runs `startfile.cmd`. The `startfile.cmd` preloads the firmware and assigns scripted commands to the 3 programmable buttons.

|     File     | Contents    | Description    |
|:------------:|-------------|----------------|
| `.config`    | {<br>"ssid": "WIFI",<br>"password":&nbsp;"wifi-password",<br>"port":&nbsp;8888,<br>"baud":&nbsp;9600,<br>"timeout":&nbsp;500,<br>"startfile":&nbsp;"startfile.cmd"<br>}  | the important JSON data here is the `startfile` assignment which will be run when the PortaProg starts up  |
| `startfile.cmd` | POWEROFF<br>UPDI<br>LOAD&nbsp;smoketest.hex<br>B1SCRIPT&nbsp;LOAD&nbsp;smoketest.cmd<br>B2SCRIPT&nbsp;CAT&nbsp;smoketest.cmd<br>B3SCRIPT&nbsp;POWERTOGGLE  | The script insures power has been turned off to the attached device<br>it sets the programming mode to UPDI<br>it preloads the firmware file from SPIFFS into memory<br>it assigns the `smoketest.cmd` script to run when button 1 is pressed<br><br>for convenience it assigned the power toggle feature to button 3 |
| `smoketest.cmd` | UCLEAR<br>POWEROFF<br>UPDI<br>CLEAR<br>WRITE<br>MUTEON<br>CLEAR<br>POWERON<br>TEST&nbsp;5000&nbsp;1&nbsp;BUZZER<br>UTEXT&nbsp;UART<br>TEST&nbsp;6000&nbsp;1&nbsp;SMOKETEST<br>WAIT&nbsp;500<br>POWEROFF<br>MUTEOFF | the script starts by attempting to clear any data on the UART connection to the attached device<br>it insures the PortaProg is not powering the device<br>it sets the programming mode to UPDI<br>it clears the PortaProg screen<br>it writes the firmware from memory to the device chip<br>it mutes any status messages the PortaProg may normally send to its screen _(this is so the PortaProg screen only shows messages coming from the attached device)_<br>it clears the PortaProg screen in preparation for executing the device smoketest<br>it powers the attached device on _(which immediately start to execute its smoketest code)_<br>it waits up to 5 seconds for the text `BUZZER` to be received over the UART from the device; it will abort the rest of the script if the text is not received.<br>it sends the text `UART` to the device _(the devcie smoketest is validates the UART connection if it receives this text)_<br>it waits up to 6 seconds for the device smoketest to send the text `SMOKETEST` _(which occurs at the end of the device code)_; it will abort the rest of the script if the text is not received.<br>it waits another 1/2 second<br>it then shuts off power to the device<br>it restores PortaProg to display status messages |
--------------------------------------------------------------------------

--- */

const char *_avr_help_text_examples[] = {
	"",
	"Examples:",
	"Flash local FILE.HEX to chip :\n(echo 'receive'; cat file.hex; echo 'flash') | nc IP PORT",
	"",
	"Dump the chip firmware to local FILE.HEX:",
	"(echo 'dump'; echo 'send') | nc IP PORT > file.hex",
	"",
	"Update config (linux):",
	"(echo 'upload .config'; cat config_file) | nc IP PORT",
	"",
	"Initial config over access point (windows):",
	"(echo upload .config & type config_file) | netcat 192.168.4.1 8888",
	"",
	NULL
};

static bool g_avr_have_power;
static uint32_t g_avr_uart_baud;
static uint16_t g_avr_uart_timeout;
static bool g_arv_uart_cmd;	// flag that the CMD processor needs the UART

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

static bool _avr_trim(char *line) {
	for (int i = strlen(line) - 1; i > 0; i--) {
		if ((line[i] == '\n') || (line[i] == '\r') || (line[i] == '\t') || (line[i] == ' '))
			continue;
		line[i + 1] = 0;
		return true;
	}
	return false;
}

static bool _is_printable(char *line) {
	// sanity check: make sure there are printable characters available
	if (!strlen(line))
		return true;
	bool printable = false;
	for (int i = 0; i < strlen(line); i++) {
		if (isprint(line[i])) {
			printable = true;
			break;
		}
	}
	return printable;
}

static bool _avr_uart_setup(uint32_t baud, uint16_t timeout) {
	if (baud != g_avr_uart_baud) {
		if (g_avr_uart_baud == 0) {
			// first time
			delay(10);
			// CHIPSERIAL is the second available (third overall) UART on the ESP32 - used for communicating to an ECC
			CHIPSERIAL.begin(baud, SERIAL_8N1, PIN_UART2_RX, PIN_UART2_TX);
			while (!CHIPSERIAL)
				; // wait for serial attach
			pinMode(PIN_UART2_RX, INPUT_PULLUP);		//set this may solve noise on the RX when no external device is connected
		} else {
			// already have the serial setup, just need to change the baud rate
			CHIPSERIAL.updateBaudRate(baud);
		}
		// we may get a bit of garbage from the first button push so let's clear it now
		delay(2);
		SERIAL_FLUSH(CHIPSERIAL);
	}
	g_avr_uart_baud = baud;
	g_avr_uart_timeout = timeout;
	return (CHIPSERIAL ? true : false);
}

#define MAX_UART_BUF 127
static char g_last_received_string[MAX_UART_BUF+1];	// we keep the most recent received data so it can be used for smoketest TEST command

static bool _uartLoop(bool to_screen, char* match) {
	bool found = false;
	if (match == NULL)
		found = true;

	if (g_avr_uart_baud == 0)
		return found;
#if 0	// we now are using this everywhere
	if (g_arv_uart_cmd)
		return found;
#endif
	char buffer[MAX_UART_BUF+1];
	int16_t len = 0;

	// NOTE: this is a convenience operation; it was RX only but we try to handle TX ... try!

	if (DEBUGSERIAL.available()) {
		len = streamReadLine(&(DEBUGSERIAL), buffer, MAX_UART_BUF, false);
		if (len) {
			CHIPSERIAL.print(buffer);
		}
		else {
			CHIPSERIAL.print("\r\n");
		}
	}

	len = streamReadLine(&(CHIPSERIAL), buffer, MAX_UART_BUF, false);
	if (len) {
		//_avr_trim(buff);
		int16_t shortened = len;
		FILESYS_STRIP_NL(buffer, len);
		shortened -= len;	// track if we cleared of a newline

		if (_is_printable(buffer)) {
			VERBOSE("uart: [%02d] {%s}\n", len, buffer);

			// we may need to temporarily override quietmode if we have the explicit to_screen parameter
			bool was_quiet = ioIsMute();
			if (to_screen)
				ioMuteOff();

			if (shortened) {
				MESSAGE("%s\n", buffer);
			} else {
				MESSAGE("%s", buffer);
			}

			if (was_quiet)
				ioMuteOn();


			if (len) {
				strcpy(g_last_received_string, buffer);
				if ((match != NULL) && (!found)) {
					if (strstr(g_last_received_string, match) != NULL) {
						found = true;
						VERBOSE("TEST: %s is in  %s\n", match, g_last_received_string);
					}
					else {
						VERBOSE("TEST: %s not in %s\n", match, g_last_received_string);
					}
				}
			}
		}
#if 1
		else {
			// display HEX for for binary data but attempt to filter noise from a floating UART
			if (!((buffer[0] == 0) || (buffer[0] == 0xFF))) {
			for (int i = 0; i < len; i++)
				ioStreamPrintf(NULL, "%02X", buffer[i]);
			ioPrint("\n");
			}
		}
#endif
	}
	return found;
}

static bool _avr_help(Stream *client) {
	if (client) {
		// temporarily configure onboard display to not wrap because the most important content is the beginning of each line
		ioWrapEnable(false);

		uint8_t entries = (sizeof(_avr_commands) / sizeof(avrCommand));

		for (int i = 0; i < entries; i++) {
			// to save some scrolling, we do not pad the help text on the display
			//MESSAGE("%s %s %s\n", _avr_commands[i].name, _avr_commands[i].parms, _avr_commands[i].desc);
			ioStreamPrintf(client, "%-8s %-16s - %s\n", _avr_commands[i].name, _avr_commands[i].parms, _avr_commands[i].desc);
		}
		for (int i = 0; _avr_help_text_legend[i] != NULL; i++) {
			ioStreamPrintf(client, "%s\n", _avr_help_text_legend[i]);
		}
		for (int i = 0; _avr_help_text_examples[i] != NULL; i++) {
			ioStreamPrintf(client, "%s\n", _avr_help_text_examples[i]);
		}

		// restore wrapping
		ioWrapEnable(true);

		// full formatted content back to client
		ioStreamPrintf(client, "%s %s %d\n", _avr_help_text_usage, wifiAddress(), configWifiPort);
		ioStreamPrintf(client, "BTN1 = %s\n", configButton1);
		ioStreamPrintf(client, "BTN2 = %s\n", configButton2);
		ioStreamPrintf(client, "BTN3 = %s\n", configButton3);
	}
	return true;
}

static int _avr_detect_chip(Stream *client, bool force) {
	int found = AVR_DEVICE_NONE;

	if (_avr_device_funcs.dev_type == AVR_DEVICE_NONE)
		force = true;
	else {
		// return if we already know what we have
		return _avr_device_funcs.dev_type;
	}

	// if we have not detected either interface, then test for both
	ioStreamPrintf(client, "Checking for SPI device ...  ");
	if (spiIsConnected(client, false)) {
		found = AVR_DEVICE_SPI;
		avrDeviceSet(AVR_DEVICE_SPI);
		ioStreamPrintf(client, "Detected SPI device\n");
	}

	ioStreamPrintf(client, "Checking for UPDI device ... ");
	if (updiIsConnected(client, false)) {
		if (found != AVR_DEVICE_NONE) {
			found = -1;
			avrDeviceSet(AVR_DEVICE_NONE);
		}
		else {
			found = AVR_DEVICE_UPDI;
			avrDeviceSet(AVR_DEVICE_UPDI);
			ioStreamPrintf(client, "Detected UPDI device\n");
		}
	}

	// TODO keep adding programming protocols as they get implemented

	if (found == AVR_DEVICE_NONE)
		ioStreamPrintf(client, "No chips detected\n");
	if (found < 0)
		ioStreamPrintf(client, "Multiple chips detected\n");

	return _avr_device_funcs.dev_type;
}

static bool _avr_get_info(Stream *client) {
	if (_avr_detect_chip(client, false))
		return (*_avr_device_funcs.pgmrGetInfo)(client);

	ioStreamPrintf(client, "\n");
	ioStreamPrintf(client, "IMPORTANT: Only one chip may be connected when using autodetect.\n");
	ioStreamPrintf(client, "           Use the SPI or UPDI command to manually set the mode.\n");
	return false;
}

static bool _avr_read_fuses(Stream *client) {
	if (_avr_detect_chip(client, false) > 0)
		return (*_avr_device_funcs.pgmrReadFuses)(client);
	return false;
}

static bool _avr_write_fuse(Stream *client, uint8_t fuse_number, uint8_t fuse_value) {
	if (_avr_detect_chip(client, false) > 0)
		return (*_avr_device_funcs.pgmrWriteFuse)(client, fuse_number, fuse_value);
	return false;
}

static bool _avr_erase(Stream *client) {
	if (_avr_detect_chip(client, false) > 0)
		return (*_avr_device_funcs.pgmrErase)(client);
	return false;
}

static bool _avr_flash_to_memory(Stream *client) {
	if (_avr_detect_chip(client, false) > 0) {
		g_avr_i8hex_stream->clear();
		return (*_avr_device_funcs.pgmrDumpFlash)(client, g_avr_i8hex_stream);
	}
	return false;
}

static bool _avr_memory_to_flash(Stream *client) {
	if (_avr_detect_chip(client, false) > 0)
		return (*_avr_device_funcs.pgmrFlashData)(client, g_avr_i8hex_stream);
	return false;
}

static bool _avr_eeprom_to_stream(Stream *client, uint32_t offset, uint32_t size, Stream* dest) {
	if (_avr_detect_chip(client, false) <= 0)
		return false;

	return (*_avr_device_funcs.pgmrReadEeprom)(client, offset, size, g_avr_i8hex_stream);
#if 0
	g_avr_i8hex_stream->clear();
	if ((*_avr_device_funcs.pgmrReadEeprom)(client, g_avr_i8hex_stream)) {
		if (!g_avr_i8hex_stream->available()) {
			ioStreamPrintf(client, "Memory buffer is empty\n");
			return false;
		}
		g_avr_i8hex_stream->rewind();
		g_avr_i8hex_stream->seek(offset);

		int32_t count = 0;
		int c = -1;
		for (count = 0; count < size; count++) {
			c = g_avr_i8hex_stream->fetch();
			if (c < 0)
				break;

			ioStreamPrintf(client, "%02X", c);	// output to display
			if (client != dest)
				dest->printf("%02X", c); // output to destination (but don't duplicate output)
		}
		if (count) {
			ioStreamPrintf(client, "\n");	// output to display
			if (client != dest)
				dest->printf("\n"); // end stream of HEX characters
		}
		return true;
	}
	return false;
#else
	return true;
#endif
}

static bool _avr_stream_to_eeprom(Stream *client, uint32_t offset, Stream* src) {
	if (_avr_detect_chip(client, false) <= 0)
		return false;

	g_avr_i8hex_stream->clear();

	int32_t count = 0;
	unsigned int byte = 0;

	while (src->available()) {
		char hex[3];

		// we read two characters, ignoring linefeed and ignoring leading space and newline
		if ((hex[0] = src->read()) <= 0) break;
		if ((hex[0] == '\r')) continue;
		if ((hex[0] == ' ') || (hex[0] == '\n')) {	if (!count) continue; else break; }
		if ((hex[1] = src->read()) <= 0) break;
		if ((hex[1] == '\r')) continue;
		if ((hex[1] == ' ') || (hex[1] == '\n')) {	if (!count) continue; else break; }
		hex[2] = 0;

		if (!(sscanf(hex, "%2X", &byte) == 1))
			byte = 0xFF;
		g_avr_i8hex_stream->write(byte);
		count++;
	}
	if (count) {
		g_avr_i8hex_stream->rewind();
		return (*_avr_device_funcs.pgmrWriteEeprom)(client, offset, g_avr_i8hex_stream);
	}
	ioStreamPrintf(client, "missing data to write\n");
	return false;
}

static bool _avr_memory_to_stream(Stream *client, Stream* dest) {
	char *line;
	int32_t count = 0;
	g_avr_i8hex_stream->rewind();

	if (!g_avr_i8hex_stream->available()) {
		// buffer is empty
		ioStreamPrintf(client, "Memory buffer is empty\n");
		return false;
	}

	while ((line = g_avr_i8hex_stream->readIHexLine()) != NULL) {
#if 0	// too slow
		ioStreamPrintf(client, "%s\n", line);	// output to display
		if (dest != client)
#endif
		count++;
		dest->printf("%s\n", line); // output to destination (but don't duplicate output)
		if (!(count % 8))
			MESSAGE(".");
	}

	MESSAGE("\n"); // end the progress '.' dots

	return true;
}

static uint32_t _avr_stream_to_memory(Stream *client, Stream* src) {

	g_avr_i8hex_stream->clear();

	bool success;
	int32_t len;
	int32_t total = 0;
	int32_t count = 0;

	// read thru all lines of the stream until we read the 'end record' indicator
	do {
		success = false;
		char *lineptr = g_avr_i8hex_stream->LINE_BUFFER;
		len = src->readBytesUntil('\n', lineptr, g_avr_i8hex_stream->I8HEX_LINE_SIZE);

		if (!len) {
			ioStreamPrintf(client, "Reached EOF before reading final I8HEX marker\n");
			break;
		}

		if (len == g_avr_i8hex_stream->I8HEX_LINE_SIZE)
			len--;		  // make room for nul terminator
		lineptr[len] = 0; // readBytesUntil() doe not null terminate the buffer
		_avr_trim(lineptr);

		len = g_avr_i8hex_stream->writeIHexLine(lineptr);

		if (len == 0 /* we read the end-of-ihex marker */) {
			success = true;
			break;
		}

		if (len < 0 /* some error ocurred */) {
			ioStreamPrintf(client, "Failed to process I8HEX data\n");
			break;
		}
		total += len;
		success = true;
		count++;
		if (!(count % 8))
			MESSAGE(".");
	} while (success);

	MESSAGE("\n"); // end the progress '.' dots

	if (success) {
		int32_t reported;
		g_avr_i8hex_stream->rewind();
		reported = g_avr_i8hex_stream->available(); // return the amount of available data we are holding
		VERBOSE("streamed %d bytes, buffer has %d bytes\n", total, reported);
		if (reported != total)
			ioStreamPrintf(client, "Error: streamed %d bytes != memory %d bytes\n", total, reported);
		return reported;
	}

	// reset everything and return 0 if we did not successfully load the entire file
	g_avr_i8hex_stream->clear();
	return 0;
}

static bool _avr_memory_to_file(Stream* client, char *filename) {
	g_avr_i8hex_stream->rewind();
	if (!g_avr_i8hex_stream->available()) {
		ioStreamPrintf(client, "Error: memory buffer is empty\n");
		return false;
	}
	File output = filesysOpen(filename, "w");
	if (!output) {
		ioStreamPrintf(client, "Error: unable to open %s\n", filename);
		return false;
	}

	bool success = false;
	// we treat the File like a stream
	success = _avr_memory_to_stream(client, (Stream *)(&output));

	if (!success)
		ioStreamPrintf(client, "Error: memory buffer appears empty\n");
	else {
		g_avr_i8hex_stream->rewind();
		ioStreamPrintf(client, "Stored %d bytes to %s\n", g_avr_i8hex_stream->available(), filename);
		success = true;
	}
	filesysClose(output);
	// displayClear(true);
	return success;
}

static bool _avr_file_to_memory(Stream* client, char* filename) {
	bool success = false;

	File input = filesysOpen(filename, "r");
	if (!input) {
		ioStreamPrintf(client, "Error: unable to open %s\n", filename);
		return false;
	}

	ioStreamPrintf(client, "Loading %s ...\n", filename);

	uint32_t stored = _avr_stream_to_memory(client, (Stream *)(&input));
	// we treat the File like a stream
	if (!stored)
		ioStreamPrintf(client, "Error: memory buffer appears empty\n");
	else {
		g_avr_i8hex_stream->rewind();
		ioStreamPrintf(client, "Loaded %d bytes from %s\n", g_avr_i8hex_stream->available(), filename);
		success = true;
	}
	filesysClose(input);
	// displayClear(true);
	return success;
}

// create a file with the contents of the stream *without* any interpretation other than excess whitespace
static bool _avr_save_stream_to_file(Stream* client, char* filename) {
	MESSAGE("Save stream to file %s\n", filename);
	static char buffer[256+1];  // need space for null terminator

	bool success = false;
	int32_t len;

	File f = filesysOpen(filename, "w");
	if (!f) {
		ioStreamPrintf(client, "Error: unable to write to file %s\n", filename);
	}
	else
		success = true;

	do {
		int c;
		len = 0;
		while (client->available()) {
			c = client->read();
			DEBUG("%c", c);

			if (c == '\r')
				continue;
			if ((c == ' ') || (c == '\n')) {
				if (!len) // we can throw away leading white space
					continue;
			}
			if (c == 0)
				break;
			buffer[len++] = c;

			if (len == 256)
				break;	// the result will not be pretty but we need to break before overflow of the buffer
		}

		if (len > 0) {
			buffer[len++] = '\n';	// newline
			buffer[len++] = 0;		// null terminate the string

			// write the buffer to the file
			if (f) {
				// the line buffer contains STREAM data to append to the active file
				f.println(buffer);
				VERBOSE("write: %s", buffer);
			}
		}
		else {
			VERBOSE("OOPS: we should not have an empty line\n");
		}
	}
	while (len > 0);

	filesysClose(f);
	return success;
}

static bool _avr_read_file_to_stream(Stream* client, char* filename) {
	MESSAGE("read file to stream %s\n", filename);
	static char buffer[256+1];  // need space for null terminator
	bool success = false;

	File input = filesysOpen(filename, "r");
	if (!input) {
		ioStreamPrintf(client, "Error: unable to open %s\n", filename);
		return false;
	}

	uint32_t len;
	while ((len = streamReadLine(&input, buffer, 256, false)) > 0) {
		FILESYS_STRIP_NL(buffer, len);
#if 1	// too slow for long files
		ioStreamPrintf(client, "%s\n", buffer);	// output to display
#else
		client->printf("%s\n", buffer);	// output to destination (but don't duplicate output)
#endif
	}

	filesysClose(input);
	// displayClear(true);
	return success;
	}



// --------------------------------------------------------------------------------------------
// AVR public functions
// --------------------------------------------------------------------------------------------

/* ---
#### avrPowerOn() / avrPowerOff()

Provide 3.3V across the VCC/sGND circuit.

The PROGRAM PWR is controllable. However, since there may be power bleed through on UART or UPDI pins, we actual switch the GND.
It may be used for cases where the chip should not execute its own code outside of the AVR API functions.

--- */
void avrPowerOn() {
	MESSAGE("Power On\n");
	pinMode(PIN_PWR_SW, OUTPUT);
	digitalWrite(PIN_PWR_SW, HIGH);
}
void avrPowerOff() {
	MESSAGE("Power Off\n");
	pinMode(PIN_PWR_SW, OUTPUT);
	digitalWrite(PIN_PWR_SW, LOW);
}

bool avrPowerIsActive() {
	return g_avr_have_power; //	can't use digitalRead(PIN_PWR_SW) because we want to know the requested state rather than the actual state
}


/* ---
#### avrDeviceSet()

Configure commands for specific programmer interface

The programming interface can be one of SPI, UPDI, SWD, or JTAG - used defitions in `cmds.h`

--- */
void avrDeviceSet(uint8_t dev_type) {
	switch (dev_type) {
		case AVR_DEVICE_SPI: {
			_avr_device_funcs.pgmrInit = &spiInit;
			_avr_device_funcs.pgmrLoop = &spiLoop;
			_avr_device_funcs.pgmrIsConnected = &spiIsConnected;
			_avr_device_funcs.pgmrGetInfo = &spiGetInfo;
			_avr_device_funcs.pgmrReadFuses = &spiReadFuses;
			_avr_device_funcs.pgmrWriteFuse = &spiWriteFuse;
			_avr_device_funcs.pgmrFlashData = &spiFlashData;
			_avr_device_funcs.pgmrDumpFlash = &spiDumpFlash;
			_avr_device_funcs.pgmrErase = &spiErase;
			_avr_device_funcs.pgmrWriteEeprom = &spiWriteEeprom;
			_avr_device_funcs.pgmrReadEeprom = &spiReadEeprom;
			_avr_device_funcs.dev_type = AVR_DEVICE_SPI;
		} break;
		case AVR_DEVICE_UPDI: {
			_avr_device_funcs.pgmrInit = &updiInit;
			_avr_device_funcs.pgmrLoop = &updiLoop;
			_avr_device_funcs.pgmrIsConnected = &updiIsConnected;
			_avr_device_funcs.pgmrGetInfo = &updiGetInfo;
			_avr_device_funcs.pgmrReadFuses = &updiReadFuses;
			_avr_device_funcs.pgmrWriteFuse = &updiWriteFuse;
			_avr_device_funcs.pgmrFlashData = &updiFlashData;
			_avr_device_funcs.pgmrDumpFlash = &updiDumpFlash;
			_avr_device_funcs.pgmrErase = &updiErase;
			_avr_device_funcs.pgmrWriteEeprom = &updiWriteEeprom;
			_avr_device_funcs.pgmrReadEeprom = &updiReadEeprom;
			_avr_device_funcs.dev_type = AVR_DEVICE_UPDI;
		} break;
		default: {
			memset(&_avr_device_funcs, 0, sizeof (avrDevice));
			_avr_device_funcs.dev_type = AVR_DEVICE_NONE;
		} break;
	}
}

/* --
#### cmdsInit()

Performs all necessary initialization of programming systems.
Must be called once before using any of the other AVR operations.

- return: **bool** `true` on success and `false` on failure

-- */
bool cmdsInit() {
	// setup a controlled power source based off the 'power' config setting
	if (configPower) {
		avrPowerOn();
		g_avr_have_power = true;
	} else {
		avrPowerOff();
		g_avr_have_power = false;
	}

	g_arv_uart_cmd = false;
	g_avr_uart_baud = 0;
	g_avr_uart_timeout = 0;	// milliseconds

	g_avr_i8hex_stream = NULL;
	g_avr_i8hex_stream = new ihexStream(MAX_FLASH_SIZE);

	g_last_received_string[0] = 0;

	DEBUG("AVR FLASH buffer is %d bytes\n", g_avr_i8hex_stream->availableForWrite());

	if (configSerialBaud && configSerialTimeout)
		_avr_uart_setup(configSerialBaud, configSerialTimeout);

	updiInit();
	spiInit();
	avrDeviceSet(AVR_DEVICE_NONE);

	MESSAGE("Usage:\necho 'help' | nc %s %d\n", wifiAddress(), configWifiPort);

	if (configWifiSSID[0] == 0) {
		MESSAGE("\n");
		MESSAGE("If this is the first time using the PortaProg,\nupload a .config file to setup WiFi.\nSee documentation for settings.\n");
	}

	// if there is a config script available, we want to run it
	if (configScript[0]) {
		MESSAGE("Running startup script %s\n", configScript);
		ioRunCommandFile(configScript, false);
	}

	MESSAGE("Button 1: %s\n", (strlen(configButton1) > 0) ? configButton1 : "not set");
	MESSAGE("Button 2: %s\n", (strlen(configButton2) > 0) ? configButton2 : "not set");
	MESSAGE("Button 3: %s\n", (strlen(configButton3) > 0) ? configButton3 : "not set");

	return true;
}

/* --
#### cmdsLoop()

Give the CMDS programming processes - SPI or UPDI - an opportunity to respond to basic events.

**NOTE:*** the TCP interface operates from the WiFi loop process and calls the published AVR API functions.
-- */

void cmdsLoop() {
	if (_avr_device_funcs.dev_type)
		(*_avr_device_funcs.pgmrLoop)(); // you can double check but this is most likely a nop

	_uartLoop(true, NULL); // this lets the PortaProg receive and display UART messages; it is RX-only
}

/* --
#### cmdsProcessCommands()

Process a Stream of commands. The most common Stream source is the TCP data. Any Stream is
supported provided it implements read(), write(), and all print...() methods.

- input: client **Stream ptr** an active Stream with the commands and data to be processes
- input: aborted **bool** indicates a prior command aborted
-- */

bool cmdsProcessCommands(Stream *client, bool aborted) {

	if (!client) {
		// oops - we cant work without a connected client
		MESSAGE("AVR processing client not available");
		return false;
	}

	// Get data from the client and process it
	uint32_t total = 0; // for diagnostics // TODO remove once things are working
	uint16_t len = 0;
	char c = 0;
	char *linebuffer = g_network_buf;
	int8_t command_id = AVR_COMMAND_NONE;
	avrCommand *active_cmd = NULL;
	uint8_t command_index = 0;
	bool needs_power = false;
	bool expecting_args, active_file;
	bool abort_processing = aborted;
	int8_t args = 0;
	uint32_t data_size_processed = 0;

	do {
		if (command_id == AVR_COMMAND_NONE) {
			expecting_args = false;
			active_file = false;
			args = 0;
			len = 0;
			command_index = 0;
			needs_power = false;
			active_cmd = NULL;
		}

		// with the exception of a stream, commands do not split across lines. commands with streams will start the stream on a new line.
		while (client->available()) {
			// we process commands and arguments the same - both assume whitespace as a delimeter
			//VERBOSE("?");
			c = client->read();
			total++;
			//VERBOSE("%c", c);

			if (c == 0)
				break;
			if (c == '\r')
				continue;
			if ((c == ' ') || (c == '\n')) {
				if (!len) // we can throw away leading white space
					continue;
				break;
			}

			linebuffer[len++] = c;
			// we chunk large streams of data
			if (len >= MAX_NETWORK_TEXT)
				break;
		}

		VERBOSE("parsed [%s]\n", linebuffer);

		if (len) {
			linebuffer[len++] = 0;
			//len = 0; // this will force the outer loop to start reading a new command or parameter
			//VERBOSE("parsed string: %s\n", linebuffer);

			// if we do not have an active command, then we look for one
			if (command_id < 0) {
				// no active command so we look for a match
				for (command_index = 0; command_index < (sizeof(_avr_commands) / sizeof(avrCommand)); command_index++) {
					// if we match a command from our list, make a note of it
					if (strcasecmp(_avr_commands[command_index].name, linebuffer) == 0) {
						active_cmd = &(_avr_commands[command_index]);
						command_id = _avr_commands[command_index].id;
						needs_power = _avr_commands[command_index].force_power;
						break;
					}
				}
			}
			// if we still don't have a command, then its an error
			if (!active_cmd) {
				ioStreamPrintf(client, "Error, unrecognized command: [%s]\n\n", linebuffer);
				client->flush();
				command_id = AVR_COMMAND_NONE; // output the help text
			}
			else {
				// if we received a command which needs args we can just continue the processing loop for the first arg
				// NOTE: the command is responsible for subsequent args
				if ((active_cmd->id > AVR_COMMAND_HELP) && (active_cmd->args > args)) {
					// TODO clean up this code so it isn't checking the arg count three times !
					if (!expecting_args) {
						expecting_args = true;
						len = 0;
					} else {
						args++; // we have an arg
						if (active_cmd->args > args) {
							// we still meed more args so ...
							len--;					 // we remove the null terminator
							linebuffer[len++] = ' '; // and replace it with a space
						}
					}
					VERBOSE("%s need %d arg(s) has %d = %s\n", active_cmd->name, active_cmd->args, args, linebuffer);
					if (active_cmd->args > args)
						continue;
				}

				VERBOSE("checking command: #%d %s\n", command_id, linebuffer);

				// if a command requires power and its not active, make it so
				if (needs_power && !avrPowerIsActive()) {
					avrPowerOn();
					DEBUG("Power required and not yet active\n");
				}

				// process the designated command
				// any required args - with the expection of a stream - are contiguous in the linebuffer buffer and can be parsed by the command
				if (abort_processing) {
					if (active_cmd->abortable) {
						VERBOSEON("Aborting CMD: %s %s\n", active_cmd->name, linebuffer);
						// skip everything else if this command has a stream as its last parameter
						if (active_cmd->has_stream) {
							while (client->available())
							c = client->read();
						}
						command_id = AVR_COMMAND_NONE;
					}
					else {
						VERBOSEON("Non-Abortable CMD: %s %s\n", active_cmd->name, linebuffer);
					}
				}
			}

			switch (command_id) {
				case AVR_COMMAND_NONE: {
				} break;
				// informational operations
				case AVR_COMMAND_HELP: {
					_avr_help(client);
					command_id = AVR_COMMAND_NONE;
				} break;

				case AVR_COMMAND_UART: {
					// linebuffer now has the first (only) arg = baud rate
					int baud;
					int timeout;
					if (sscanf(linebuffer, "%d %d", &baud, &timeout) == 2) {
						VERBOSE("UART: %d Baud %d timeout\n", baud, timeout);
						if (!timeout)
							timeout = 250;
						if (!_avr_uart_setup(baud, timeout))
							ioStreamPrintf(client, "Error: unable to setup UART\n");
						else
							ioStreamPrintf(client, "UART setup for %d 8N1, %d ms timeout\n", baud, timeout);
					} else
						ioStreamPrintf(client, "Error: unrecognized Baud or timeout: [%s]\n", linebuffer);
					command_id = AVR_COMMAND_NONE;
				} break;

				case AVR_COMMAND_DETECT: {
					_avr_detect_chip(client, true);
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_INFO: {
					_avr_get_info(client);
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_UPDI: {
					avrDeviceSet(AVR_DEVICE_UPDI);
					ioStreamPrintf(client, "Programmer set to UPDI mode\n");
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_SPI: {
					avrDeviceSet(AVR_DEVICE_SPI);
					ioStreamPrintf(client, "Programmer set to SPI mode\n");
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_POWERON: {
					avrPowerOn();
					g_avr_have_power = true;
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_POWEROFF: {
					avrPowerOff();
					g_avr_have_power = false;
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_POWERTOGGLE: {
					if (g_avr_have_power) {
						g_avr_have_power = false;
						avrPowerOff();
					} else {
						g_avr_have_power = true;
						avrPowerOn();
					}
					command_id = AVR_COMMAND_NONE;
				} break;

				case AVR_COMMAND_B1SCRIPT: {
					// assign command string to button
					// we need to read the rest of the line
					len = streamReadLine(client, linebuffer, MAX_NETWORK_TEXT, false);
					FILESYS_STRIP_NL(linebuffer, len);
					if (len)
						strncpy(configButton1, linebuffer, MAX_BUTTON_STRING);
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_B2SCRIPT: {
					// assign command string to button
					// we need to read the rest of the line
					len = streamReadLine(client, linebuffer, MAX_NETWORK_TEXT, false);
					FILESYS_STRIP_NL(linebuffer, len);
					if (len)
						strncpy(configButton2, linebuffer, MAX_BUTTON_STRING);
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_B3SCRIPT: {
					// assign command string to button
					// we need to read the rest of the line
					len = streamReadLine(client, linebuffer, MAX_NETWORK_TEXT, false);
					FILESYS_STRIP_NL(linebuffer, len);
					if (len)
						strncpy(configButton3, linebuffer, MAX_BUTTON_STRING);
					command_id = AVR_COMMAND_NONE;
				} break;

				// I8HEX file operations
				case AVR_COMMAND_LOAD: {
					// read file to memory buffer
					// linebuffer now has the only arg = the filename
					// if the file extension is HEX then we load it's contents into the memory buffer
					// if the file extension is CMD then we pass the file off to the IO Stream
					uint8_t ext = filesysGetType(linebuffer);
					if (ext == FILE_TYPE_CMD)
						ioRunCommandFile(linebuffer, false);
					else if (ext == FILE_TYPE_HEX)
						_avr_file_to_memory(client, linebuffer);
					else {
						ioStreamPrintf(client, "Unrecognized file type - %s\n", linebuffer);
					}
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_STORE: {
					// write memory buffer to file
					// linebuffer now has the only arg = the filename
					_avr_memory_to_file(client, linebuffer);
					command_id = AVR_COMMAND_NONE;
				} break;

				// generic file operations
				case AVR_COMMAND_DIR: {
					ioStreamPrintf(client, "Contents:\n");
					char *info = filesysGetFileInfo(true, true);
					while (info) {
						ioStreamPrintf(client, "\t%s\n", info);
						info = filesysGetFileInfo(false, true);
					}
					client->print("\n");
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_DEL: {
					// linebuffer now has the first arg = the filename
					if (!filesysExists(linebuffer))
						ioStreamPrintf(client, "Error: file %s does not exist\n", linebuffer);
					else {
						filesysDelete(linebuffer);
						ioStreamPrintf(client, "File %s deleted\n", linebuffer);
						ioClear(true);
					}
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_CAT: {
					_avr_read_file_to_stream(client, linebuffer);
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_UPLOAD: {
					_avr_save_stream_to_file(client, linebuffer);
					command_id = AVR_COMMAND_NONE;
				} break;

				// STREAM operations
				case AVR_COMMAND_SEND: {
					// on initial entry, we have just parsed the command and now we will start receiving the HEX stream
					data_size_processed = _avr_stream_to_memory(client, client);
					VERBOSE("\n");
					// when LoadStream returns, we are done with the HEX stream data
					//displayClear(true);
					ioStreamPrintf(client, "Received %d bytes\n", data_size_processed);
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_RECEIVE: {
					if (!_avr_memory_to_stream(client, client))
						ioStreamPrintf(client, "The memory buffer appears empty\n");
					command_id = AVR_COMMAND_NONE;
				} break;

				// FLASH I/O operations
				case AVR_COMMAND_WRITE: {
					_avr_memory_to_flash(client);
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_READ: {
					_avr_flash_to_memory(client);
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_ERASE: {
					_avr_erase(client);
					command_id = AVR_COMMAND_NONE;
				} break;

				// EEPROM I/O operations
				case AVR_COMMAND_EWRITE: {
					// linebuffer now has the combined args = the offset<space>(stream)
					// we still have the stream to read
					uint32_t offset;
					if (sscanf(linebuffer, "%u", &offset) == 1) {
						VERBOSE("EWRITE at %d\n", offset);
						if (!_avr_stream_to_eeprom(client, offset, client)) {
							ioStreamPrintf(client, "The memory buffer may be empty\n");
						}
					} else {
						ioStreamPrintf(client, "Error: unrecognized EWRITE parameters: [%s]\n", linebuffer);
						ioStreamPrintf(client, "       EWRITE parameters uses a strict format: addr (stream)\n");
						ioStreamPrintf(client, "         where addr is an integer offset into the EEPROM memory\n");
					}
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_EREAD: {
					// linebuffer now has the combined args = the offset<space>size
					// the args are int int so linebuffer should have a string of this format
					uint32_t offset;
					uint32_t size;
					if (sscanf(linebuffer, "%u %u", &offset, &size) == 2) {
						VERBOSE("EREAD from %d of %d bytes\n", offset, size);
						if (!_avr_eeprom_to_stream(client, offset, size, client)) {
							ioStreamPrintf(client, "The memory buffer may be empty\n");
						}
					} else {
						ioStreamPrintf(client, "Error: unrecognized EREAD parameters: [%s]\n", linebuffer);
						ioStreamPrintf(client, "       EREAD parameters uses a strict format: addr len\n");
						ioStreamPrintf(client, "         where addr is an integer offset into the EEPROM memory\n");
						ioStreamPrintf(client, "         and len is an integer size for the bytes to read\n");
					}
					command_id = AVR_COMMAND_NONE;
				} break;

				// FUSE I/O operations
				case AVR_COMMAND_READFUSES: {
					_avr_read_fuses(client);
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_SETFUSE: {
					// linebuffer now has the combined args = the fuse_number<space>value
					// the arg is nn XX so linebuffer should have a string of this format
					int num;
					unsigned int val;
					if (sscanf(linebuffer, "%2d %2X", &num, &val) == 2) {
						VERBOSE("SET[fuse] %d to %02X\n", num, val);
						// write-fuse will output the fuse values before and after
						_avr_write_fuse(client, num, val);
					} else {
						ioStreamPrintf(client, "Error: unrecognized FUSE SET parameter: [%s]\n", linebuffer);
						ioStreamPrintf(client, "       FUSE SET parameter uses a strict format: nn XX\n");
						ioStreamPrintf(client, "         where nn is a 2 digit number\n");
						ioStreamPrintf(client, "         and  XX is a 2 character HEX value\n");
					}
					command_id = AVR_COMMAND_NONE;
				} break;

				case AVR_COMMAND_CLEAR: {
					ioClear(false);
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_TEXT: {
					// we reuse the linebuffer to avoid byte-at-a-time IO
					int len = 0;
					while (client->available()) {
						len = streamReadLine(client, linebuffer, MAX_NETWORK_TEXT, true);
						FILESYS_STRIP_NL(linebuffer, len);
						MESSAGE("%s\n", linebuffer);
						//CHIPSERIAL.print(linebuffer);
					}
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_UTEXT: {
					if (!g_avr_uart_baud) {
						if (!_avr_uart_setup(9600, 250))
							ioStreamPrintf(client, "Error: unable to setup UART\n");
						else
							ioStreamPrintf(client, "UART initialization defaulting to 9600 N81\n");
					}

					g_arv_uart_cmd = true; // for the duration of this command, we want exclusive use of the chip UART

					MESSAGE("Sending to UART ...\n");
					// we reuse the linebuffer to avoid byte-at-a-time IO
					int len = 0;
					while (client->available()) {
						len = streamReadLine(client, linebuffer, MAX_NETWORK_TEXT, true);
						FILESYS_STRIP_NL(linebuffer, len);
						MESSAGE("%s\n", linebuffer);
						CHIPSERIAL.print(linebuffer);
					}
					// KLUDGE if we are in quiet mode, we dont care what - if anything - comes back; in a typical smoketest, the inner code would swallow all messages
					if (!ioIsMute()) {
						delay(g_avr_uart_timeout / 2); // give device some time to reply
						uint32_t timeout_timer = millis() + g_avr_uart_timeout;
						MESSAGE("\nReceiving from UART ...\n");

						len = 0;
						while (timeout_timer > millis()) {
							while (CHIPSERIAL.available()) {
								_uartLoop((!ioIsMute()), NULL);			   //	streamReadLine(&(CHIPSERIAL), linebuffer, MAX_NETWORK_TEXT, false);
								timeout_timer = millis() + g_avr_uart_timeout; // reset timeout
							}
						}
					}
					MESSAGE("\nEnd\n");
					if (!ioIsMute()) {
						client->print("\n");
					}

					g_arv_uart_cmd = false;
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_UCLEAR: {
					// read anything available from the attached device UART
					while (CHIPSERIAL.available()) {
						_uartLoop(true, NULL);	//	streamReadLine(&(CHIPSERIAL), linebuffer, MAX_NETWORK_TEXT, false);
					}
					// clear the last-received buffer
					g_last_received_string[0] = 0;
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_WAIT: {
					// linebuffer now has the first (only) milliseconds
					int timeout = 0;
					if (sscanf(linebuffer, "%d", &timeout) == 1) {
						VERBOSE("WAIT: %d ms\n", timeout);
						if (!timeout)
							timeout = 250;
						// loop for 'timeout' and occassionally give UART a chance to receive data
						while (timeout > 0) {
							delay(50); // warning: this should not be very long
							timeout -= 50;
							_uartLoop(true, NULL);
						}
					}
					else {
						ioStreamPrintf(client, "Error: unrecognized WAIT timeout: [%s]\n", linebuffer);
						abort_processing = true;
					}
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_BUTTON: {
					VERBOSE("waiting for button press to continue\n");
					uint8_t button = BUTTON_IDLE, detected = BUTTON_IDLE;
					// loop for 'timeout' and occassionally give UART a chance to receive data
					do {
						buttonsUpdate();
						button = buttonGetState(BUTTON_CENTER);
						if ((button == BUTTON_PRESS) || (button == BUTTON_LONG_PRESS))
							detected = button;
						delay(50);
						_uartLoop(true, NULL);
					} while ((button != BUTTON_RELEASE) && (button != BUTTON_LONG_RELEASE));

					if (detected == BUTTON_LONG_PRESS)
						CLIENTMSG("ABORTED\n");
					else if (detected == BUTTON_PRESS)
						CLIENTMSG("CONTINUE\n");
					else
						CLIENTMSG("BUTTON ERROR\n");
					if (detected == BUTTON_LONG_PRESS)
						abort_processing = true;
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_TEST: {
					// linebuffer now has the args = the timeout (milliseconds) <space> 0/1 for abort

					VERBOSE("test last text received from attached device against stream\n");

					int timeout, abort_on_error;
					if (sscanf(linebuffer, "%d %d", &timeout, &abort_on_error) == 2) {
						VERBOSE("TEST timeout: %d ms and abort: %s\n", timeout, (abort_on_error ? 'true' : "false"));

						// get stream text
						// we reuse the linebuffer
						while (client->available()) {
							streamReadLine(client, linebuffer, MAX_NETWORK_TEXT, true);
							MESSAGE("TEST for %s\n", linebuffer);
						}
						VERBOSEON("TEST for [%s] timeout: %d ms and abort is %s\n", linebuffer, timeout, (abort_on_error ? "true" : "false"));

						// process all data coming from device and test for our string
						bool found = false;
						uint32_t timeout_timer = millis() + timeout;
						do {
							found = _uartLoop(true, linebuffer);
						} while (!found && (timeout_timer > millis()));
						timeout_timer = millis();
						if (!found) {
							abort_processing = (abort_on_error ? true : false);
#if 1						// we want to force this message to appear
							// we temporarily override quietmode
							bool was_quiet = ioIsMute();
							ioMuteOff();
							MESSAGE("[%s] not received, %s\n", linebuffer, (abort_processing ? "aborting" : "continuing"));
							if (was_quiet)
								ioMuteOn();
#else						// we don't care if this message appears
							MESSAGE("TEST for [%s] timed out, %s\n", linebuffer, (abort_processing ? "aborting" : "continuing"));
#endif
						}
					}
					else {
						ioStreamPrintf(client, "Error: unrecognized TEST timeout: [%s]\n", linebuffer);
						abort_processing = true;
					}
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_MUTEON: {
					VERBOSE("disable command and operations messages to the display\n");
					ioMuteOn();
					command_id = AVR_COMMAND_NONE;
				} break;
				case AVR_COMMAND_MUTEOFF: {
					VERBOSE("restore command and operations messages to the display\n");
					MESSAGE("QUIETOFF\n");
					ioMuteOff();
					command_id = AVR_COMMAND_NONE;
				} break;
				default: {
					ioStreamPrintf(client, "Oops: unknown state with [%d].\n", command_id);
					command_id = AVR_COMMAND_NONE;
				} break;
			} // end command switch

			// if a command required power and we temporarily activated it, shut it down now
			if (needs_power && !avrPowerIsActive()) {
				DEBUG("Power required and no longer active\n");
				avrPowerOff();
			}
#if 0
			// if one of our commands signals we need to abort, then run out any remaining command stream data
			if (abort_processing) {
				while (client->available())
					c = client->read();
			}
#endif
		}
		else {
			VERBOSE("empty command\n");
		}

	} while (client->available());

	// this should never occur but it's here just in case
	if (active_file) {
		filesysSaveFinish();
		ioClear(true);
		ioStreamPrintf(client, "Received %d bytes\n", data_size_processed);
	}

	VERBOSE("done with commands\n");
	// handle any responces to send back to connected source
	//client->flush();
	return (!abort_processing);
}
