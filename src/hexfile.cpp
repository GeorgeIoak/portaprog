/* ***************************************************************************
* File:    ihexstream.cpp
* Date:    2020.08.09
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the MIT License
*
* ******************************************************************************/

/* ---
--------------------------------------------------------------------------
### IHEX STREAM API

Provide access Stream I/O for Intel HEX formatted data

Provides a Stream compatible methods for handling Intel HEX file format and uses Stream I/O
to handle both FILE operations and Stream data with the TCP connection.

The Intel HEX (I8HEX) file format consists of a series of lines.
Each line of data starts with zero or more whitespace and then a colon ':'.
All characters after the colon are text HEX representations of bytes - e.g. '2' and 'F' become 0x2F.

The format of each line is :llaaaarrdd...cc
  - ':' starts with colon
  - ll = bytes of actual data on line
  - aaaa = page address (file 'page' address; not target chip address)
  - rr = record type (00 = data, 01 = END
  - dd... = zero or more data bytes; max is 255 dd _(510 characters)_
  - cc = checksum

Examples:
```
	:106EB00081959F4F0895709561957F4F0895EE0FCE
	:0E6EC000FF1F0590F491E02D0994F894FFCF88
	:106ECE0001000000010305060402006340201010BB
```

The Stream implementation was written after reviewing https://github.com/paulo-raca/ArduinoBufferedStreams

--- */

#include "allincludes.h"
#define LOCALDEBUG DEBUG

// -------------------------------------------------------------------------
// ----------------------- Private Methods ---------------------------------
// -------------------------------------------------------------------------

uint8_t ihexStream::_hex_to_char_high(uint8_t c) {
	uint8_t b;
	b = (c >> 4) & 0xF;
	if (b <= 9) b = b + '0';
	else		b = (b - 10) + 'A';
	return b;
}
uint8_t ihexStream::_hex_to_char_low(uint8_t c) {
	uint8_t b;

	b = (c)&0xF;
	if (b <= 9)
		b = b + '0';
	else
		b = (b - 10) + 'A';
	return b;
}

bool ihexStream::_is_EOL(char c) {
	if (c == '\n' || c == '\r')
		return true;
	return false;
}

int ihexStream::_hex_to_num(uint8_t h) {
	// Turn a character 'hex digit' (0..9, A..F) into the equivalent binary value (0-16); a return of 255 is an error
	if (h >= '0' && h <= '9')
		return (h - '0');
	if (h >= 'A' && h <= 'F')
		return ((h - 'A') + 10);
	if (h >= 'a' && h <= 'f')
		return ((h - 'a') + 10);

	// MESSAGE("Bad digit! 0x%X", h);
	return (-1); // error
}

int ihexStream::_read_hex_from_line(char *text) {
	int h, l;

	if (text && text[0] && text[1]) {
		h = _hex_to_num(text[0]);
		l = _hex_to_num(text[1]);
		if ((h < 0) || (l < 0))
			return -1;
		return ((h & 0xF) << 4) + ((l & 0xF));
	}
	return -1;
}

int ihexStream::_read_hex_from_stream(Stream *src) {
	if (src->available() >= 2) {
		char text[2];
		text[0] = src->read();
		text[1] = src->read();
		return _read_hex_from_line(text);
	}
	return -1;
}

int32_t ihexStream::_read_hex(uint8_t len) {
	// read string as hex characters and return value; len <= 8 but really its 7FFFFFFF as we need the high bit for negative return
	uint32_t rtn = 0;

	for (int32_t i = 0; i < len; i++) {
		int c = read(); // read from our buffer
		if (c < 0)
			return -1;

		if ((c >= '0') && (c <= '9'))
			rtn = (rtn << 4) + (c - '0');
		else if ((c >= 'A') && (c <= 'F'))
			rtn = (rtn << 4) + ((c - 'A') + 10);
		else if ((c >= 'a') && (c <= 'f'))
			rtn = (rtn << 4) + ((c - 'a') + 10);
		else
			return -1;
	}

	VERBOSE("readHex(%d) %0X\n", len, rtn);
	return rtn;
}

// -------------------------------------------------------------------------
// ------------------------ Public Methods ---------------------------------
// -------------------------------------------------------------------------

ihexStream::ihexStream(int32_t requested_size) {
	VERBOSE("ihexStream::ihexStream\n");
	int32_t size = requested_size;

	// _total_size should be a multiple of 256
	VERBOSE("ihexStream(%d) ", size);
	size = (size + 255);
	size /= 256;
	size *= 256;
	VERBOSE(" = %d\n", size);

	for (int32_t i = 0; i < IHEX_MAX_BUFFERS; i++)
		this->_blocks[i] = NULL;

	this->_block_count = 0;
	this->_block_size = 0;
	this->_total_size = 0;

	if (size <= 0) {	// in case a uint32_t value was passed in and exceeds an in32_t
		MESSAGE("Error: IHEX buffer request too large\n");
		return;
	}

	VERBOSE("Total heap:  %7d bytes\n", ESP.getHeapSize());
	VERBOSE("Free heap:   %7d bytes\n", ESP.getFreeHeap());
  	VERBOSE("Total PSRAM: %7d bytes\n", ESP.getPsramSize());
  	VERBOSE("Free PSRAM:  %7d bytes\n", ESP.getFreePsram());
	VERBOSE("--------------------------\n");

	int32_t psram_size = ESP.getFreePsram();
	// for a RAM buffer, we attempt to use multiple chunks if a single large buffer is not possible

	if (psram_size >= size) {
	//VERBOSE("Attempting to allocate PSRAM %d bytes\n", size);
	this->_blocks[0] = (uint8_t *)ps_malloc(size);
		this->_block_count = 1;
		if (this->_blocks[0] == NULL) {
			this->_total_size = 0;
			this->_block_count = 0;
			VERBOSE("Unable to allocate PSRAM of %d bytes\n", size);
		} else {
			this->_total_size = size;
			this->_block_size = size;
			VERBOSE("Allocated PSRAM of %d bytes\n", size);
		}
	} else {
		// big buffers are possible with PSRAM but conventual RAM typically can only handle aprox 128KB max.
		if (size > 128 * 1024)
			size = (128 + 1) * 1024;
	}

	// if we do not yet have a buffer, we will attempt to chunk up our allocations
	if (!(this->_total_size)) {
		int32_t chunk_size = size;
		uint8_t chunks = 1;
		bool success = true;

		do {
			// if we are ending up with too many chunks, then let's try for less memory
			if (chunks > IHEX_MAX_BUFFERS) {
				chunks = 1;
				size = size / 2;
			}

			chunk_size = size / chunks;
			success = true;
			VERBOSE("Attempting to allocate %d chunk(s) of %d bytes\n", chunks, chunk_size);
			// if the original _total_size is not a multiple of chunks, then this is equal to a ceiling function

			for (int i = 0; i < chunks; i++) {
				this->_blocks[i] = (uint8_t *)malloc(chunk_size);
				if (this->_blocks[i] == NULL) {
					success = false;
					break;
				}
				else {
					VERBOSE("Allocated #%d buffer of %d bytes\n", i, chunk_size);
				}
			}
			if (!success) {
				VERBOSE("Failed     to allocate %d chunk(s) of %d bytes\n", chunks, chunk_size);
				for (int i = IHEX_MAX_BUFFERS - 1; i >= 0; i--) {
					// in an attempt to avoid fragmentation, we will free the buffers in reverse order
					if (this->_blocks[i] != NULL) {
						free(this->_blocks[i]);
						this->_blocks[i] = NULL;
					}
				}
				chunks = chunks * 2;
			}
			else {
				this->_block_count = chunks;
				this->_block_size = chunk_size;
				this->_total_size = (chunk_size * chunks);
				VERBOSE("Success     allocating %d chunk(s) of %d bytes\n", chunks, chunk_size);
				VERBOSE("Requested buffer size: %d bytes\n", requested_size);
				VERBOSE("Actual    buffer size: %d bytes\n", this->_total_size);
			}
		} while (!success && (chunks <= IHEX_MAX_BUFFERS));
	}

	MESSAGE("FLASH buffer %d KB in %d chunk(s)\n", (this->_total_size / 1024), this->_block_count);

	VERBOSE("--------------------------\n");

	VERBOSE("Total heap:  %7d bytes\n", ESP.getHeapSize());
  	VERBOSE("Free heap:   %7d bytes\n", ESP.getFreeHeap());
  	VERBOSE("Total PSRAM: %7d bytes\n", ESP.getPsramSize());
  	VERBOSE("Free PSRAM:  %7d bytes\n", ESP.getFreePsram());

	this->clear();

	/*
		this is a NOT a circular buffer:
			the _total_size is a constant.
			the _position is the index of the next byte to read
			the _unread_size is number of unread bytes

		if it were not for the rewind() method, we could wrap
		so the assumption is we are write, rewind, and read
	*/
}

ihexStream::~ihexStream() {
	for (int i = IHEX_MAX_BUFFERS - 1; i >= 0; i--) {
		// in an attempt to avoid fragmentation, we will free the buffers in reverse order
		if (this->_blocks[i] != NULL) {
			free(this->_blocks[i]);
			this->_blocks[i] = NULL;
		}
	}
}

void ihexStream::clear() {
	VERBOSE("ihexStream::clear()\n");

	_position = 0;
	_unread_size = 0;
	_stored_size = 0;
	_buffer_errors = 0;
	_i8hex_lineaddr = 0;
	_non_filler = 0;
	_i8hex_EOD = true;
	for (int i = 0; i < IHEX_MAX_BUFFERS; i++) {
		if (_blocks[i] != NULL) {
			memset(_blocks[i], FILLER_BYTE, _block_size);
		}
	}
}

void ihexStream::rewind() {
	VERBOSE("ihexStream::rewind()\n");
	_position = 0;
	_unread_size = _stored_size;
	_buffer_errors = 0;
	_i8hex_lineaddr = 0;
	if (_unread_size)
		_i8hex_EOD = false;
	else
		_i8hex_EOD = true;
}

int ihexStream::seek(uint32_t offset) {
	if (offset < _stored_size) {
		_position = offset;
		_unread_size = _stored_size;
		return _position;
	}
	return -1;
}

int ihexStream::peek() {
	if ((_blocks[0] == NULL) || _unread_size == 0)
		return -1;

	int32_t index;
	int32_t chunk;

	// the current buffer position is chunk:pos were chunk = buffer position / chunk size; and position is buffer position % chunk size
	chunk = _position / _block_size;
	index = _position % _block_size;

	if (chunk >= _block_count) {
		MESSAGE("PEEK BLOCK TRACKING ERROR");
		return -1;
	}

	uint8_t *buffer;
	buffer = _blocks[chunk];
	return (buffer[index]);
}

int ihexStream::fetch() {
	int c = peek();
	if (c < 0) {
		_buffer_errors++;
		return -1;
	}

	_position++;

	// end of all data
	if (_position >= _total_size) {
		if (_unread_size > 0)
			MESSAGE("READ BUFFER TRACKING ERROR");
		_unread_size = 0;
	}
	_buffer_errors = 0;
	if (c != FILLER_BYTE)
		_non_filler--;
	return c;
}

int ihexStream::read() {
	int c = fetch();
	if (c >= 0) {
		if (_unread_size > 0)
			_unread_size--;
	}
	return c;
}

int ihexStream::available() {
	VERBOSE("ihexStream::available() = %d\n", _unread_size);
	if (_blocks[0] == NULL)
		return 0;

	return _unread_size;
}

size_t ihexStream::update(uint8_t b) {
	if (_blocks[0] == NULL)
		return 0;
	if (_unread_size >= _total_size) {
		MESSAGE("UPDATE BUFFER TRACKING ERROR");
		_buffer_errors++;
		return 0;
	}

	int32_t index;
	int32_t chunk;
	// the current buffer position is chunk:pos were chunk = buffer position / chunk size; and position is buffer position % chunk size
	chunk = _position / _block_size;
	index = _position % _block_size;

	if (chunk >= _block_count) {
		MESSAGE("UPDATE BLOCK TRACKING ERROR");
		return 0;
	}

	uint8_t *buffer;
	buffer = _blocks[chunk];

	buffer[index] = b;
	_position++;
	if (b != FILLER_BYTE)
		_non_filler++;
	return 1;
}


size_t ihexStream::write(uint8_t b) {
	if (_blocks[0] == NULL)
		return 0;
	if (update(b)) {
		_unread_size++;
		_stored_size++;
		return 1;
	}
	return 0;
}

int32_t ihexStream::availableForWrite() {
	if (_blocks[0] == NULL)
		return 0;
	return _total_size - _unread_size;
}

void ihexStream::flush() {
	// I'm not sure what to do here...
	MESSAGE("I8HEX: Flush() not implemented\n");
}

int32_t ihexStream::resize(int32_t max_size, uint16_t pagesize) {
	VERBOSE("resize(%d, %d) with %d:%d stored bytes starting at %d out of %d total available\n", max_size, pagesize, _stored_size, _non_filler, _position, _total_size);
	if (_blocks[0] == NULL)
		return 0;

	uint8_t *buffer = _blocks[0];

	if (max_size > _total_size)
		max_size = _total_size;


	for (int32_t i = (IHEX_MAX_BUFFERS - 1); i >= 0; i--) {
		if (_blocks[i] == NULL)
			continue;
		VERBOSE("scanning chunk #%d %d bytes\n", i, _block_size);
		buffer = _blocks[i];
		// scan the chunk for anything that is not the FILL_BYTE
		for (int32_t j = (_block_size - 1); j >= 0; j--) {
			if (buffer[j] != FILLER_BYTE) {
				// we found data; move to 'full block' boundary
				int32_t size = (_block_size * i) + (j + (pagesize - (j % pagesize)));
				_stored_size = size;
				_unread_size = size;
				VERBOSE("found real data in block #%d at index %d; position %d becomes %d\n", i, j, (_block_size * i) + j, size);
				rewind();
				return _unread_size;
			}
		 }
	}
	// everything was the filler byte so our data is zero
	clear();
	return 0;
}

// ------------------------------------------------------
// the following is for Intel HEX formatted data (I8HEX)
// ------------------------------------------------------

int16_t ihexStream::writeIHexLine(char *line) {
	VERBOSE("writeIHexLine(%s)\n",line);
	if (_blocks[0] == NULL)
		return 0;

	uint16_t datalen = 0;

	uint8_t low = 0,
			high = 0,
			cksum = 0;

	char *ptr = line;

	// skip leading whitespace; lines starts with zero or more whitespace and then a colon ':'.
	LOCALDEBUG("Processing    ");
	int c;
	do {
		c = *ptr++;
		LOCALDEBUG("%c", c);
	} while (c == ' ' || c == '\n' || c == '\r' || c == '\t');
	if (c != ':') {
		if (c >= 0)
			VERBOSE("Line missing colon at start\n");
		return -1;
	}

	// Read the byte count into 'len'
	cksum = (datalen = _read_hex_from_line(ptr));
	ptr += 2;
	LOCALDEBUG("%02X", datalen);

	// read high address byte
	cksum += (high = _read_hex_from_line(ptr));
	ptr += 2;
	// read low address byte
	cksum += (low = _read_hex_from_line(ptr));
	ptr += 2;
	_i8hex_lineaddr = (high << 8) + low; // we really only care about the line address for error reporting
	LOCALDEBUG("%04X", _i8hex_lineaddr);

	// read record type
	cksum += (low = _read_hex_from_line(ptr));
	ptr += 2;
	LOCALDEBUG("%02X", low);

	if (low == 0x01) {
		// end record, we're done; but there should be one more HEX byte to read == 0xFF
		high = _read_hex_from_line(ptr);
		ptr += 2;
		LOCALDEBUG("end of data marker\n");
		return 0;
	}

	// process remainder of line, 2 characters as a time (converted to 1 hex byte at a time)
	for (int32_t i = 0; i < datalen; i++) {
		// read 2 characters as 1 hex byte from file
		cksum += (low = _read_hex_from_line(ptr));
		ptr += 2;
		write(low); // store the byte in our stream buffer
		LOCALDEBUG("%02X", low);
	}

	// read the cksum at the end of the line; it should zero out our cksum accumulation
	cksum += (low = _read_hex_from_line(ptr));
	ptr += 2;
	LOCALDEBUG("%02X\n", low);

	if (cksum != 0) {
		MESSAGE("Bad checksum for line address 0x%X\n", _i8hex_lineaddr);
		return -1;
	}
	if (peek() < 0) {
		MESSAGE("Reached EOF before reading final HEX marker\n");
		return -1;
	}
	//Serial.printf("I8HEX: %02d bytes    STORAGE: %05d\n", datalen, _unread_size);
	return datalen; // for standard Atmel data, this will be 16 for most line and 16 or less for the last line of real data
}

// read data from our internal stream and generate a I8HEX formatted text line; null terminated but no newline
char *ihexStream::readIHexLine() {
	VERBOSE("readIHexLine");
	if (_blocks[0] == NULL)
		return NULL;

	// will return null or a pointer to the internal buffer

	// note: we use the internal _i8hex_lineaddr; use rewind() to reset it to 0 before starting to use this method, especially in a loop

	// are there any bytes to read?
	if (!available()) {
		// if there are no more data and we have not yet sent the end marker, do so now
		if (!_i8hex_EOD) {
			strcpy(LINE_BUFFER, ":00000001FF");
			VERBOSE("(%s)\n", LINE_BUFFER);
			_i8hex_EOD = true;
			return LINE_BUFFER;
		}
		return NULL;
	}

	LOCALDEBUG("(");

	// each line is :llaaaarrdd...cc

	char *ptr = LINE_BUFFER;
	uint8_t cksum = 0;
	uint16_t dw = _i8hex_lineaddr;
	int c;

	// : 10 0010 00 0C9462000C9462000C9462000C946200 D8
	// : 10 0000 01 0C9462000C9462000C9462000C94620009

	// start with colon ':'
	*ptr++ = ':';
	LOCALDEBUG("%c", ptr[-1]);

	c = 16;	// length
	//cksum = (c & 0xFF);	// we do the size at the end in case the data is less than 16 bytes
	*ptr++ = _hex_to_char_high(c);
	LOCALDEBUG("%c", ptr[-1]);
	*ptr++ = _hex_to_char_low(c);
	LOCALDEBUG("%c", ptr[-1]);

	// add address
	c = (dw >> 8) & 0xFF;
	cksum += (c & 0xFF);
	*ptr++ = _hex_to_char_high(c);
	LOCALDEBUG("%c", ptr[-1]);
	*ptr++ = _hex_to_char_low(c);
	LOCALDEBUG("%c", ptr[-1]);

	c = (dw) & 0xFF;
	cksum += (c & 0xFF);
	*ptr++ = _hex_to_char_high(c);
	LOCALDEBUG("%c", ptr[-1]);
	*ptr++ = _hex_to_char_low(c);
	LOCALDEBUG("%c", ptr[-1]);

	// add record type
	c = 0x00;
	//cksum += (c & 0xFF);
	*ptr++ = _hex_to_char_high(c);
	LOCALDEBUG("%c", ptr[-1]);
	*ptr++ = _hex_to_char_low(c);
	LOCALDEBUG("%c", ptr[-1]);

	// add actual data
	uint8_t size;
	// we dump data using the default of 16 bytes per I8HEX line
	for (size = 0; size < 16; size++) {
		if (!available())
			break;

		int c = read();
		if (c < 0)
			break;

		cksum += (c & 0xFF);
		*ptr++ = _hex_to_char_high(c);
		LOCALDEBUG("%c", ptr[-1]);
		*ptr++ = _hex_to_char_low(c);
		LOCALDEBUG("%c", ptr[-1]);
	}

	_i8hex_lineaddr += size;
	cksum += (size & 0xFF);

	// if we did not have a full 16 bytes then we need to go back and adjust the data length
	if (size < 16) {
		LINE_BUFFER[1] = _hex_to_char_high(size);
		LINE_BUFFER[2] = _hex_to_char_low(size);
	}

	cksum = -cksum;
	c = cksum;
	*ptr++ = _hex_to_char_high(c);
	LOCALDEBUG("%c", ptr[-1]);
	*ptr++ = _hex_to_char_low(c);
	LOCALDEBUG("%c", ptr[-1]);

	*ptr++ = 0;
	LOCALDEBUG(")\n             ");

	VERBOSE("%s\n", LINE_BUFFER);
	//VERBOSE("line address: %04X\n", i8hex_lineaddr);
	return LINE_BUFFER;
}
