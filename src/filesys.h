#ifndef __FILE_SYSTEM_H
#define __FILE_SYSTEM_H

#define MAX_FILENAME_LEN 32
#define MAX_FORMATBYTES 10

#define FILE_TYPE_UNKN	0
#define FILE_TYPE_CMD	1
#define FILE_TYPE_HEX	2

#define FILESYS_STRIP_NL(b, n)   \
	if (n && b[n - 1] == '\n') { \
		n--;                     \
		b[n] = 0;                \
	}

bool filesysInit();
void filesysLoop();

bool filesysExists(const char* name);
void filesysDelete(const char *name);
File filesysOpen(const char *name, const char* mode);
void filesysClose(File handle);
char *filesysGetFileInfo(bool first, bool hidden);
uint8_t filesysGetType(const char *name);
uint8_t filesysReadHex(Stream *handle);
uint16_t streamReadLine(Stream* handle, char *buf, uint16_t size, bool escaped_characters);

bool filesysSaveStart(const char* name);
bool filesysSaveWrite(uint8_t* buf, size_t size);
bool filesysSaveFinish();

#endif
