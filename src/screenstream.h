#ifndef __SCREENSTREAM_H
#define __SCREENSTREAM_H

#ifdef USER_SETUP_LOADED	// indicates we are using the TFT eSPI library
#define SPIVIDEO			// create a more intuitive defice for conditional code

#include <TFT_eSPI.h>

#define HEADER_FONT 2
#define WINDOW_FONT 0

// viewport scroll-pan Y of virtual screen
#define FONT_WIDTH			6
#define FONT_HEIGHT			9
#define VIEWPORT_LINE_CHARS 40	// screen width divided by font width
#define VIEWPORT_LINES		15  // number of lines using are normal font
#define VIEWPORT_LIST_LINES 13  // number of lines after the header

#else

#endif

#define SCREEN_LINES	  	80
#define SCREEN_LINE_CHARS 	VIEWPORT_LINE_CHARS

#define IDLE_USER_TIMEOUT	90		// used to revert the display if the user hasn't done anything for this period of time
#define IDLE_ACTION_TIMEOUT	15		// used as a delay before switchign to text mode if the user is using the menu system
#define IDLE_TIMEOUT_TEST	0		// just test if the timout has expired

// by tracking 'stored_size' and 'unread_size' we are able to rewind the buffer repeat using it
class screenStream : public Stream {
  private:
	// the output (screen) properties
#ifdef SPIVIDEO
	TFT_eSPI *display;
#endif
	char lines_buffer[SCREEN_LINES][SCREEN_LINE_CHARS + 1];
	uint16_t current_display_line;				// number of lines in the buffer currently being used
	uint16_t current_display_line_char;		// current character position on the line
	uint16_t current_viewport_first_line;		// number of lines in the buffer currently being used
	uint16_t current_viewport_first_line_char; // current character position on the line
	uint16_t file_list_length;					// total count of files; updated by file_list_display()
	uint16_t skipped_lines;					// number of files from file_list_length which have scrolled off the top; updated by file_list_display()
	int16_t file_list_index;					// the currently highlighted position on the display; incremented by the button
	int32_t text_mode_delay;


	bool display_dirty;
	bool display_wrap;
	uint8_t display_mode;
	char selected_filename[MAX_FILENAME_LEN + 1]; // the selected file; used between the list and the menu
	uint8_t selected_type;

	void refresh_battery();
	uint16_t refresh_header(const char *title);
	void refresh_current();
	void refresh_file_list();
	void refresh_menu();
	void refresh_text_mode(bool);

  public:
	// represents the the maximin possible string when formatted using I8HEX; realistically, the data portion will likely be 32 2B data
	// 1B start code; 1 2B data length; 1 4B address; 1 2B data type; 255 2B max possible data; 1 1B checksum; 1B null terminator

	static const uint32_t DEFAULT_SIZE = 256;

	screenStream(uint32_t buffer_size = screenStream::DEFAULT_SIZE);
	~screenStream();

	// write to display
	virtual size_t write(uint8_t);
	virtual int print(const char*);
	virtual int printf(const char*, ...);

	virtual int availableForWrite(void);
	virtual void flush();

	// screen specific methods
	void clear(bool);
	void clearData();
	void refresh();
	void icons(int8_t rx, int8_t tx, int8_t con);
	bool timeout(int seconds);
	void updateListPosition(int);
	void selectCurrent();
	void scrollUp();
	void scrollDown();
	void scrollLeft();
	void scrollRight();
	void wrapOff();
	void wrapOn();
	bool isTextMode();
	void delayTextMode(bool);
	void displayOff();
	void displayOn();

	// stream requires these but we are output-only so we will stub them out
	virtual int available() {
		return -1;
	}
	virtual int peek() {
		return -1;
	}
	virtual int read() {
		return -1;
	}
};

#endif
