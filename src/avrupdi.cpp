/* ***************************************************************************
* File:    avrupdi.cpp
* Date:    2019.09.16
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the multitude of licenses within the pedigree ... sorry
*
* ******************************************************************************/

/* ---
--------------------------------------------------------------------------
### UDPI API

Provide AVR programming using the UPDI protocol over a TCP connection.

**NOTE:** These functions should only be access through the AVR TCP API

This code is a re-work based on c_updi_public which is a port from pyupdi.
	https://github.com/jarl93rsa/c_updi_public
	https://github.com/mraardvark/pyupdi

--- */

#include <string.h>

#include "allincludes.h"

//#define AVRDEBUG(fmt, ...)		DEBUGSERIAL.printf(fmt, ##__VA_ARGS__)
//#define AVRVERBOSE(fmt, ...)		DEBUGSERIAL.printf(fmt, ##__VA_ARGS__)

#ifndef AVRDEBUG
#define AVRDEBUG(fmt, ...) ((void)0)
#endif
#ifndef AVRVERBOSE
#define AVRVERBOSE(fmt, ...) ((void)0)
#endif

/*
	UPDI uses a BREAK BREAK to reset the chip when attempting to enter programming mode.
	The original idea was to end the serial connection, change the pins temporarily to OUTPUT,
	pull the pins LOW for two intervals (with a HIGH in the middle), then begin the serial again.
	On some ESP32 chips, this causes a crash and stack dump.

	To avoid this crash, we leave the serial active but switch the BAUD rate to be so slow that
	sending a NULL will pull the pin LOW for the necessary length of time. Send a NULL, delay briefly,
	and send a second NULL, performs the necessary BREAK BREAK. Then the baud rate is restored.
*/

#define UPDI_AVOID_SERIAL_RESTART


#if 1 // serial IO on the UART connected to the UPDI device // this #if is to facilitate code collapse for readability
// --------------------------------------------------------------------------------------------------------

static uint8_t _updi_serial_retry_counter; // resets after success or failure
static uint16_t _updi_serial_retry_count;  // used for diagnostics
static bool _updi_serial_inited;

static void _updi_serial_init() {
	if (_updi_serial_inited) {
		AVRVERBOSE("updi serial already inited\n");
	}
	else {
		AVRVERBOSE("updi serial init begin\n");
		_updi_serial_retry_counter = 0;
		_updi_serial_retry_count = 0;
		// UPDISERIAL is the second UART UART on the ESP32; it is sed for UPDI
	#if 1
		pinMode(PIN_UPDI_RX, OUTPUT);
		pinMode(PIN_UPDI_TX, OUTPUT);
		digitalWrite(PIN_UPDI_RX, HIGH);
		digitalWrite(PIN_UPDI_TX, HIGH);
		delay(1);
		//pinMode(PIN_UPDI_RX, INPUT);
		//pinMode(PIN_UPDI_TX, OUTPUT);
		AVRVERBOSE("updi serial init pins RX=%d, TX=%d\n", PIN_UPDI_RX, PIN_UPDI_TX);
	#endif
		//UPDISERIAL.begin(230400, SERIAL_8E2, PIN_UPDI_RX, PIN_UPDI_TX);
		UPDISERIAL.begin(115200, SERIAL_8E2, PIN_UPDI_RX, PIN_UPDI_TX);
		size_t s = 0;
		s = UPDISERIAL.setRxBufferSize(512+16);	// KLUDGE this should be based off of UPDI_MAX_REPEAT
		UPDISERIAL.setTimeout(50);
		AVRVERBOSE("updi serial init set with buffer = %d\n", s);

		while (!UPDISERIAL)
			; // wait for serial attach
		AVRVERBOSE("updi serial init finish\n");
	}
	_updi_serial_inited = true;
}

static void _updi_serial_term() {
#ifdef UPDI_AVOID_SERIAL_RESTART
	AVRVERBOSE("AVOID_SERIAL_RESTART: serial term ignored\n");
#else
	AVRVERBOSE("updi serial term begin\n");
	SERIAL_FLUSH(UPDISERIAL);

	delay(10);
	AVRVERBOSE("updi serial term flushed\n");
	delay(10);
	UPDISERIAL.end();
	AVRVERBOSE("updi serial term closed\n");
	// now we manually drive both pins low (timing is relative to chip clock speed (Mhz):
	//20MHz = 6ms, 8Mhz = 13, 4Mhz = 25 : and since we do not know the clock, we assume its slow

	// must force the pins back to being MUXed for I/O
	gpio_matrix_out(PIN_UPDI_TX, SIG_GPIO_OUT_IDX, false, false);
	gpio_matrix_out(PIN_UPDI_RX, SIG_GPIO_OUT_IDX, false, false);
	//delay(1);

	// switch pins to input so we are not feeding power back over the UPDI pin
	pinMode(PIN_UPDI_RX, INPUT_PULLUP);
	pinMode(PIN_UPDI_TX, INPUT_PULLUP);
	AVRVERBOSE("updi serial term input\n");
	_updi_serial_inited = false;
	//return;

	pinMode(PIN_UPDI_RX, OUTPUT);
	pinMode(PIN_UPDI_TX, OUTPUT);
	AVRVERBOSE("updi serial term output\n");
	//delay(1);

	digitalWrite(PIN_UPDI_RX, HIGH);
	digitalWrite(PIN_UPDI_TX, HIGH);
	AVRVERBOSE("updi serial term high\n");
	//delay(1);
	AVRVERBOSE("updi serial term finish\n");
#endif
}

static void _updi_serial_force_break() {
#ifdef UPDI_AVOID_SERIAL_RESTART

	AVRVERBOSE("updi_serial_force_break()\n");

	// flush anything
	while (UPDISERIAL.available())
		UPDISERIAL.read();

	AVRVERBOSE("updi_serial baud 110\n");
	UPDISERIAL.updateBaudRate(110);
	delay(50);

	AVRVERBOSE("updi_serial BREAK 1\n");
	UPDISERIAL.write(0);
	// flush anything
	while (UPDISERIAL.available())
		UPDISERIAL.read();

	delay(12);

	AVRVERBOSE("updi_serial BREAK 2\n");
	UPDISERIAL.write(0);
	while (UPDISERIAL.available())
		UPDISERIAL.read();

	AVRVERBOSE("updi_serial baud 115200\n");
	UPDISERIAL.updateBaudRate(115200);
#else
	_updi_serial_term();
	digitalWrite(PIN_UPDI_RX, HIGH);
	digitalWrite(PIN_UPDI_TX, HIGH);
	delay(12);
	// first BREAK
	AVRVERBOSE("updi_serial Digital BREAK 1\n");
	digitalWrite(PIN_UPDI_RX, LOW);
	digitalWrite(PIN_UPDI_TX, LOW);
	delay(25);
	digitalWrite(PIN_UPDI_RX, HIGH);
	digitalWrite(PIN_UPDI_TX, HIGH);
	delay(12);
	AVRVERBOSE("updi_serial Digital BREAK 2\n");
	digitalWrite(PIN_UPDI_RX, LOW);
	digitalWrite(PIN_UPDI_TX, LOW);
	delay(25);
	// establish a known high state
	digitalWrite(PIN_UPDI_RX, HIGH);
	digitalWrite(PIN_UPDI_TX, HIGH);
	_updi_serial_init();
#endif
}

static int _updi_serial_read_wait() {
	int b = -1;
	_updi_serial_retry_counter++;
	_updi_serial_retry_count++;

	// try to wait for data
	while (_updi_serial_retry_counter++) {
		if ((b = UPDISERIAL.read()) >= 0)
			break;
		delay(1);
		_updi_serial_retry_count++;
	}
	_updi_serial_retry_counter = 0;
	return b;
}

static bool _updi_serial_send(uint8_t *data, uint16_t size) {
	/*
		NOTE: since the TX and RX pins are tied together,
		everything we send gets echo'd and needs to be
		discarded QUICKLY.
	*/
	bool good_echo = true;
	uint16_t count = 0;
	int b;

	SERIAL_FLUSH(UPDISERIAL);

	// write all data in one shot and then
	// read back the echo
	// this method requires a serial RX buffer as large as the largest possible TX block of data
	// it is possible to check the process but the larger the block, the faster

	count = UPDISERIAL.write(data, size);
	if (count != size) {
		AVRVERBOSE("UPDISERIAL send count error %d != %d\n", count, size);
	}
	delay(2);
	count = 0;
	for (uint16_t i = 0; i < size; i++) {
		b = _updi_serial_read_wait(); // wait for data
		if (b != data[i]) {
			good_echo = false;
			AVRVERBOSE("send[%d] %02x != %02x\n", i, data[i], b);
		} else {
			// AVRDEBUG(" %02x", b);
		}
		count++;
	}
	//AVRDEBUG("\n");

	if (count != size) {
		AVRVERBOSE("UPDISERIAL echo count error %d != %d\n", count, size);
		return false;
	}
	return good_echo; // was return true
}

static bool _updi_serial_send_receive(uint8_t *data, uint16_t size, uint8_t *buff, uint32_t len) {
	AVRDEBUG("serial_send_receive( , %d, , %d) =", size, len);
	bool timeout = false;
	int32_t count = 0;
	int b;

	if (_updi_serial_send(data, size)) {
		for (int32_t i = 0; i < len; i++) {
			b = _updi_serial_read_wait(); // wait for data
			buff[count++] = b;
			if (b == -1)
				timeout = true;
			else {
				AVRDEBUG(" %02X", b);
			}
		}
		AVRDEBUG("\n");

		if (count != len) {
			AVRVERBOSE("UPDISERIAL receive count error %d != %d\n", count, len);
			return false;
		}
		if (timeout) {
			AVRVERBOSE("UPDISERIAL timeout while reading data\n");
			return false;
		}
		return true;
	}
	AVRVERBOSE("something happened with serial_send\n");
	return false;
}

// --------------------------------------------------------------------------------------------------------
#endif // serial IO on the UART connected to the UPDI device // this #if is to facilitate code collapse for readability

#if 1 // UPDI programming codes // this #if is to facilitate code collapse for readability
// --------------------------------------------------------------------------------------------------------

#define UPDI_BREAK 			0x00

#define UPDI_LDS			0x00
#define UPDI_STS			0x40
#define UPDI_LD				0x20
#define UPDI_ST				0x60
#define UPDI_LDCS			0x80
#define UPDI_STCS			0xC0
#define UPDI_REPEAT 		0xA0
#define UPDI_KEY			0xE0

#define UPDI_PTR		 	0x00
#define UPDI_PTR_INC	 	0x04
#define UPDI_PTR_ADDRESS 	0x08

#define UPDI_ADDRESS_8		0x00
#define UPDI_ADDRESS_16 	0x04
#define UPDI_ADDRESS_24 	0x08

#define UPDI_DATA_8	 		0x00
#define UPDI_DATA_16 		0x01
#define UPDI_DATA_24 		0x02

#define UPDI_KEY_SIB 		0x04
#define UPDI_KEY_KEY 		0x00

#define UPDI_KEY_64	 		0x00
#define UPDI_KEY_128 		0x01
#define UPDI_KEY_256 		0x02

#define UPDI_SIB_8BYTES	 	UPDI_KEY_64
#define UPDI_SIB_16BYTES 	UPDI_KEY_128
#define UPDI_SIB_32BYTES 	UPDI_KEY_256

#define UPDI_REPEAT_BYTE 	0x00
#define UPDI_REPEAT_WORD 	0x01

#define UPDI_PHY_SYNC 		0x55
#define UPDI_PHY_ACK  		0x40

#define UPDI_MAX_REPEAT 	0x1FF	// was 0xFF but it looks like there are now chips with a page size of 512

//CS and ASI Register Address map
#define UPDI_CS_STATUSA		0x00
#define UPDI_CS_STATUSB		0x01
#define UPDI_CS_CTRLA		0x02
#define UPDI_CS_CTRLB		0x03
#define UPDI_ASI_KEY_STATUS 0x07
#define UPDI_ASI_RESET_REQ	0x08
#define UPDI_ASI_CTRLA		0x09
#define UPDI_ASI_SYS_CTRLA	0x0A
#define UPDI_ASI_SYS_STATUS 0x0B
#define UPDI_ASI_CRC_STATUS 0x0C

#define UPDI_CTRLA_IBDLY_BIT	7
#define UPDI_CTRLA_RSD_BIT		3
#define UPDI_CTRLB_CCDETDIS_BIT 3
#define UPDI_CTRLB_UPDIDIS_BIT	2

#define UPDI_KEY_NVM		   "NVMProg "
#define UPDI_KEY_CHIPERASE	   "NVMErase"
#define UPDI_KEY_USERROW_WRITE "NVMUs&te"


#define UPDI_ASI_STATUSA_REVID 4
#define UPDI_ASI_STATUSB_PESIG 0

#define UPDI_ASI_KEY_STATUS_CHIPERASE	3
#define UPDI_ASI_KEY_STATUS_NVMPROG		4
#define UPDI_ASI_KEY_STATUS_UROWWRITE	5

#define UPDI_ASI_SYS_STATUS_RSTSYS		5
#define UPDI_ASI_SYS_STATUS_INSLEEP		4
#define UPDI_ASI_SYS_STATUS_NVMPROG		3
#define UPDI_ASI_SYS_STATUS_UROWPROG	2
#define UPDI_ASI_SYS_STATUS_LOCKSTATUS	0

#define UPDI_ASI_SYS_CTRLA_UROWupdi_write_FINAL 1
#define UPDI_ASI_SYS_CTRLA_CLKREQ				0

#define UPDI_RESET_REQ_VALUE 0x59

//FLASH CONTROLLER
#define UPDI_NVMCTRL_CTRLA	  0x00
#define UPDI_NVMCTRL_CTRLB	  0x01
#define UPDI_NVMCTRL_STATUS	  0x02
#define UPDI_NVMCTRL_INTCTRL  0x03
#define UPDI_NVMCTRL_INTFLAGS 0x04
#define UPDI_NVMCTRL_DATAL	  0x06
#define UPDI_NVMCTRL_DATAH	  0x07
#define UPDI_NVMCTRL_ADDRL	  0x08
#define UPDI_NVMCTRL_ADDRH	  0x09
#define UPDI_NVMCTRL_ADDRX	  0x0A

//CTRLA V1
#define UPDI_NVM0CTRL_CTRLA_NOP					0x00
#define UPDI_NVM0CTRL_CTRLA_WRITE_PAGE			0x01
#define UPDI_NVM0CTRL_CTRLA_ERASE_PAGE			0x02
#define UPDI_NVM0CTRL_CTRLA_ERASEWRITE_PAGE		0x03	// use for EEPROM
#define UPDI_NVM0CTRL_CTRLA_PAGE_BUFFER_CLR		0x04	// sue before write
#define UPDI_NVM0CTRL_CTRLA_ERASE_CHIP			0x05
#define UPDI_NVM0CTRL_CTRLA_ERASE_EEPROM		0x06

#define UPDI_NVMCTRL_CTRLA_UPDI_WRITE_FUSE		0x07

//CTRLA V2
#define UPDI_NVM2CTRL_CTRLA_NOCMD				0x00
#define UPDI_NVM2CTRL_CTRLA_FLASH_WRITE			0x02
#define UPDI_NVM2CTRL_CTRLA_FLASH_PAGE_ERASE	0x08
#define UPDI_NVM2CTRL_CTRLA_EEPROM_ERASE_WRITE	0x13
#define UPDI_NVM2CTRL_CTRLA_CHIP_ERASE			0x20
#define UPDI_NVM2CTRL_CTRLA_EEPROM_ERASE		0x30

//CTRLA V3
#define UPDI_NVM3CTRL_CTRLA_NOCMD						0x00
#define UPDI_NVM3CTRL_CTRLA_NOOP						0x01
#define UPDI_NVM3CTRL_CTRLA_FLASH_PAGE_WRITE			0x04
#define UPDI_NVM3CTRL_CTRLA_FLASH_PAGE_ERASE_WRITE		0x05
#define UPDI_NVM3CTRL_CTRLA_FLASH_PAGE_ERASE			0x08
#define UPDI_NVM3CTRL_CTRLA_FLASH_PAGE_BUFFER_CLEAR		0x0F
#define UPDI_NVM3CTRL_CTRLA_EEPROM_PAGE_WRITE			0x14
#define UPDI_NVM3CTRL_CTRLA_EEPROM_PAGE_ERASE_WRITE		0x15
#define UPDI_NVM3CTRL_CTRLA_EEPROM_PAGE_ERASE			0x17
#define UPDI_NVM3CTRL_CTRLA_EEPROM_PAGE_BUFFER_CLEAR	0x1F
#define UPDI_NVM3CTRL_CTRLA_CHIP_ERASE					0x20
#define UPDI_NVM3CTRL_CTRLA_EEPROM_ERASE				0x30


#define UPDI_NVM_STATUS_UPDI_WRITE_ERROR 2
#define UPDI_NVM_STATUS_EEPROM_BUSY		 1
#define UPDI_NVM_STATUS_FLASH_BUSY		 0

#define UPDI_MAX_FILENAME_LEN 256

#define AVR_SYSCFG_ADDRESS	  0x0F00
#define AVR_OCD_ADDRESS		  0x0F80
#define AVR_NVM_ADDRESS		  0x1000
#define AVR_SIG_ADDRESS		  0x1100
#define AVR_FUSES_ADDRESS	  0x1280
#define AVR_USERSIG_ADDRESS	  0x1300
#define AVR_EEPROM_ADDR		  0x1400
#define AVR_FLASH_ADDR		  // varies with chip size
#define AVR_UID_LENGTH		  16 // was 10 but NVM2 has a 16 byte signature

// FUSE definitions
#define AVR_FUSE_UNINITIALIZED 0xFF	// TODO need to find a value that is guaranteed to never be used
#define AVR_NUM_FUSES	 11 // 3 fuses are marked 'reserved': the 4th, 5th, and 10th (0-based #3, #4, and #9)
#define AVR_FUSE_BASE	 0x1280
#define AVR_FUSE_WDTCFG	 0x1280 // watch dog timer
#define AVR_FUSE_BODCFG	 0x1281 // brown out detection
#define AVR_FUSE_OSCCFG	 0x1282 // oscillator frequency
#define AVR_FUSE_RESV3	 0x1283 // must be 0xFF
#define AVR_FUSE_RESV4	 0x1284
#define AVR_FUSE_SYSCFG0 0x1285
#define AVR_FUSE_SYSCFG1 0x1286
#define AVR_FUSE_APPEND	 0x1287
#define AVR_FUSE_BOOTEND 0x1288
#define AVR_FUSE_RESV9	 0x1289
#define AVR_FUSE_LOCK	 0x128A

// --------------------------------------------------------------------------------------------------------
#endif // UPDI programming codes // this #if is to facilitate code collapse for readability

#if 1 // UPDI device data structures and assignment functions // this #if is to facilitate code collapse for readability
// --------------------------------------------------------------------------------------------------------

#define AVR_PAGESIZE_MAX 512	// it was 128 until the new AVR DA and DB chips

// non-volitile memory version
#define NVM_VERSION_0 1
#define NVM_VERSION_2 2

// AVR device types
#define AVR8X_TINY_2X	0
#define AVR8X_TINY_4X	1
#define AVR8X_TINY_8X	2
#define AVR8X_TINY_16X	3
#define AVR8X_MEGA_320	4
#define AVR8X_MEGA_321	5
#define AVR8X_MEGA_480	6
#define AVR8X_AVR_32DA	7
#define AVR8X_AVR_64DA	8
#define AVR8X_AVR_128DA	9

typedef struct {
	uint8_t config_type;
	uint32_t flash_start;
	uint32_t flash_size;
	uint16_t flash_pagesize;
	uint16_t eeprom_size;
	uint16_t eeprom_pagesize;
} DeviceConfiguration;

static DeviceConfiguration g_device_configs[] = {
	// flash address, flash size, flash page size, eeprom size, eeprom page size
	{AVR8X_TINY_2X,   0x8000,     2 * 1024,  64,  64, 32},
	{AVR8X_TINY_4X,   0x8000,     4 * 1024,  64, 128, 32},
	{AVR8X_TINY_8X,   0x8000,     8 * 1024,  64, 128, 32},
	{AVR8X_TINY_16X,  0x8000,    16 * 1024,  64, 256, 32},
	{AVR8X_MEGA_320,  0x4000,    32 * 1024, 128, 256, 64},
	{AVR8X_MEGA_321,  0x8000,    32 * 1024, 128, 256, 64},
	{AVR8X_MEGA_480,  0x4000,    48 * 1024, 128, 256,  1},	// technically these are not 1 byte pages but the interface allows single byte updates
	{AVR8X_AVR_32DA,  0x800000,  32 * 1024, 512, 512,  1},	// technically these are not 1 byte pages but the interface allows single byte updates
	{AVR8X_AVR_64DA,  0x800000,  64 * 1024, 256, 512,  1},	// the page size is actually 512 but it seems, since we erse flash first, we can write 256 at a time
	{AVR8X_AVR_128DA, 0x800000, 128 * 1024, 512, 512,  1},
	};
	// TODO add support for larger chips (64, 128, and 256 KB)

typedef struct {
	uint16_t signature; // all signatures begin with 0x1E and then 2 bytes
	char shortname[8];
	char longname[16];
	uint8_t config_type;
} DeviceIdentification;

DeviceIdentification g_updi_devices[] = {
	//	signature, short id, descriptive name, config
	{0x9123, "t202",   "ATtiny202",  AVR8X_TINY_2X},
	{0x9122, "t204",   "ATtiny204",  AVR8X_TINY_2X},
	{0x9121, "t212",   "ATtiny212",  AVR8X_TINY_2X},
	{0x9120, "t214",   "ATtiny214",  AVR8X_TINY_2X},
	{0x9223, "t402",   "ATtiny402",  AVR8X_TINY_4X},
	{0x9226, "t404",   "ATtiny404",  AVR8X_TINY_4X},
	{0x9225, "t406",   "ATtiny406",  AVR8X_TINY_4X},
	{0x9223, "t412",   "ATtiny412",  AVR8X_TINY_4X},
	{0x9222, "t414",   "ATtiny414",  AVR8X_TINY_4X},
	{0x9221, "t416",   "ATtiny416",  AVR8X_TINY_4X},
	{0x9220, "t417",   "ATtiny417",  AVR8X_TINY_4X},
	{0x9325, "t804",   "ATtiny804",  AVR8X_TINY_8X},
	{0x9324, "t806",   "ATtiny806",  AVR8X_TINY_8X},
	{0x9323, "t807",   "ATtiny807",  AVR8X_TINY_8X},
	{0x9322, "t814",   "ATtiny814",  AVR8X_TINY_8X},
	{0x9321, "t816",   "ATtiny816",  AVR8X_TINY_8X},
	{0x9320, "t817",   "ATtiny817",  AVR8X_TINY_8X},
	{0x9425, "t1604",  "ATtiny1604", AVR8X_TINY_16X},
	{0x9424, "t1606",  "ATtiny1606", AVR8X_TINY_16X},
	{0x9423, "t1607",  "ATtiny1607", AVR8X_TINY_16X},
	{0x9422, "t1614",  "ATtiny1614", AVR8X_TINY_16X},
	{0x9421, "t1616",  "ATtiny1616", AVR8X_TINY_16X},
	{0x9420, "t1617",  "ATtiny1617", AVR8X_TINY_16X},
	{0x9552, "m3208",  "ATmega3208", AVR8X_MEGA_320},
	{0x9553, "m3209",  "ATmega3209", AVR8X_MEGA_320},
	{0x9520, "t3214",  "ATtiny3214", AVR8X_MEGA_321},
	{0x9521, "t3216",  "ATtiny3216", AVR8X_MEGA_321},
	{0x9522, "t3217",  "ATtiny3217", AVR8X_MEGA_321},
	{0x9650, "m4808",  "ATmega4808", AVR8X_MEGA_480},
	{0x9651, "m4809",  "ATmega4809", AVR8X_MEGA_480},
	{0x9615, "r64d28", "AVR64DA28",  AVR8X_AVR_64DA},
	{0x9614, "r64d32", "AVR64DA32",  AVR8X_AVR_64DA},
	{0x9613, "r64d48", "AVR64DA48",  AVR8X_AVR_64DA},
	{0x9612, "r64d64", "AVR64DA64",  AVR8X_AVR_64DA},
	/* TODO need to add the AVR32D and AVR128D chips */
	};

typedef struct {
	char family[8];
	uint8_t nmv_rev;
	char nvm_version[4];
	char ocd_version[4];
	uint8_t signature_bytes[3];
	char dbg_osc_freq;
	uint8_t pdi_rev;
	uint8_t dev_rev;
	int8_t error_16v3;
	int8_t error_16v5;
	int8_t error_20v3;
	int8_t error_20v5;
	uint8_t uid[16];
} DeviceDetails; // data read from chip

typedef struct {
	bool initialized;
	bool unlocked;
	bool verified;
	DeviceIdentification *device;
	DeviceConfiguration *config;
	DeviceDetails details; // this is data read from device
	uint8_t fuses[AVR_NUM_FUSES];
} UPDI;

static UPDI g_updi;	// global device; shared with the various functions

#define IS_UPDI0 (g_updi.details.nmv_rev == NVM_VERSION_0)	// macro for testing if current device is NVM0 or not
#define IS_UPDI2 (g_updi.details.nmv_rev == NVM_VERSION_2)	// macro for testing if current device is NVM2 or not
#define BYTE_DATA false	// is_word = false
#define WORD_DATA true	// is_word = true




static uint8_t _updi_flash_page_buffer[AVR_PAGESIZE_MAX];

// provide lookup using signature or shortname
DeviceIdentification *_updi_chip_lookup(uint16_t sig, char *name) {
	uint8_t max = sizeof(g_updi_devices) / sizeof(DeviceIdentification);

	// search for the correct device identification using any one of: list index, signature, or name
	// the priority for matching is: signature, name, then index
	// because an index of 0 is valid but a signature of 0 is not so we can differentiate missing parameters
	for (uint8_t i = 0; i < max; i++) {
		if (sig) {
			if (g_updi_devices[i].signature == sig) {
				AVRVERBOSE("Found Chip %s\n", g_updi_devices[i].longname);
				return &g_updi_devices[i];
			}
		} else if (name && name[0]) {
			// as a courtesy, we check for both the shortname and the longname; eg: m4809 and ATmega4809
			if (strcasecmp(g_updi_devices[i].shortname, name) == 0) {
				return &g_updi_devices[i];
			}
			if (strcasecmp(g_updi_devices[i].longname, name) == 0) {
				return &g_updi_devices[i];
			} else {
				// if we were not given a signature or a shortname, then we have no chance of finding the device
				return NULL;
			}
		}
	}
	return NULL;
}

static bool _updi_chip_data_init_info(uint16_t sig, char *shortname, bool format) {
	// one of (dev, sig, name) must be valid

	// optionally initialize everything to zeros
	if (format)
		memset(&g_updi, 0, sizeof(UPDI));

	// get the device identification using any one of: list index, signature, or name
	g_updi.device = _updi_chip_lookup(sig, shortname);

	if (!g_updi.device)
		return false;

	uint8_t count = sizeof(g_device_configs) / sizeof(DeviceConfiguration);
	for (uint8_t i = 0; i < count; i++) {
		if (g_updi.device->config_type == g_device_configs[i].config_type) {
			g_updi.config = &g_device_configs[i];
			return true;
		}
	}
	return false;
}

// --------------------------------------------------------------------------------------------------------
#endif // UPDI device data structures and assignment functions // this #if is to facilitate code collapse for readability

#if 1 // LD & ST and NVM functions // this #if is just to facilitate code collapse for readability
// --------------------------------------------------------------------------------------------------------

/*
		Primer:

		"LD" = load data = a UPDI read operation
		"ST" = store data = a UPDI write operation
		"CS" = control status space, eg a specific UPDI block of addresses
		"NVM" = non volatile memory = flash, eeprom, and fuses

		There is also the ability to write a key (?) and set a repeat counter (?) on the UPDI device
*/

// helper for building the common command block
static uint8_t _build_updi_cmd (uint8_t* buf, const char* label, uint8_t cmd, uint32_t address) {
	buf[0] = UPDI_PHY_SYNC;
	buf[1] = cmd;
	buf[2] = (uint8_t)(address & 0xFF);
	buf[3] = (uint8_t)((address >> 8) & 0xFF);
	buf[4] = (uint8_t)((address >> 16) & 0xFF);	// only used for 24 bit address memory

	if (IS_UPDI2) {
		// switch to 24 bit addressing if necessary
		if (buf[1] & UPDI_ADDRESS_16) {
			buf[1] &= ~UPDI_ADDRESS_16;
			buf[1] |= UPDI_ADDRESS_24;
		}
		if (buf[1] & UPDI_DATA_16) {
			buf[1] &= ~UPDI_DATA_16;
			buf[1] |= UPDI_DATA_24;
		}

		AVRDEBUG("BLD: %s %02X %02X %02X %02X %02X\n", label, buf[0], buf[1], buf[2], buf[3], buf[4]);
		return 5;
	}
	// TODO if/when there is UPDI3 this will need to change
	if (IS_UPDI0) {
		AVRDEBUG("BLD: %s %02X %02X %02X %02X ", label, buf[0], buf[1], buf[2], buf[3]);
		return 4;
	}

	AVRDEBUG("build_cmd(%s, %02X, %04X) unknown UPDI type\n", label, cmd, address);
	return 0; // never should happen
}

//Load byte from Control/Status space
static uint8_t _updi_ld_cs(uint8_t address) {

	uint8_t buf[2] = {UPDI_PHY_SYNC, (uint8_t)(UPDI_LDCS | (address & 0x0F))};
	uint8_t recv = 0;
	AVRDEBUG("LD_CS %02X %02X = ", buf[0], buf[1]);

	if (!_updi_serial_send_receive(buf, 2, &recv, 1)) {
		AVRVERBOSE("error\n");
		return 0;
	}
	AVRDEBUG("%02X\n", recv);
	return recv;
}

//Store a value to Control/Status space
static bool _updi_st_cs(uint8_t address, uint8_t value) {
	uint8_t buf[3] = {UPDI_PHY_SYNC, (uint8_t)(UPDI_STCS | (address & 0x0F)), value};
	AVRDEBUG("ST_CS(%02X) %02X %02X %02X\n", value, buf[0], buf[1], buf[2]);

	return _updi_serial_send(buf, 3);
}

//Load a byte direct from an address
static uint8_t _updi_ld(uint32_t address) {
	uint8_t len;
	uint8_t buf[5];
	len = _build_updi_cmd(buf, "LD", UPDI_LDS | UPDI_ADDRESS_16 | UPDI_DATA_8, address);

	uint8_t recv = 0;
	bool success;
	success = _updi_serial_send_receive(buf, len, &recv, 1);
	if (!success) {
		AVRVERBOSE("ld error\n");
		return 0;
	}
	AVRDEBUG("%02X\n", recv);
	return recv;
}


//Loads a number of bytes from the pointer location with pointer post-increment
static bool _updi_ld_ptr_inc(uint8_t *buffer, uint16_t size) {
	uint8_t buf[2] = {UPDI_PHY_SYNC, (UPDI_LD | UPDI_PTR_INC | UPDI_DATA_8)};
	AVRDEBUG("LD_PTR_INC(%d) %02X %02X\n", size, buf[0], buf[1]);

	if (!_updi_serial_send_receive(buf, 2, buffer, size)) {
		AVRVERBOSE("in ld_ptr_inc(): error\n");
		return false;
	}
	return true;
}

#if 0
//Load a 16-bit word value from the pointer location with pointer post-increment
static bool _updi_ld_ptr_inc16(uint8_t *buffer, uint16_t numwords) {
	uint8_t buf[2] = {UPDI_PHY_SYNC, (UPDI_LD | UPDI_PTR_INC | UPDI_DATA_16)};
	AVRDEBUG("LD_PTR_INC_16(%d) %02X %02X = ", numwords, buf[0], buf[1]);

	if (!_updi_serial_send_receive(buf, 2, buffer, (numwords << 1))) {
		AVRVERBOSE("ld_ptr_inc16 error\n");
		return false;
	}
	uint16_t word = (buffer[0] << 8) | buffer[1];
	AVRDEBUG("%04X\n", word);

	return true;
}
#endif

//Store a single byte or word value directly to a 16-bit address
static bool _updi_st_value(uint32_t address, uint16_t value, bool use_words) {
	uint8_t buf[5];
	uint8_t len;
	len = _build_updi_cmd(buf, "ST", UPDI_STS | UPDI_ADDRESS_16 | UPDI_DATA_8, address);

	uint8_t recv = 0;

	if (!_updi_serial_send_receive(buf, len, &recv, 1)) {
		AVRVERBOSE("send recv failed address %04X recv %02X\n", address, recv);
		return false;
	} else {
		if (recv != UPDI_PHY_ACK) {
			AVRVERBOSE("no ACK from address %04X\n", address);
			return false;
		}
	}

	bool success;
	buf[0] = (uint8_t)(value & 0xFF);
	buf[1] = (uint8_t)((value >> 8) & 0xFF);

	AVRDEBUG(" ");
	AVRDEBUG("%02X", buf[0]);
	if (use_words) { AVRDEBUG("%02X", buf[1]); }
	AVRDEBUG(" = ");

	if (use_words) {
		success = _updi_serial_send_receive(buf, 2, &recv, 1);
	} else {
		success = _updi_serial_send_receive(buf, 1, &recv, 1);
	}

	AVRDEBUG("%02X\n", recv);

	if (!success) {
		AVRVERBOSE("send recv value %02X error\n", value);
		return false;
	} else {
		if (recv != UPDI_PHY_ACK) {
			AVRVERBOSE("no ACK after value %02X sent\n", value);
			return false;
		}
	}

	return true;
}

static bool _updi_st(uint32_t address, uint8_t value) {
	return _updi_st_value(address, value, BYTE_DATA);	// store byte
}

#if 0
static bool _updi_st16(uint32_t address, uint16_t value) {
	return _updi_st_value(address, value, WORD_DATA);	// store word
}
#endif

//Set the pointer location
static bool _updi_set_ptr(uint32_t address, bool use_words) {
	AVRDEBUG("SeT_PTR(%04X)\n", address);

	uint16_t len;
	uint8_t buf[5];

	len = _build_updi_cmd(buf, "SET_PTR", UPDI_ST | UPDI_PTR_ADDRESS | UPDI_DATA_16, address); // (use_words ? UPDI_DATA_16 : UPDI_DATA_8)

	uint8_t recv = 0;
	if (!_updi_serial_send_receive(buf, len, &recv, 1)) {
		AVRVERBOSE("error\n");
		return false;
	} else {
		if (recv != UPDI_PHY_ACK) {
			AVRVERBOSE("no ACK\n");
			return false;
		}
	}
	AVRDEBUG("ACK\n");
	return true;
}

//Store data to the pointer location with pointer post-increment
static bool _updi_st_ptr_inc(uint8_t *data, uint32_t size) {
	uint8_t buf[3] = {UPDI_PHY_SYNC, UPDI_ST | UPDI_PTR_INC | UPDI_DATA_8, data[0]};
	uint8_t recv;
	AVRDEBUG("ST_PTR_INC(%d) %02X %02X %02X = ", size, buf[0], buf[1], buf[2]);

	if (!_updi_serial_send_receive(buf, 3, &recv, 1)) {
		AVRVERBOSE("error\n");
		return false;
	} else {
		if (recv != UPDI_PHY_ACK) {
			AVRVERBOSE("no ACK");
			return false;
		}
	}

	for (uint32_t i = 1; i < size; i++) {
		recv = 0;
		AVRDEBUG(" %02X", data[i]);
		if (!_updi_serial_send_receive(data + i, 1, &recv, 1)) {
			AVRVERBOSE("send recv error\n");
			return false;
		} else {
			if (recv != UPDI_PHY_ACK) {
				AVRVERBOSE("send recv no ACK\n");
				return false;
			}
		}
	}
	AVRDEBUG("\n");
	return true;
}

//Store a 16-bit word value to the pointer location with pointer post-increment. Disable acks when we do this, to reduce latency.
static void _updi_st_ptr_inc16(uint8_t *data, uint32_t numwords) {
	uint8_t buf[2] = {UPDI_PHY_SYNC, UPDI_ST | UPDI_PTR_INC | UPDI_DATA_16};
	AVRDEBUG("ST_PTR_16(%d) %02X %02X\n", numwords, buf[0], buf[1]);

	uint8_t ctrla_ackon = 1 << UPDI_CTRLA_IBDLY_BIT;
	uint8_t ctrla_ackoff = ctrla_ackon | (1 << UPDI_CTRLA_RSD_BIT);

	//disable acks
	_updi_st_cs(UPDI_CS_CTRLA, ctrla_ackoff);

	_updi_serial_send(buf, 2); //no response expected
	_updi_serial_send(data, numwords << 1);

	//reenable acks
	_updi_st_cs(UPDI_CS_CTRLA, ctrla_ackon);

	return;
}

//Store a value to the repeat counter
static void _updi_set_repeat(uint16_t repeats, bool use_words) {
	//AVRVERBOSE("set repeat %d\n", repeats);
	repeats -= 1;
	uint8_t buf[4];
	buf[0] = UPDI_PHY_SYNC;
	buf[1] = UPDI_REPEAT | (use_words ? UPDI_REPEAT_WORD : UPDI_REPEAT_BYTE);
	buf[2] = (uint8_t)(repeats & 0xFF);
	if (use_words) {
		buf[3] = (uint8_t)((repeats >> 8) & 0xFF);
		AVRDEBUG("REPT-WORD %02X %02X %02X %02X\n", buf[0], buf[1], buf[2], buf[3]);
		_updi_serial_send(buf, 4);
	} else {
		AVRDEBUG("REPT-BYTE %02X %02X %02X\n", buf[0], buf[1], buf[2]);
		_updi_serial_send(buf, 3);
	}
	return;
}

//Write a key
static void _updi_write_key(uint8_t size, uint8_t *key) {
	uint16_t len = 8 << size;
	uint8_t buf[2] = {UPDI_PHY_SYNC, (uint8_t)(UPDI_KEY | UPDI_KEY_KEY | size)};
	uint8_t key_reversed[len];
	AVRDEBUG("KEY %02X %02X  ", buf[0], buf[1]);

	for (uint16_t i = 0; i < len; i++) {
		AVRDEBUG("%c", key[i]);
		key_reversed[i] = key[len - 1 - i];
	}
	AVRDEBUG("\n");

	_updi_serial_send(buf, 2);
	_updi_serial_send(key_reversed, 8 << size);

	return;
}

// --------------------------------------------------------------------------------------------------------
#endif // LD & ST and NVM functions // this #if is just to facilitate code collapse for readability

#if 1 // UPDI operations // this #if is just to facilitate code collapse for readability
// --------------------------------------------------------------------------------------------------------

/*
	Primer:

	the force reset function plays with the UART to attempt to reset the UPDI state of the device

	init / term for basic bookends for other operations
	'handshake' is just a status check and expects a non-zero ACK
	enter / leave device programming mode
	erase chip
	read / write data by bytes / words
	read / write fuses
	read / write flash
*/

static bool _updi_device_force_reset() {

	AVRVERBOSE("Sending BREAK BREAK\n");
	/*
		The BREAK character is used to reset the internal state of the UPDI to the default setting.
		This is useful if the UPDI enters an Error state due to a communication error or when the
		synchronization between the debugger and the UPDI is lost.

		To ensure that a BREAK is successfully received by the UPDI in all cases, the debugger
		must send two consecutive BREAK characters. The first BREAK will be detected if the UPDI
		is in Idle state and will not be detected if it is sent while the UPDI is receiving or
		transmitting (at a very low baud rate). However, this will cause a frame error for the
		reception (RX) or a contention error for the transmission (TX), and abort the ongoing
		operation. The UPDI will then detect the next BREAK successfully.

		The minimum BREAK is 6ms (a 20Mhz oscillator) and the worst-case is 25ms.

		We could calculate the optimal BREAK using details about the chip ... or not
	*/

	_updi_serial_force_break();
	return true;
}

static bool _updi_check() {
	AVRVERBOSE("_updi_check()\n");
	if (_updi_ld_cs(UPDI_CS_STATUSA) != 0) {
		return true;
	}
	return false;
}

static void _updi_send_handshake() {
	uint8_t buf = UPDI_BREAK;
	_updi_serial_send(&buf, 1);
	return;
}

static void _updi_init(bool force) {
	AVRVERBOSE("_updi_init()\n");

#if 1
	if (force) {
		// seems we need to toggle the power outside of the init/setup at teh start of programming
		avrPowerOff();
		delay(500);
		avrPowerOn();
		delay(500);
	}
#endif

	_updi_serial_init();
	_updi_send_handshake();

	_updi_st_cs(UPDI_CS_CTRLB, 1 << UPDI_CTRLB_CCDETDIS_BIT);
	_updi_st_cs(UPDI_CS_CTRLA, 1 << UPDI_CTRLA_IBDLY_BIT);
	return;
}

static void _updi_term() {
	_updi_serial_term();
	delay(5);
}

static void _updi_apply_reset() {
	AVRVERBOSE("Applying reset\n");
	_updi_st_cs(UPDI_ASI_RESET_REQ, UPDI_RESET_REQ_VALUE);

	if (!(_updi_ld_cs(UPDI_ASI_SYS_STATUS) & (1 << UPDI_ASI_SYS_STATUS_RSTSYS))) {
		AVRVERBOSE("error applying reset\n");
		return;
	}

	delay(5);
	AVRVERBOSE("Releasing reset\n");
	_updi_st_cs(UPDI_ASI_RESET_REQ, 0x00);
	uint8_t retries = (255-10);	// at most 10 retries

	while (retries++) {
		uint8_t b;
		if (!((b = _updi_ld_cs(UPDI_ASI_SYS_STATUS)) & (1 << UPDI_ASI_SYS_STATUS_RSTSYS)))
			break;
		//AVRVERBOSE("Wait for release %03d ( %02X != %02X)\n", retries, b, (1 << UPDI_ASI_SYS_STATUS_RSTSYS));
	}
	if (!retries) {
		// if our retry counter rolled over, then we failed to reset
		AVRVERBOSE("Error releasing reset\n");
	}
}

//Waits for the device to be unlocked. All devices boot up as locked until proven otherwise
static bool _updi_wait_unlocked(uint32_t timeout) {
	unsigned long end = millis() + timeout;

	while (millis() < end) {
		if (!(_updi_ld_cs(UPDI_ASI_SYS_STATUS) & (1 << UPDI_ASI_SYS_STATUS_LOCKSTATUS))) {
			g_updi.unlocked = true;
			return true;
		}
	}

	AVRVERBOSE("TIMEOUT WAITING FOR DEVICE TO UNLOCK\n");
	g_updi.unlocked = false;

	return false;
}

static bool _updi_is_prog_mode() {
	if (_updi_ld_cs(UPDI_ASI_SYS_STATUS) & (1 << UPDI_ASI_SYS_STATUS_NVMPROG)) {
		return true;
	} else {
		return false;
	}
}

//Inserts the NVMProg key and updi_checks that its accepted
static bool _updi_progmode_key() {
	_updi_write_key(UPDI_KEY_64, (uint8_t *)UPDI_KEY_NVM);

	uint8_t key_status = _updi_ld_cs(UPDI_ASI_KEY_STATUS);

	if (!(key_status & (1 << UPDI_ASI_KEY_STATUS_NVMPROG))) {
		return false;
	}

	return true;
}

static bool _updi_enter_progmode() {

	//Enter NVMProg key
	if (!_updi_is_prog_mode()) {
		if (!_updi_progmode_key()) {
			return false;
		}
	}

	_updi_apply_reset();

	//Wait for unlock
	if (!_updi_wait_unlocked(100)) {
		AVRVERBOSE("FAILED TO ENTER NVM PROGRAMMING MODE, DEVICE IS LOCKED\n");
		return false;
	}

	//updi_check for NVMPROG flag
	if (!_updi_is_prog_mode()) {
		AVRVERBOSE("STILL NOT IN PROG MODE\n");
		return false;
	}
	g_updi.unlocked = true;
	return true;
}

//Disables UPDI which releases any keys enabled
static void _updi_leave_progmode() {
	AVRVERBOSE("leaving progmode...\n");
	_updi_apply_reset();
	_updi_st_cs(UPDI_CS_CTRLB, (1 << UPDI_CTRLB_UPDIDIS_BIT) | (1 << UPDI_CTRLB_CCDETDIS_BIT));

	return;
}

//Unlock and erase
static bool _updi_unlock_device() {
	AVRVERBOSE("UNLOCKING AND ERASING\n");

	//enter key
	_updi_write_key(UPDI_KEY_64, (uint8_t *)UPDI_KEY_CHIPERASE);

	//updi_check key status
	uint8_t key_status = _updi_ld_cs(UPDI_ASI_KEY_STATUS);

	if (!(key_status & (1 << UPDI_ASI_KEY_STATUS_CHIPERASE))) {
		AVRVERBOSE("Unlock error: key not accepted\n");
		return false;
	}

	//Insert NVMProg key as well
	//In case of CRC being enabled, the device must be left in programming mode after the erase
	//to allow the CRC to be disabled (or flash reprogrammed)
	_updi_progmode_key();

	_updi_apply_reset();

	//wait for unlock
	if (!_updi_wait_unlocked(100)) {
		AVRVERBOSE("Failed to chip erase using key\n");
		return false;
	}

	AVRVERBOSE("UNLOCKED DEVICE\n");

	return true;
}

//Reads a number of bytes of data from UPDI
static bool _updi_read_data(uint32_t address, uint8_t *buf, uint32_t size, bool use_words) {
	AVRVERBOSE("updi_read_data(%04X, , %d, %s)\n", address, size, (use_words ? "WORD":"BYTE"));

	//Range updi_check
	if (size > UPDI_MAX_REPEAT + 1) {
		AVRVERBOSE("read_data error: cant read that many bytes at once\n");
		return false;
	}

	if (!_updi_set_ptr(address, use_words)) { //Store address pointer
		AVRVERBOSE("read_data() error: st_ptr()\n");
		return false;
	}
	if (size > 1)
		_updi_set_repeat(size, use_words); //Set repeat

	if (!_updi_ld_ptr_inc(buf, size)) {
		AVRVERBOSE("updi_read_data(): ld_ptr_inc error\n");
		return false;
	}

	return true;
}


//Writes a number of bytes to memory
static bool _updi_write_data(uint32_t address, uint8_t *data, uint32_t len) {
	AVRVERBOSE("updi_write_data(%04X, , %d)\n", address, len);
	if (len == 1) {
		if (!_updi_st(address, data[0])) {
			AVRVERBOSE("in _updi_write_data() error: st ret false\n");
			return false;
		}
	} else if (len == 2) {
		if (!_updi_st(address, data[0])) {
			AVRVERBOSE("in _updi_write_data() error: st ret false\n");
			return false;
		}
		if (!_updi_st(address + 1, data[1])) {
			AVRVERBOSE("in _updi_write_data() error: st ret false\n");
			return false;
		}
	} else {
		//Range updi_check
		if (len > UPDI_MAX_REPEAT + 1) {
			AVRVERBOSE("in _updi_write_data() error: invalid length\n");
			return false;
		}

		//store address
		if (!_updi_set_ptr(address, WORD_DATA)) {
			AVRVERBOSE("in _updi_write_data() error: couldnt st_ptr(address)\n");
			return false;
		}

		//set up repeat

		_updi_set_repeat(len, WORD_DATA);
		if (!_updi_st_ptr_inc(data, len)) {
			AVRVERBOSE("in _updi_write_data() error: couldnt st_ptr_inc() error\n");
			return false;
		}
	}

	return true;
}

//Writes a number of words to memory
static bool _updi_write_data_words(uint32_t address, uint8_t *data, uint32_t numwords) {
	AVRVERBOSE("updi_write_data_words(%04X, , %d)\n", address, numwords);
	if (numwords == 1) {
		return _updi_write_data(address, data, 2);
	} else {
		//Range updi_check
		if (numwords > ((UPDI_MAX_REPEAT >> 1) + 1)) {
			AVRVERBOSE("in _updi_write_data_words error: invalid length %d WORDs\n", numwords);
			return false;
		}

		//Store address
		if (!_updi_set_ptr(address, WORD_DATA)) {
			AVRVERBOSE("in _updi_write_data_words error: st_ptr() error\n");
			return false;
		}

		//Set up repeat
		_updi_set_repeat(numwords, WORD_DATA);
		_updi_st_ptr_inc16(data, numwords);
	}

	return true;
}

//Execute NVM command
static bool _updi_execute_nvm_command(uint8_t command) {
	AVRDEBUG("execute_nvm_command(%02X)\n", command);
	if (!_updi_st(AVR_NVM_ADDRESS + UPDI_NVMCTRL_CTRLA, command)) {
		AVRVERBOSE("in execute_nvm_command() error: st() false return\n");
		return false;
	}
	return true;
}

//Waits for the NVM controller to be ready
static bool _updi_wait_nvm_ready() {
	uint32_t end = millis() + 10000;

	while (millis() < end) {
		uint8_t status = _updi_ld(AVR_NVM_ADDRESS + UPDI_NVMCTRL_STATUS);
		if (status & (1 << UPDI_NVM_STATUS_UPDI_WRITE_ERROR)) {
			AVRVERBOSE("in wait_flash_ready() error: nvm error\n");
			return false;
		}

		if (!(status & ((1 << UPDI_NVM_STATUS_EEPROM_BUSY) | (1 << UPDI_NVM_STATUS_FLASH_BUSY)))) {
			return true;
		}
	}

	AVRVERBOSE("in wait_flash_ready() error: wait flash ready timed out\n");
	return false;
}

//Writes a page of data to NVM. By default the PAGE_WRITE command is used, which requires that the page is already erased. By default word access is used (flash)
static bool _updi_write_nvm(uint32_t address, uint8_t *data, uint32_t len, bool use_words) {
	//wait for NVM controller to be ready
	if (!_updi_wait_nvm_ready()) {
		AVRVERBOSE("in updi_write_nvm() error: cant wait flash ready\n");
		return false;
	}

	//Clear page buffer
	if (IS_UPDI0) {
		if (!_updi_execute_nvm_command(UPDI_NVM0CTRL_CTRLA_PAGE_BUFFER_CLR)) {
			AVRVERBOSE("in updi_write_nvm() error: execute_nvm_command(UPDI_NVMCTRL_CTRLA_PAGE_BUFFER_CLR)\n");
			return false;
		}

		//wait for NVM controller to be ready
		if (!_updi_wait_nvm_ready()) {
			AVRVERBOSE("in updi_write_nvm() error: cant wait flash ready after page buffer clear\n");
			return false;
		}
	}

	uint8_t cmd = 0;
	bool is_eeprom = (address >= AVR_EEPROM_ADDR) && (address <= (AVR_EEPROM_ADDR + g_updi.config->eeprom_size));
	AVRDEBUG("updi_write_nvm(%04X,,%d,%s access) in %s memory\n", address, len, (use_words ? "WORD" : "BYTE"), (is_eeprom ? "EEPROM" : " FLASH"));

	if (IS_UPDI2) {
		cmd = (is_eeprom ? (UPDI_NVM2CTRL_CTRLA_EEPROM_ERASE_WRITE) : UPDI_NVM2CTRL_CTRLA_FLASH_WRITE);
		if (!_updi_execute_nvm_command(cmd)) {
			AVRVERBOSE("in updi_write_nvm() error: execute_nvm_command(%02x)\n", cmd);
			return false;
		}
	}

	// Load the page buffer by writing directly to location
	// FYI: word access is significantly faster for write but no faster for read

	if (use_words) {
		if (!_updi_write_data_words(address, data, len >> 1)) {
			AVRVERBOSE("in updi_write_nvm() error: _updi_write_data_words(%X %d) error\n", address, (len >> 1));
			return false;
		}
	} else {
		if (!_updi_write_data(address, data, len)) {
			AVRVERBOSE("in updi_write_nvm() error: _updi_write_data() error\n");
			return false;
		}
	}

	if (IS_UPDI0) {
		cmd = (is_eeprom ? (UPDI_NVM0CTRL_CTRLA_ERASEWRITE_PAGE) : UPDI_NVM0CTRL_CTRLA_WRITE_PAGE);
		//Write the page to NVM, maybe erase first
		if (!_updi_execute_nvm_command(cmd)) {
			AVRVERBOSE("in updi_write_nvm() error: execute_nvm_command(UPDI_NVMCTRL_CTRLA_WRITE_PAGE)\n");
			return false;
		}
	}

	if (IS_UPDI2) {
		if (!_updi_execute_nvm_command(UPDI_NVM2CTRL_CTRLA_NOCMD)) {
			AVRVERBOSE("in updi_write_nvm() error: execute_nvm_command(UPDI_NVM2CTRL_CTRLA_NOCMD)\n");
			return false;
		}
	}

	//wait for NVM controller to be ready
	if (!_updi_wait_nvm_ready()) {
		AVRVERBOSE("in updi_write_nvm() error: cant wait flash ready after commit page\n");
		return false;
	}

	return true;
}

//Get device info
static bool _updi_get_device_info() {
	AVRVERBOSE("_updi_get_device_info()\n");
	uint8_t buf[2] = {UPDI_PHY_SYNC, UPDI_KEY | UPDI_KEY_SIB | UPDI_SIB_16BYTES};
	uint8_t recv[AVR_UID_LENGTH]; // buffer for received data; reused multiple times
	AVRDEBUG("SIB %02X %02X\n", buf[0], buf[1]);

	_updi_chip_data_init_info(0x00, NULL, true);	// clear out any prior data

	if (!_updi_serial_send_receive(buf, 2, recv, 16)) {
		AVRVERBOSE("device info recv error\n");
	} else {
		for (uint8_t i = 0; i < 7; i++)
			g_updi.details.family[i] = recv[i];
		for (uint8_t i = 8; i < 11; i++)
			g_updi.details.nvm_version[i - 8] = recv[i];
		for (uint8_t i = 11; i < 14; i++)
			g_updi.details.ocd_version[i - 11] = recv[i];
		g_updi.details.dbg_osc_freq = recv[15];

		AVRVERBOSE("Chip Family:  %s\n", g_updi.details.family);
		AVRVERBOSE("Chip NVM:     %s\n", g_updi.details.nvm_version);
		AVRVERBOSE("Chip OCD:     %s\n", g_updi.details.ocd_version);
		g_updi.details.nmv_rev = (g_updi.details.nvm_version[2] == '2' ? NVM_VERSION_2 : NVM_VERSION_0);
		AVRVERBOSE("NVM Revision: %d\n", g_updi.details.nmv_rev);

		// if we are in program mode we can get additional details
		// the SIGROW address starts with 3 bytes which hold the chip type (always starts with 0x1E
		// the next 10 bytes are the chip unique id
		if (_updi_is_prog_mode()) {
			if (!_updi_read_data(AVR_SIG_ADDRESS, recv, 3, WORD_DATA))
				return false;
			for (int i = 0; i < 3; i++)
				g_updi.details.signature_bytes[i] = recv[i];

			// our data lookup uses the 16 bit value of the chip type (ignoring the leading 0x1E)
			uint16_t signature = ((recv[1] << 8) + recv[2]);
			AVRVERBOSE("Chip signature: %04X\n", signature);

			if (IS_UPDI0) {
				if (!_updi_read_data(AVR_SIG_ADDRESS+3, recv, 10, WORD_DATA))
					return false;
				for (int i = 0; i < 10; i++)
					g_updi.details.uid[i] = recv[i];
			}
			if (IS_UPDI2) {
				if (!_updi_read_data(AVR_SIG_ADDRESS+0x10, recv, AVR_UID_LENGTH, WORD_DATA))
					return false;
				for (int i = 0; i < 16; i++)
					g_updi.details.uid[i] = recv[i];
			}

			if (!_updi_chip_data_init_info(signature, NULL, false))
				return false;

			if (!_updi_read_data(AVR_SYSCFG_ADDRESS, recv, 1, WORD_DATA))
				return false;

			g_updi.details.dev_rev = recv[0];

			g_updi.details.pdi_rev = (_updi_ld_cs(UPDI_CS_STATUSA) >> 4);

			// mostly for geeky interest, we also grab the oscillator error correction data
			if (!_updi_read_data(AVR_SIG_ADDRESS + 0x20, recv, 6, WORD_DATA))
				return false;

			// the error data consists of 2 bytes of temperature sensors, 2 bytes of oscillator at 3v and 2 bytes of oscillator at 5 bytes
			g_updi.details.error_16v3 = recv[2];
			g_updi.details.error_16v5 = recv[3];
			g_updi.details.error_20v3 = recv[4];
			g_updi.details.error_20v5 = recv[5];

			g_updi.initialized = true;
		}
	}
	return g_updi.initialized;
}

//Does a chip erase using the NVM controller Note that on locked devices this it not possible and the ERASE KEY has to be used instead
static bool _updi_erase_chip() {
	AVRVERBOSE("ERASING CHIP...\n");

	//Wait until NVM CTRL is ready to erase
	if (!_updi_wait_nvm_ready()) {
		AVRVERBOSE("in updi_erase_chip() error: timeout waiting for flash ready before erase\n");
		return false;
	}

	if (IS_UPDI2) {
		if (!_updi_execute_nvm_command(UPDI_NVM2CTRL_CTRLA_CHIP_ERASE)) {
			AVRVERBOSE("in updi_write_nvm() error: execute_nvm_command(UPDI_V2_NVMCTRL_CTRLA_CHIP_ERASE)\n");
			return false;
		}
	}

	if (IS_UPDI0) {
		if (!_updi_execute_nvm_command(UPDI_NVM0CTRL_CTRLA_ERASE_CHIP)) {
			AVRVERBOSE("in updi_erase_chip() error: execute_nvm_command(UPDI_NVM0CTRL_CTRLA_ERASE_CHIP) failed()\n");
			return false;
		}
	}

	//Wait to finish
	if (!_updi_wait_nvm_ready()) {
		AVRVERBOSE("in updi_erase_chip() error: timeout waiting for flash ready after erase\n");
		return false;
	}

	AVRVERBOSE("ERASED\n");

	return true;
}

#if 0
//Does a eeprom erase using the NVM controller Note that on locked devices this it not possible and the ERASE KEY has to be used instead
static bool _updi_erase_eeprom() {
	AVRVERBOSE("ERASING EEPROM...\n");

	//Wait until NVM CTRL is ready to erase
	if (!_updi_wait_nvm_ready()) {
		AVRVERBOSE("in updi_erase_eeprom() error: timeout waiting for flash ready before erase\n");
		return false;
	}

	//Erase
	if (IS_UPDI2) {
		if (!_updi_execute_nvm_command(UPDI_NVM2CTRL_CTRLA_EEPROM_ERASE)) {
			AVRVERBOSE("in updi_erase_eeprom() error: execute_nvm_command(UPDI_NVM2CTRL_CTRLA_EEPROM_ERASE) failed()\n");
			return false;
		}
	}
	if (IS_UPDI0) {
		if (!_updi_execute_nvm_command(UPDI_NVM0CTRL_CTRLA_ERASE_EEPROM)) {
			AVRVERBOSE("in updi_erase_eeprom() error: execute_nvm_command(UPDI_NVMCTRL_CTRLA_ERASE_EEPROM) failed()\n");
			return false;
		}
	}

	//Wait to finish
	if (!_updi_wait_nvm_ready()) {
		AVRVERBOSE("in updi_erase_eeprom() error: timeout waiting for flash ready after erase\n");
		return false;
	}

	AVRVERBOSE("ERASED\n");

	return true;
}
#endif

//Reads one fuse value
static uint8_t _updi_read_fuse(uint8_t fuse) {
	AVRVERBOSE("_updi_read_fuse(%d)\n", fuse);

	if (!_updi_is_prog_mode()) {
		AVRVERBOSE("in updi_read_fuse() error: not in prog mode\n");
		return 0;
	}

	return _updi_ld(AVR_FUSE_BASE + fuse);
}

//Writes one fuse value
static bool _updi_write_fuse(uint8_t fuse, uint8_t value) {
	uint8_t data;

	if (!_updi_is_prog_mode()) {
		AVRVERBOSE("in updi_write_fuse() error: not in prog mode\n");
		return false;
	}

	if (!_updi_wait_nvm_ready()) {
		AVRVERBOSE("in updi_write_fuse() error: cant wait flash ready\n");
		return false;
	}

	data = (AVR_FUSE_BASE + fuse) & 0xff;
	if (!_updi_write_data(AVR_NVM_ADDRESS + UPDI_NVMCTRL_ADDRL, &data, 1)) {
		AVRVERBOSE("in updi_write_fuse() error: write data fail\n");
		return false;
	}

	data = (AVR_FUSE_BASE + fuse) >> 8;
	if (!_updi_write_data(AVR_NVM_ADDRESS + UPDI_NVMCTRL_ADDRH, &data, 1)) {
		AVRVERBOSE("in updi_write_fuse() error: write data fail\n");
		return false;
	}

	if (!_updi_write_data(AVR_NVM_ADDRESS + UPDI_NVMCTRL_DATAL, &value, 1)) {
		AVRVERBOSE("in updi_write_fuse() error: write data fail\n");
		return false;
	}

	data = UPDI_NVMCTRL_CTRLA_UPDI_WRITE_FUSE;
	if (!_updi_write_data(AVR_NVM_ADDRESS + UPDI_NVMCTRL_CTRLA, &data, 1)) {
		AVRVERBOSE("in updi_write_fuse() error: write data fail\n");
		return false;
	}

	return true;
}

//read address space - the 'save' flag tells us if we need to save the contents to the buffer or compare the contents to the buffer
static bool _updi_read_address_space(uint32_t start, uint16_t pagesize, uint32_t maxsize, ihexStream* data, bool use_words, bool save) {
	AVRVERBOSE("read_address_space(%04X, %d, %d, , %s, %s)\n", start, pagesize, maxsize, (use_words ? "WORD" : "BYTE"), (save ? "SAVE" : "VERIFY"));

	if (pagesize > AVR_PAGESIZE_MAX) {
		MESSAGE("pagesize %d bytes exceeds %d byte maximum\n", pagesize, AVR_PAGESIZE_MAX);
		return false;
	}

	if (!_updi_is_prog_mode()) {
		AVRVERBOSE("dump_flash() error: not in prog mode\n");
		return false;
	}

	uint32_t size;
	if (save) {
		size = maxsize;
		data->clear();
		MESSAGE("Reading");
	} else {
		data->rewind();
		size = data->available();
		MESSAGE("verifying");
	}

	uint32_t pages = size / pagesize;
	uint32_t address = start;

	if (size % pagesize != 0)	// round up to a full page
		pages++;

	MESSAGE(" %d bytes from %04X as %d pages of %d size ", size, start, pages, pagesize); // deliberately no LF; the start + progress + finish messages are all combined

	uint8_t err_count = 0;
	uint32_t i = 0;
	while (i < pages) {
		bool success;
		//AVRVERBOSE("read page %d of %d to addr %04X of %04X\n", i, pages, i*pagesize, size);
		success = _updi_read_data(address, _updi_flash_page_buffer, pagesize, use_words);

		if (!success) {
			err_count++;
			AVRVERBOSE("read words error at 0x%04X\n", address);
			if (err_count > 4) { // too many consecutive errors
				MESSAGE(" Failed at %04X address\n", address);
				return false;
			}
			continue;
		} else
			err_count = 0;

		if (save) {
			for (uint32_t j = 0; j < pagesize; j++) {
				AVRDEBUG("%02X ", _updi_flash_page_buffer[j]);
				data->write(_updi_flash_page_buffer[j]);
			}
			AVRDEBUG("\n");
		}
		else {
			// compare the data
			err_count = 0;
			for (uint32_t j = 0; j < pagesize; j++) {
				int c = data->read();
				if (c < 0)
					c = 0xFF;	// fill end of data with the FILLER_BYTE
				if (_updi_flash_page_buffer[j] != c) {
					// bytes don't match
					err_count++;
					//AVRVERBOSE("Verification %02X != %02X at 0x%04X\n", _updi_flash_page_buffer[j], c, (address + j));
					//if (err_count > 4)	// shouldn't we fail if even a single byte is wrong
					{
						MESSAGE("\nVerification %02X != %02X at 0x%04X\n", _updi_flash_page_buffer[j], c, (address + j));
						return false;
					}
				}
			}
		}
		//AVRVERBOSE("read_flash page=%d of %d bytes=%d into 0x%04X\n", i + 1, pages, pagesize, (i * pagesize));
		if (pages > 1) MESSAGE(".");
		i++;
		address += pagesize;
	} // end white

	// if we finished and this was a compare operation then we set the verified flag
	// if we finished and this was a read operation then we update the used buffer size
	if (!save)
		g_updi.verified = true;
	else {
		// only attempt to resize the full flash address space
		if (maxsize == g_updi.config->flash_size)
			data->resize(g_updi.config->flash_size, pagesize);
	}

	if (pages > 1) MESSAGE("\n");
	//MESSAGE("Success\n");

	return true;
}

//Write memory in pages
static bool _updi_write_address_space(uint32_t start, uint32_t pagesize, uint32_t maxsize, ihexStream* data, bool use_words) {
	if (!_updi_is_prog_mode()) {
		AVRVERBOSE("in updi_write_flash error: not in prog mode\n");
		return false;
	}
	data->rewind();
	int32_t len = data->available();
	int32_t numpages = len / pagesize;
	uint32_t address = start;

	if (len % pagesize) {
		//pad data
		// since the data buffer was initialized with FF, all we need to do is increase 'len' to a page boundary
		AVRVERBOSE("PADDING DATA\n");
		numpages++;
		len = numpages * pagesize;
	}

	MESSAGE("writing %d bytes to %04X as %d pages of %d size\n", len, start, numpages, pagesize); // deliberately no LF; the start + progress + finish messages are all combined

#if 0
	if (!_updi_wait_nvm_ready()) {
		AVRVERBOSE("in updi_write_nvm() error: cant wait flash ready\n");
		return false;
	}
#endif

	//program by page
	for (uint32_t i = 0; i < numpages; i++) {
		uint8_t page[pagesize];

		for (uint32_t j = 0; j < pagesize; j++) {
			int c = data->read();
			page[j] = c;
		}

		if (!_updi_write_nvm(address, page, pagesize, use_words)) {
			MESSAGE("Failed\n");
			return false;
		}

		address += pagesize;
		MESSAGE(".");		   		// give progress indication
	}
	MESSAGE("\n");

	if (!_updi_wait_nvm_ready()) {
		AVRVERBOSE("in updi_write_nvm() error: cant wait flash ready\n");
		return false;
	}
	MESSAGE("Finished\n");

	return true;
}

// --------------------------------------------------------------------------------------------------------
#endif // UPDI operations // this #if is just to facilitate code collapse for readability

static bool _updi_task_setup(bool unlock) {
	MESSAGE("Checking for UPDI chip\n");

	g_updi.initialized = false;
	g_updi.unlocked = false;
	_updi_serial_retry_count = 0;

	_updi_init(true);

	if (_updi_check()) {
		AVRVERBOSE("UPDI ALREADY INITIALISED\n");
		g_updi.initialized = true;
	}
	else {
		AVRVERBOSE("UPDI not initialised\n");

		if (!_updi_device_force_reset()) {
			AVRVERBOSE("double BREAK reset failed\n");
			return false;
		}

		_updi_init(false);	// re-init the UPDI interface

		if (_updi_check()) {
			AVRVERBOSE("UPDI INITIALISED\n");
			g_updi.initialized = true;
		}
		else {
			AVRVERBOSE("Cannot initialise UPDI, aborting.\n");
			g_updi.initialized = false;
			g_updi.unlocked = false;
			return false;
		}
	}

	//enter progmode & unlock if needed && write flash / erase set since unlocking erases
	if (_updi_enter_progmode()) {
		AVRVERBOSE("IN PROG MODE EASY\n");
		g_updi.unlocked = true;
	}
	else {
		AVRVERBOSE("Couldnt enter progmode\n");

		if (unlock) {
			AVRVERBOSE("need to unlock device\n");

			_updi_unlock_device();

			if (_updi_is_prog_mode()) {
				AVRVERBOSE("IN PROG MODE HARD\n");
				g_updi.unlocked = true;
			} else {
				MESSAGE("Could not unlock.\n");
				g_updi.unlocked = false;
				return false;
			}
		}
		else {
			//MESSAGE("Need to erase device to unlock progmode. Need process args UPDI_TASK_ERASE or UPDI_TASK_WRITE_FLASH set\n");
			MESSAGE("UPDI chip is locked\n");
			g_updi.unlocked = false;
			return false;
		}
	}

	if (!_updi_get_device_info()) {
		AVRVERBOSE("Unable to get chip information - may not be UPDI capable.");
		//g_updi.initialized = false;
		return false;
	}

	// TODO find out where we reset these because it was a mistake
	g_updi.initialized = true;
	g_updi.unlocked = true;
	return true;
}

static void _updi_task_cleanup () {
	//leave progmode
	_updi_leave_progmode();
	//Tidy up
	_updi_term();
	AVRVERBOSE("UPDI Serial retry counter: %d\n", _updi_serial_retry_count);
}


// -------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------
/*
	*setup() and *check_state_change() are the common entry points requried by the avr.cpp wrapper
*/
// -------------------------------------------------------------------------------------------------

// this consolodates the common errors to report
static bool _reported_error_messages(Stream *client) {
	if (g_updi.initialized) {
		if (g_updi.unlocked)
			return false;
		else {
			CLIENTMSG("Error: Chip is locked.\n");
			CLIENTMSG("       Reset the connection to the chip\n");
			CLIENTMSG("       or cycle power OFF/ON to the chip.\n");
		}
	} else {
		CLIENTMSG("Error: Unable to retrieve chip information.\n");
		CLIENTMSG("       Chip may not be UPDI capable.\n");
		CLIENTMSG("       Check the connection to the chip.\n");
		CLIENTMSG("       Attempt to cycle power OFF/ON to the chip.\n");
	}
	return true;
}

// this is for test purposes only and will go away
bool updiInit() {
	_updi_serial_retry_counter = 0;
	_updi_serial_retry_count = 0;
	_updi_serial_inited = false;
	memset(&g_updi, 0, sizeof(g_updi));
	return true;
}

void updiLoop() {
	// noting to do as it is all handled thru TCP methods
}

bool updiIsConnected(Stream* client, bool silent) {
	_updi_task_setup(false);	// this also gets chip info
	_updi_task_cleanup();

	if (g_updi.initialized) {
		if (g_updi.unlocked)
			if (!silent)
				CLIENTMSG("Detected %s\n", g_updi.device->longname);
			return true;
	}
	if (!silent) {
		CLIENTMSG("No UPDI chip detected\n");
		CLIENTMSG("                             ");
		CLIENTMSG("Check the connection to the chip.\n");
		CLIENTMSG("                             ");
		CLIENTMSG("Attempt to cycle power to the chip.\n");
		CLIENTMSG("                             ");
		CLIENTMSG("If cycling power works, consider using VCC(sw).\n");

	}
	return false;
}

bool updiGetInfo(Stream *client) {
	AVRVERBOSE("Begin updiGetInfo()\n");
	if (_updi_task_setup(false)) {
		// this also gets chip info
	} else {
		AVRVERBOSE("_updi_task_setup() failed\n");
	}
	_updi_task_cleanup();

	if (!_reported_error_messages(client)) {
		CLIENTMSG("Device info:\n");
		CLIENTMSG("Family: %s\n", g_updi.details.family);
		CLIENTMSG("Shortname: %s\n", g_updi.device->shortname);
		CLIENTMSG("Longname: %s\n", g_updi.device->longname);
		//CLIENTMSG("Signature: %04X\n", g_updi.device->signature);
		CLIENTMSG("Signature: %02X%02X%02X\n", g_updi.details.signature_bytes[0], g_updi.details.signature_bytes[1], g_updi.details.signature_bytes[2]);

		//CLIENTMSG("Chip Type: %02X%02X%02X\n", g_updi.details.signature_bytes[0], g_updi.details.signature_bytes[1], g_updi.details.signature_bytes[2]);
		CLIENTMSG("FLASH Addr: %04X bytes\n", g_updi.config->flash_start);
		CLIENTMSG("FLASH Size: %d bytes\n", g_updi.config->flash_size);
		CLIENTMSG("FLASH Page: %d * %d bytes\n", g_updi.config->flash_size / g_updi.config->flash_pagesize, g_updi.config->flash_pagesize);

		CLIENTMSG("EEPROM Addr: %04X bytes\n", AVR_EEPROM_ADDR);
		CLIENTMSG("EEPROM Size: %d bytes\n", g_updi.config->eeprom_size);
		CLIENTMSG("NVM version: %s\n", g_updi.details.nvm_version);
		CLIENTMSG("OCD version: %s\n", g_updi.details.ocd_version);
		if (IS_UPDI0)
			CLIENTMSG("NVM type 0: 16-bit addresses\n");
		else if (IS_UPDI2)
			CLIENTMSG("NVM type 2: 24-bit addresses\n");
		else
			CLIENTMSG("NVM type unknown\n");

		//CLIENTMSG("DBG freq: %c MHz\n", g_updi.details.dbg_osc_freq);
		//CLIENTMSG("PDI rev: %d\n", g_updi.details.pdi_rev);
		//CLIENTMSG("Rev: 0x%02X\n", g_updi.details.dev_rev);

		//CLIENTMSG("16Mhz 3V error: %+3d\n", g_updi.details.error_16v3);
		//CLIENTMSG("20Mhz 3V error: %+3d\n", g_updi.details.error_16v5);
		//CLIENTMSG("16Mhz 5V error: %+3d\n", g_updi.details.error_20v3);
		//CLIENTMSG("20Mhz 5V error: %+3d\n", g_updi.details.error_20v5);

		char hex_string[(AVR_UID_LENGTH + 1) * 2 + 1];
		uint8_t len = AVR_UID_LENGTH;
		if (IS_UPDI0)
			len = 10;
		for (uint i = 0; i < len; i++)
			sprintf(&(hex_string[i*2]), "%02X", g_updi.details.uid[i]);
		CLIENTMSG("UID: %s\n", hex_string);
		//CLIENTMSG("\n");
		return true;
	}
	return false;
}

bool updiReadFuses(Stream *client) {
	const char *fuse_names[] = {"WDT", "BOD", "OSC", NULL, NULL, "SY0", "SY1", "APE", "BTE", NULL, "LCK"};

	AVRVERBOSE("Begin updiReadFuses()\n");
	if (_updi_task_setup(false)) {
		for (uint8_t i = 0; i < AVR_NUM_FUSES; i++) {
			uint8_t value = _updi_read_fuse(i);
			g_updi.fuses[i] = value;
		}
	}
	_updi_task_cleanup();

	if (!_reported_error_messages(client)) {
		CLIENTMSG("Fuses:  ");
		for (int i = 0; i < AVR_NUM_FUSES; i++) {
			if (fuse_names[i])
				CLIENTMSG("%s:%d:%02X ", fuse_names[i], i, g_updi.fuses[i]);
		}
		CLIENTMSG("\n");
		return true;
	}
	return false;
}

bool updiWriteFuse(Stream *client, uint8_t fuse_number, uint8_t fuse_value) {
	AVRVERBOSE("Begin updiWriteFuse()\n");
	if (fuse_number >= AVR_NUM_FUSES) {
		CLIENTMSG("Error: fuse #%d is out of range of 0..%d\n", fuse_number, AVR_NUM_FUSES);
		return false;
	}
	if (fuse_number == (AVR_FUSE_LOCK - AVR_FUSE_BASE)) {
		CLIENTMSG("Nope: we are not going to let you change the lock bits\n");
		return false;
	}

	CLIENTMSG("Current ");
	updiReadFuses(client);
	if (!_reported_error_messages(client)) {
		if (_updi_task_setup(true)) {
			_updi_write_fuse(fuse_number, fuse_value);
		}
		_updi_task_cleanup();
		if (!_reported_error_messages(client)) {
			CLIENTMSG("    New ");
			updiReadFuses(client);
			return true;
		}
	}
	return false;
}

bool updiErase(Stream *client) {
	AVRVERBOSE("Begin updiErase()\n");
	if (_updi_task_setup(true)) {
		if (!_updi_erase_chip()) {
			MESSAGE("Chip erase failed\n");
		}
	}
	_updi_task_cleanup();

	if (!_reported_error_messages(client)) {
		CLIENTMSG("Erase finished.\n");
		return true;
	}
	return false;
}

bool updiFlashData(Stream *client, ihexStream* src) {
	AVRVERBOSE("Begin updiFlashData()\n");
	bool success = true;

	src->rewind();
	uint32_t datasize = src->available();

	//load hex, determine size
	if (!datasize) {
		MESSAGE("No data to flash\n");
		return false;
	}

	datasize = 512;

	if (_updi_task_setup(true)) {
		if (!_updi_erase_chip()) {
			MESSAGE("Chip erase failed\n");
			success = false;
		}
		else {
			MESSAGE("Flashing %d bytes: \n", datasize);
			if (!_updi_write_address_space(g_updi.config->flash_start, g_updi.config->flash_pagesize, datasize, src, WORD_DATA)) {
				MESSAGE("Writing flash failed\n");
				success = false;
			} else {
				MESSAGE("Flash written\n");
			}
			if (success) {
				VERBOSE("Verifying flash\n");
				src->rewind();

				_updi_task_cleanup();

				if (_updi_task_setup(true)) {
					// read flash 'don't save' = compare contents = verify
					if (!_updi_read_address_space(g_updi.config->flash_start, g_updi.config->flash_pagesize, datasize, src, BYTE_DATA, false)) {
						MESSAGE("Verify flash failed\n");
						success = false;
					}
					VERBOSE("Verify flash passed\n");
				}
			}
		}
	}
	_updi_task_cleanup();

	CLIENTMSG("\n");
	if (!_reported_error_messages(client)) {
		src->rewind();
		datasize = src->available();

		if (datasize) {
			if (!(g_updi.verified))
				CLIENTMSG("FLASH failed verification. Chip in unknown\n");
			else
				CLIENTMSG("WRITE %d bytes to FLASH\n", datasize);
			return true;
		}
		CLIENTMSG("Error: FLASH WRITE failed, Chip in unknown state\n");
	}
	return false;
}

bool updiDumpFlash(Stream *client, ihexStream* dest) {
	AVRVERBOSE("Begin updiDumpFlash()\n");
	if (_updi_task_setup(true)) {
		AVRVERBOSE("Reading %d bytes starting at %04X\n", g_updi.config->flash_size, g_updi.config->flash_start);

		// read flash 'save' = dump contents = store in memory buffer
		if (!_updi_read_address_space(g_updi.config->flash_start, g_updi.config->flash_pagesize, g_updi.config->flash_size, dest, BYTE_DATA, true)) {
			MESSAGE("Reading flash failed\n");
		} else {
			dest->rewind();
			//data->resize(g_updi.config->flash_size, g_updi.config->flash_pagesize);
			uint32_t datasize = dest->available();
			MESSAGE("Read %d bytes: \n", datasize);
		}
	}
	_updi_task_cleanup();
	CLIENTMSG("\n");

	if (!_reported_error_messages(client)) {
		uint32_t size;
		dest->rewind();
		size = dest->available();
		if (size) {
			CLIENTMSG("FLASH %d bytes to memory buffer\n", size);
			return true;
		}
		CLIENTMSG("Error: FLASH READ  no data available\n");
	}
	return false;
}

bool updiWriteEeprom(Stream *client, uint32_t offset, ihexStream* src) {
	AVRVERBOSE("Begin updiWriteEeprom()\n");
	src->rewind();
	uint32_t datasize = src->available();
	if (!datasize) {
		MESSAGE("No data to write\n");
		return false;
	}

	if (_updi_task_setup(true)) {
		MESSAGE("Writing eeprom %d bytes: \n", datasize);

		if (!_updi_write_address_space(AVR_EEPROM_ADDR + offset, datasize, datasize, src, BYTE_DATA)) {
			MESSAGE("Writing eeprom failed\n");
		} else {
			MESSAGE("eeprom written\n");
			src->rewind();

			_updi_task_cleanup();

			if (_updi_task_setup(true)) {
				VERBOSE("Verifying eeprom\n");
				// read flash 'don't save' = compare contents = verify
				if (!_updi_read_address_space(AVR_EEPROM_ADDR + offset, datasize, datasize, src, BYTE_DATA, false)) {
					MESSAGE("Verify eeprom failed\n");
				} else {
					VERBOSE("Verify eeprom passed\n");
				}
			}
		}
	}
	_updi_task_cleanup();
	CLIENTMSG("\n");

	if (!_reported_error_messages(client)) {
		src->rewind();
		datasize = src->available();

		if (datasize && g_updi.verified) {
			return true;
		}
	}
	CLIENTMSG("EEPROM failed verification.\n");
	return false;
}

bool updiReadEeprom(Stream *client, uint32_t offset, uint32_t size, ihexStream* dest) {
	AVRVERBOSE("Begin updiReadEeprom()\n");
	uint32_t len = 0;
	if (_updi_task_setup(false)) {
		AVRVERBOSE("Reading %d bytes starting at %04X\n", size, AVR_EEPROM_ADDR + offset);

		// read eeprom and store in memory buffer; eeprom has a pagesize but allows read/write byte at a time
		if (!_updi_read_address_space(AVR_EEPROM_ADDR + offset, size /* g_updi.config->eeprom_pagesize */, size, dest, WORD_DATA, true)) {
			MESSAGE("Reading eeprom failed\n");
		} else {
			dest->rewind();
			//data->resize(g_updi.config->flash_size, g_updi.config->flash_pagesize);
			len = dest->available();
			MESSAGE("Read %d of %d bytes\n", len, size);
		}
	}
	_updi_task_cleanup();

	CLIENTMSG("\n");
	if (!_reported_error_messages(client)) {
		if (len != size) {
			CLIENTMSG("size mismatch %d != %d\n", size, len);
		}
		if (!len) {
			CLIENTMSG("Memory buffer is empty\n");
			return false;
		}

		int c = -1;
		for (uint32_t count = 0; count < len; count++) {
			c = dest->read();
			if (c < 0)
				break;

			ioStreamPrintf(client, "%02X", c);	// output to display
		}
		ioStreamPrintf(client, "\n");	// output to display
		dest->rewind();
		return true;
	}
	return false;
}

